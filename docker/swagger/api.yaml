openapi: 3.0.3
info:
  description: Przykładowy swagger dla części wystawionego API (end-pointy z kontrolera ClientController)
  version: 1.0.0
  title: Dokumentacja API udostępnionego przez "Aplikacja do zarządzania firmą"
tags:
  - name: api
    description: End-pointy udostępnione w kontrolerze ClientController

paths:
  /company/clients:
    post:
      security:
        - jwtAuth: [ ]
      tags:
        - api
      summary: Tworzenie nowego klienta w kontekście zalogowanej firmy
      requestBody:
        description: Wszystkie potrzebne dane do utworzenia nowego klienta
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateOrUpdateClientData'
      responses:
        '200':
          description: Nowy klient został utworzony i przypisany do firmy prawidłowo
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OperationSucceeded'
        '400':
          description: Przekazana dane są nieprawidłowe (np. brakuje klucza 'name' lub wprowadzony 'nip' już istnieje w bazie danych i jest przypisany jako klient dla zalogowanej firmy)
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ClientCreationOrUpdateFailedError"
        '401':
          description: Token JWT jest nieprawidłowy
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnauthorizedError"
    get:
      security:
        - jwtAuth: [ ]
      tags:
        - api
      summary: Pobranie listy wszystkich klientów w kontekście zalogowanej firmy
      responses:
        '200':
          description: Lista wszystkich klientów w kontekście zalogowanej firmy
          content:
            application/json:
              schema:
                items:
                  $ref: '#/components/schemas/ClientBaseModel'
        '401':
          description: Token JWT jest nieprawidłowy
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnauthorizedError"

  /company/clients/{id}:
    get:
      security:
        - jwtAuth: [ ]
      tags:
        - api
      summary: Pobranie danych konkretnego klienta po jego id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: unikalny id szukanego klienta
      responses:
        '200':
          description: Dane szukanego klienta
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ClientBaseModel'
        '404':
          description: Nie znaleziono klienta o podanym id
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ClientNotFoundError"
        '401':
          description: Token JWT jest nieprawidłowy
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnauthorizedError"
    put:
      security:
        - jwtAuth: [ ]
      tags:
        - api
      summary: Aktualizacja danych konkretnego klienta po jego id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: unikalny id aktualizowanego klienta
      requestBody:
        description: Wszystkie potrzebne dane do zaktualizowania istniejącego klienta
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateOrUpdateClientData'
      responses:
        '200':
          description: Klient został zaktualizowany prawidłowo
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OperationSucceeded'
        '400':
          description: Przekazana dane są nieprawidłowe (np. brakuje klucza 'name' lub wprowadzony 'nip' już istnieje w bazie danych i jest przypisany jako klient dla zalogowanej firmy)
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ClientCreationOrUpdateFailedError"
        '404':
          description: Nie znaleziono klienta o podanym id
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ClientNotFoundError"
        '401':
          description: Token JWT jest nieprawidłowy
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnauthorizedError"
    delete:
      security:
        - jwtAuth: [ ]
      tags:
        - api
      summary: Usunięcie danych konkretnego klienta po jego id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: unikalny id usuwanego klienta
      responses:
        '200':
          description: Wybrany klient został usunięty prawidłowo
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OperationSucceeded'
        '404':
          description: Nie znaleziono klienta o podanym id
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ClientNotFoundError"
        '401':
          description: Token JWT jest nieprawidłowy
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnauthorizedError"

components:
  securitySchemes:
    jwtAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT

  schemas:
    CreateOrUpdateClientData:
      type: object
      properties:
        name:
          type: string
          example: SOFTIQ sp. z o.o.
        nip:
          type: string
          example: 5272446008
        regon:
          type: string
          example: 073872476
        address1:
          type: string
          example: 44-100 Gliwice, ul. Robotnicza 2
        address2:
          type: string
          example: ''
        contactPhoneNumber:
          type: string
          example: 222333444
        contactEmail:
          type: string
          example: info@softiq.pl

    OperationSucceeded:
      type: object
      properties:
        id:
          type: integer
          example: 123456
        status:
          type: integer
          example: 200

    ClientBaseModel:
      type: object
      properties:
        id:
          type: integer
          example: 196743890
        name:
          type: string
          example: Corporate Sp. z o.o.
        nip:
          type: string
          example: 7575591012
        contactEmail:
          type: string
          example: corporate@gmail.com
        contactPhoneNumber:
          type: string
          example: 605554683
        numberOfClosedProjects:
          type: integer
          example: 0
        numberOfProjects:
          type: integer
          example: 2
        regon:
          type: string
          example: 072558766
        address1:
          type: string
          example: ul. Architektów 109
        address2:
          type: string
          example: 81-528 Gdynia

    ClientCreationOrUpdateFailedError:
      type: object
      properties:
        status:
          type: integer
          example: 400
        message:
          type: string
          example: INVALID_CLIENT_REQUEST_DATA

    UnauthorizedError:
      type: object
      properties:
        status:
          type: integer
          example: 401
        message:
          type: string
          example: INVALID_JWT

    ClientNotFoundError:
      type: object
      properties:
        status:
          type: integer
          example: 404
        message:
          type: string
          example: CLIENT_NOT_FOUND

servers:
  - url: 'http://127.0.0.1/api'
    description: Lokalne środowisko testowe (używa danych zapisanych w bazie lokalnej)
