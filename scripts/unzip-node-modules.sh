#!/usr/bin/env bash

if [ ! -d ./codebase/client/node_modules ]; then
  cd codebase/client || exit

  if [ ! -f node_modules.tar.gz ]; then
    npm install
  else
    tar -xzvf node_modules.tar.gz
  fi;

  cd ../.. || exit
fi;
