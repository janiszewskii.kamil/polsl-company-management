#!/usr/bin/env bash
ENV="$1"

echo "=================== Starting Docker containers ================="
bash scripts/unzip-node-modules.sh
docker-compose -f docker-compose."${ENV}".yml up -d
echo "================================================================"
echo ""

echo "===================== Starting server =========================="
docker-compose -f docker-compose."${ENV}".yml exec -T server-php composer install
docker-compose -f docker-compose."${ENV}".yml exec -T server-php php bin/console doctrine:database:drop --force
docker-compose -f docker-compose."${ENV}".yml exec -T server-php php bin/console doctrine:database:create
docker-compose -f docker-compose."${ENV}".yml exec -T server-php php bin/console doctrine:schema:update --force
docker-compose -f docker-compose."${ENV}".yml exec -T server-php php bin/console doctrine:fixtures:load -n
echo ""
bash scripts/create-jwt-certs.sh
echo "================================================================"
echo ""
