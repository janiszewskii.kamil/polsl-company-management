#!/usr/bin/env bash
if [ -f ./codebase/server/.env ]; then
  export $(cat ./codebase/server/.env | sed 's/#.*//g' | xargs)
fi

if [ -z "$JWT_PASSPHRASE" ]; then
  echo "ENV variables not loaded. Step skipped."
  exit 1
else
  echo "ENV variables loaded."
fi

KERNEL_PATH_STRING="%kernel.project_dir%"
JWT_CERTS_PATH_STRING=$PWD"/codebase/server"

rm -Rf "$JWT_CERTS_PATH_STRING/config/jwt"
mkdir "$JWT_CERTS_PATH_STRING/config/jwt"

JWT_SECRET_PEM=$(echo $JWT_SECRET_KEY | sed "s|$KERNEL_PATH_STRING|$JWT_CERTS_PATH_STRING|")
JWT_PUBLIC_PEM=$(echo $JWT_PUBLIC_KEY | sed "s|$KERNEL_PATH_STRING|$JWT_CERTS_PATH_STRING|")

if [ ! -f "${JWT_SECRET_PEM}" ]; then
  echo "Generating JWT private key and public certificate for project."
  openssl genpkey -out "${JWT_SECRET_PEM}" -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -pass env:JWT_PASSPHRASE
  openssl pkey -in "${JWT_SECRET_PEM}" -out "${JWT_PUBLIC_PEM}" -pubout -passin env:JWT_PASSPHRASE
  echo "JWT private key and public certificate created for project - details are in /codebase/server/.env file."
else
  echo "JWT private key and public certificate for project are already set in this project."
fi

chmod -R 777 "$JWT_CERTS_PATH_STRING/config/jwt"
