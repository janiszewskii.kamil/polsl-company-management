#!/usr/bin/env bash
ENV="$1"

docker-compose -f docker-compose."${ENV}".yml down --remove-orphans -v
