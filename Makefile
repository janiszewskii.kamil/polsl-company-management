SHELL:=/bin/bash
include .env

unzip_node_modules:
	bash ./scripts/unzip-node-modules.sh

start:
	bash ./scripts/start.sh $(ENV)

stop:
	bash ./scripts/stop.sh $(ENV)

stop_clear:
	bash ./scripts/stop-clear.sh $(ENV)

restart:
	make stop_clear && make start

start_server_async_workers:
	docker-compose -f docker-compose.${ENV}.yml exec -T server-php php bin/console messenger:consume async -vv
