# Aplikacja do zarządzania firmą

Projekt zaliczeniowy na przedmioty "Projektowanie Aplikacji Internetowych" i "Organizacja i Rozwój Projektów Open
Source" z 3 semestru na informatyce niestacjonarnej, magisterskiej.

autorzy:\
Kamil Janiszewski\
Tomasz Mazurek\
Szymon Nowak

## Instalacja i uruchamianie projektu

Do uruchomienia projektu niezbędne jest posiadanie zainstalowanego w systemie Dockera. W celu uzyskania najwyższej
wydajności, zaleca się uruchamianie projektu na Linuxie lub WSL2 (dla Windowsa).

### Instalacja na Linux lub WSL2

1. Sklonuj lub rozpakuj repozytorium w dowolnym miejscu na dysku.
2. W głównym folderze skopiuj plik .env.example i zapisz jako .env - `cp .env.example .env`.
3. Sprawdź czy posiadasz folder node_modules.tar.gz pod ścieżką /codebase/client/node_modules.tar.gz.
    1. Jeżeli folder istnieje, przejdź do kolejnego kroku (z numerem 4).
    2. Jeżeli folder nie istnieje i nie masz w systemie zainstalowanego Node.js w wersji 12 i nie chcesz go instalować,
       to pobierz folder z tego linku: https://gitlab.com/janiszewskii.kamil/polsl-company-management/-/blob/develop/codebase/client/node_modules.tar.gz.
    3. Jeżeli posiadasz Node.js w wersji 12 i nie pobierzesz folderu z node_modules ręcznie, to następny skrypt uruchomi
       polecenie `npm install`, które będzie przetwarzało się około 8 minut.
4. Upewnij sie, że posiadasz w systemie zainstalowany program do obsługi plików Makefile
   (np. `sudo apt-get -y install make` - na Ubuntu), a następnie w przygotowanym folderze uruchom w konsoli
   komendę `make start` i odczekaj chwilę aż skrypt automatycznie zainstaluje i skonfiguruje wszystko co jest niezbędne.
5. Jeżeli chcesz, aby w skrzynce pocztowej pojawiały się wiadomości wysyłane z aplikacji, uruchom w konsoli
   komendę `make start_server_async_workers` i następnie nie zamykaj okna w którym komenda została uruchomiona.

### Testowanie aplikacji

Klient (front-end) dostępny jest pod adresem: "localhost:4200"\
Serwer API (back-end) dostępny jest pod adresem: "localhost:80/api"

Użytkownicy testowi (stanowisko w firmie / login / hasło):

szef firmy - admin@dev.dev / test\
1 kadrowy firmy - kadrowy1@dev.dev / test\
2 kadrowy firmy - kadrowy2@dev.dev / test\
1 księgowy firmy - ksiegowy1@dev.dev / test\
2 księgowy firmy - ksiegowy2@dev.dev / test\
1 programista w firmie - programista1@dev.dev / test\
2 programista w firmie - programista2@dev.dev / test\
3 programista w firmie - programista3@dev.dev / test\
4 programista w firmie - programista4@dev.dev / test\
stażysta w firmie - stazysta@dev.dev / test

### Dostępy

Dostęp do opisu API (Swagger):\
adres: localhost:5001

Dostęp do bazy danych (PostgreSQL):\
adres: localhost:5432\
nazwa bazy: cm-database\
uzytkownik: admin / hasło: admin

Dostęp do testowej skrzynki pocztowej (MailHog):\
adres: localhost:8025

Dostęp do panelu brokera wiadomości (RabbitMQ):\
adres: localhost:15672\
uzytkownik: guest / hasło: guest

### Dostępne komendy Makefile

make unzip_node_modules - rozpakowuje node_modules (jeżeli istnieje potrzeba wykonania tego ręcznie)\
make start - uruchamia i konfiguruje cały projekt\
make stop - zatrzymuje cały projekt\
make stop_clear - zatrzymuje cały projekt i czyści dockerowe volumes\
make restart - restartuje cały projekt\
make start_server_async_workers - uruchamia workery do asynchronicznej obsługi wiadomości w aplikacji
