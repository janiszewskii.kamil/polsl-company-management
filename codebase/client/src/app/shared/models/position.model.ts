export class Position {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public isManagerialPosition?: boolean
  ) {
  }

  static adapt(item): Position {
    if (item) {
      return new Position(
        item.id,
        item.name,
        item.description,
        item.isManagerialPosition,
      );
    } else {
      return null;
    }
  }
}
