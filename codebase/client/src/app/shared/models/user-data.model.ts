export enum Genders {
  Male = 'male',
  Female = 'female',
}

export class UserData {
  constructor(
    public firstName?: string,
    public lastName?: string,
    public birthDate?: string,
    public phoneNumber?: string,
    public gender?: Genders,
    public street?: string,
    public houseNo?: string,
    public flatNo?: string,
    public zipCode?: string,
    public city?: string,
    public province?: string,
  ) {
  }

  static adapt(item: any): UserData {
    if (item) {
      return new UserData(
        item.firstName,
        item.lastName,
        item.birthDate,
        item.phoneNumber,
        item.gender,
        item.street,
        item.houseNo,
        item.flatNo,
        item.zipCode,
        item.city,
        item.province
      );
    } else {
      return null;
    }
  }
}
