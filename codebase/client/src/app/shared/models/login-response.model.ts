import {User} from './user.model';

export class LoginResponse {
  constructor(
    public token?: string,
    public user?: User,
  ) {}

  static adapt(item): LoginResponse {
    if (item) {
      return new LoginResponse(
        item.token,
        item.user ? User.adapt(item.user) : null,
      );
    } else {
      return null;
    }
  }
}
