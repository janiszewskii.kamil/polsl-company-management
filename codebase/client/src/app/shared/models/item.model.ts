import {User} from './user.model';
import {Department} from './department.model';

export class Item {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public department?: Department,
    public employee?: User,
    public warrantyEndDate?: string,
    public typeOfOwnership?: string,
  ) {
  }

  static adapt(item): Item {
    if (item) {
      return new Item(
        item.id,
        item.name,
        item.description,
        item.department ? Department.adapt(item.department) : null,
        item.user ? User.adapt(item.user) : null,
        item.warrantyEndDate,
        item.typeOfOwnership,
      );
    } else {
      return null;
    }
  }
}
