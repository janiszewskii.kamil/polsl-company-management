import {User} from './user.model';
import {Department} from './department.model';

export class Application {
  constructor(
    public id?: number,
    public title?: string,
    public targetDepartment?: Department,
    public text?: string,
    public amount?: number,
    public createdBy?: User,
    public servedBy?: User,
    public createdAt?: string,
    public updatedAt?: string,
    public rejectionReason?: string,
    public status?: string,
  ) {
  }

  static adapt(item): Application {
    if (item) {
      return new Application(
        item.id,
        item.title,
        item.targetDepartment ? Department.adapt(item.targetDepartment) : null,
        item.text,
        item.amount,
        item.createdBy ? User.adapt(item.createdBy) : null,
        item.servedBy ? User.adapt(item.servedBy) : null,
        item.createdAt,
        item.updatedAt,
        item.rejectionReason,
        item.status
      );
    } else {
      return null;
    }
  }
}
