import {User} from './user.model';

export class Message {
  constructor(
    public title?: string,
    public topic?: string,
    public createdBy?: User,
    public recipient?: User,
    public createdAt?: string,
  ) {
  }

  static adapt(item): Message {
    if (item) {
      return new Message(
        item.title,
        item.topic,
        item.createdBy ? User.adapt(item.createdBy) : null,
        item.recipient ? User.adapt(item.recipient) : null,
        item.createdAt
      );
    } else {
      return null;
    }
  }
}
