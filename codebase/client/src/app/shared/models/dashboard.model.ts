export class Dashboard {
  constructor(
    public numberOfEmployees?: number,
    public numberOfEvents?: number,
    public numberOfApplications?: number,
    public numberOfProjects?: number,
  ) {
  }

  static adapt(item): Dashboard {
    if (item) {
      return new Dashboard(
        item.numberOfEmployees,
        item.numberOfEvents,
        item.numberOfApplications,
        item.numberOfProjects,
      );
    } else {
      return null;
    }
  }
}
