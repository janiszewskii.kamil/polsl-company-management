export class Client {
  constructor(
    public id?: number,
    public name?: string,
    public regon?: string,
    public nip?: string,
    public contactPhoneNumber?: string,
    public contactEmail?: string,
    public address1?: string,
    public address2?: string,
    public numberOfProjects?: number,
    public numberOfClosedProjects?: number,
  ) {
  }

  static adapt(item): Client {
    if (item) {
      return new Client(
        item.id,
        item.name,
        item.regon,
        item.nip,
        item.contactPhoneNumber,
        item.contactEmail,
        item.address1,
        item.address2,
        item.numberOfProjects,
        item.numberOfClosedProjects,
      );
    } else {
      return null;
    }
  }
}
