export class UserPermissions {
  constructor(
    public canViewClients?: boolean,
    public canEditClients?: boolean,
    public canViewProjects?: boolean,
    public canEditProjects?: boolean,
    public canViewItems?: boolean,
    public canEditItems?: boolean,
    public canViewWorkTimeReports?: boolean,
    public canViewCompany?: boolean,
    public canEditCompany?: boolean,
    public canEditEmployees?: boolean,
    public canViewEmployees?: boolean,
    public canServeApplications?: boolean,
    public canViewApplications?: boolean,
    public canEditEvents?: boolean,
    public canViewEvents?: boolean,
    public canPublishNews?: boolean,
  ) {
  }

  static adapt(item): UserPermissions {
    if (item) {
      return new UserPermissions(
        item.canViewClients,
        item.canEditClients,
        item.canViewProjects,
        item.canEditProjects,
        item.canViewItems,
        item.canEditItems,
        item.canViewWorkTimeReports,
        item.canViewCompany,
        item.canEditCompany,
        item.canEditEmployees,
        item.canViewEmployees,
        item.canServeApplications,
        item.canViewApplications,
        item.canEditEvents,
        item.canViewEvents,
        item.canPublishNews
      );
    } else {
      return null;
    }
  }
}
