import {UserData} from './user-data.model';
import {Company} from './company.model';
import {Position} from './position.model';
import {Department} from './department.model';
import {UserPermissions} from './user-permissions.model';

export class User {
  constructor(
    public id?: number,
    public email?: string,
    public hireDate?: string,
    public position?: Position,
    public department?: Department,
    public permissions?: UserPermissions,
    public data?: UserData,
    public company?: Company,
  ) {}

  static adapt(item): User {
    if (item) {
      return new User(
        item.id,
        item.email,
        item.hireDate,
        item.position ? Position.adapt(item.position) : null,
        item.department ? Department.adapt(item.department) : null,
        item.permissions ? UserPermissions.adapt(item.permissions) : null,
        item.data ? UserData.adapt(item.data) : null,
        item.company ? Company.adapt(item.company) : null,
      );
    } else {
      return null;
    }
  }
}
