import {User} from './user.model';

export class CompanyNews {
  constructor(
    public id?: number,
    public title?: string,
    public text?: string,
    public type?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public createdBy?: User,
    public updatedBy?: User
  ) {
  }

  static adapt(item): CompanyNews {
    if (item) {
      return new CompanyNews(
        item.id,
        item.title,
        item.text,
        item.type,
        item.createdAt,
        item.updatedAt,
        item.createdBy ? User.adapt(item.createdBy) : null,
        item.updatedBy ? User.adapt(item.updatedBy) : null,
      );
    } else {
      return null;
    }
  }
}
