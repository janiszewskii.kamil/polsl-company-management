export enum LocalStorageKeys {
  CurrentUser = 'currentUser',
  CurrentCompany = 'currentCompany',
  Token = 'token'
}
