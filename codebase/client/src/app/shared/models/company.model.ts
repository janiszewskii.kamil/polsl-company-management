import {User} from './user.model';

export class Company {
  constructor(
    public id?: number,
    public name?: string,
    public nip?: string,
    public regon?: string,
    public typeOfBusiness?: string,
    public address1?: string,
    public address2?: string,
    public updatedAt?: string,
    public updatedBy?: User
  ) {
  }

  static adapt(item): Company {
    if (item) {
      return new Company(
        item.id,
        item.name,
        item.nip,
        item.regon,
        item.typeOfBusiness,
        item.address1,
        item.address2,
        item.updatedAt,
        item.updatedBy ? User.adapt(item.updatedBy) : null,
      );
    } else {
      return null;
    }
  }
}
