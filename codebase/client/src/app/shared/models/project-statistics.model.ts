export class ProjectStatistics {
  constructor(
    public totalFullDays?: number,
    public totalTime?: number,
    public totalDelayedDays?: number,
    public totalProfit?: number,
    public totalSubmittedReports?: number,
    public totalEmployees?: number,
  ) {
  }

  static adapt(item): ProjectStatistics {
    if (item) {
      return new ProjectStatistics(
        item.totalFullDays,
        item.totalTime,
        item.totalDelayedDays,
        item.totalProfit,
        item.totalSubmitterReports,
        item.totalEmployees,
      );
    } else {
      return null;
    }
  }
}
