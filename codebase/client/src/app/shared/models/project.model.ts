import {User} from './user.model';
import {Client} from './client.model';

export class Project {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public client?: Client,
    public manager?: User,
    public description?: string,
    public startDate?: string,
    public plannedEndDate?: string,
    public endDate?: string,
    public plannedBudget?: string,
    public spentBudget?: string,
    public plannedIncome?: string,
    public finalIncome?: string,
    public isOpen?: boolean
  ) {
  }

  static adapt(item): Project {
    if (item) {
      return new Project(
        item.id,
        item.name,
        item.code,
        item.client ? Client.adapt(item.client) : null,
        item.manager ? User.adapt(item.manager) : null,
        item.description,
        item.startDate,
        item.plannedEndDate,
        item.endDate,
        item.plannedBudget,
        item.spentBudget,
        item.plannedIncome,
        item.finalIncome,
        item.isOpen,
      );
    } else {
      return null;
    }
  }
}
