import {User} from './user.model';

export class Department {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public manager?: User
  ) {
  }

  static adapt(item): Department {
    if (item) {
      return new Department(
        item.id,
        item.name,
        item.description,
        item.manager ? User.adapt(item.manager) : null,
      );
    } else {
      return null;
    }
  }
}
