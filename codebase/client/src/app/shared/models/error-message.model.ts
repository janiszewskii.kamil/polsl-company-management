export enum ErrorMessages {
  InvalidToken = 'invalidToken',
  InvalidCredentials = 'invalidCredentials',
  AccountNotActive = 'accountNotActive'
}
