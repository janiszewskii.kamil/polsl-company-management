import {Project} from './project.model';
import {User} from './user.model';

export class WorkTimeReport {
  constructor(
    public id?: number,
    public message?: string,
    public time?: number,
    public type?: string,
    public project?: Project,
    public reportForDate?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public createdBy?: User
  ) {
  }

  static adapt(item): WorkTimeReport {
    if (item) {
      return new WorkTimeReport(
        item.id,
        item.message,
        item.time,
        item.type,
        item.project ? Project.adapt(item.project) : null,
        item.reportForDate,
        item.createdAt,
        item.updatedAt,
        item.createdBy ? User.adapt(item.createdBy) : null,
      );
    } else {
      return null;
    }
  }
}
