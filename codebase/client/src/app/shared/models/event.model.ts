import {User} from './user.model';

export class Event {
  constructor(
    public id?: number,
    public name?: string,
    public startDate?: string,
    public endDate?: string,
    public participant?: User,
    public place?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public createdBy?: User,
    public updatedBy?: User,
    public isCanceled?: boolean,
    public time?: string,
  ) {
  }

  static adapt(item): Event {
    if (item) {
      return new Event(
        item.id,
        item.name,
        item.startDate,
        item.endDate,
        item.participant ? User.adapt(item.participant) : null,
        item.place,
        item.createdAt,
        item.updatedAt,
        item.createdBy ? User.adapt(item.createdBy) : null,
        item.updatedBy ? User.adapt(item.updatedBy) : null,
        item.isCanceled,
        item.time
      );
    } else {
      return null;
    }
  }
}
