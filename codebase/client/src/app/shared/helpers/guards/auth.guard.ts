import {Injectable} from '@angular/core';
import {Router, CanActivate, CanActivateChild} from '@angular/router';
import {LocalStorageKeys} from '../../models/local-storage.model';
import {NotificationService} from '../../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private router: Router, private notificationService: NotificationService) {
  }

  canActivate(): boolean {
    if (!localStorage.getItem(LocalStorageKeys.Token) || !localStorage.getItem(LocalStorageKeys.CurrentUser)) {
      this.router.navigate(['login']);
      this.notificationService.warn('Nieprawidłowy token sesji użytkownika.');
      return false;
    }
    return true;
  }

  canActivateChild(): boolean {
    return this.canActivate();
  }
}
