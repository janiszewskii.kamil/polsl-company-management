import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ErrorMessages} from '../../models/error-message.model';
import {NotificationService} from '../../services/notification.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router,
              private notificationService: NotificationService,
              private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (!(error.error instanceof ErrorEvent)) {
          if (error.error && (error.error.message === ErrorMessages.InvalidToken)) {
            this.authService.logout();
            this.notificationService.warn('Sesja wygasła');
            this.router.navigate(['/login']);
          } else if (error.error && (error.error.message === ErrorMessages.InvalidCredentials)) {
            this.authService.logout();
            this.notificationService.warn('Wprowadziłeś nieprawidłowe dane do logowania');
            this.router.navigate(['/login']);
          } else if (error.error && (error.error.message === ErrorMessages.AccountNotActive)) {
            this.authService.logout();
            this.notificationService.warn('Podane konto jest nieaktywne');
            this.router.navigate(['/login']);
          } else if (error.status === 404 && error.error.message !== 'emailNotFound') {
            this.router.navigate(['/404']);
          }  else if (error.status === 403) {
            this.router.navigate(['/403']);
          } else if (error.status === 401) {
            this.authService.logout();
            this.notificationService.warn('Sesja wygasła');
            this.router.navigate(['/login']);
          }
        }
        return throwError(error);
      })
    );
  }
}
