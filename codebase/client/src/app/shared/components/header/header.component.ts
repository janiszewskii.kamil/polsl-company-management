import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  subscriptions = new Subscription();
  currentCompanyName: string;

  constructor(private authService: AuthService, private userService: UserService) {
  }

  ngOnInit() {
    this.currentCompanyName = this.userService.getCurrentUser().company.name;
  }

  toggleSideBar(): void {
    this.toggleSideBarForMe.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

  logout(): void {
    this.subscriptions.add(this.authService.logout().subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
