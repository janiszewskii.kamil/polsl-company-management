import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {WorkTimeReport} from '../models/work-time-report.model';
import {FormBuilder, Validators} from '@angular/forms';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class WorkTimeReportService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  workReportTimeForm = this.formBuilder.group({
    $key: [null],
    project: [''],
    time: ['', Validators.required],
    message: ['', Validators.required],
    reportForDate: ['', Validators.required],
    type: ['', Validators.required]
  });

  initializeFormGroup() {
    this.workReportTimeForm.setValue({
      $key: null,
      project: '',
      time: '',
      message: '',
      reportForDate: '',
      type: ''
    });
  }

  populateForm(workTimeReport) {
    this.workReportTimeForm.setValue({
      $key: 'edit',
      project: workTimeReport.project ? workTimeReport.project.id : null,
      time: workTimeReport.time,
      message: workTimeReport.message,
      reportForDate: workTimeReport.reportForDate,
      type: workTimeReport.type
    });
  }

  getAllByCurrentUser(): Observable<WorkTimeReport[]> {
    return this.http.get<WorkTimeReport[]>(this.apiUrl + 'user/work-time-reports').pipe(
      map(res => res.map(WorkTimeReport.adapt))
    );
  }

  getAll(): Observable<WorkTimeReport[]> {
    return this.http.get<WorkTimeReport[]>(this.apiUrl + 'company/work-time-reports').pipe(
      map(res => res.map(WorkTimeReport.adapt))
    );
  }

  getById(id) {
    return this.http.get<WorkTimeReport>(this.apiUrl + 'work-time-reports/' + id).pipe(
      map(res => WorkTimeReport.adapt(res))
    );
  }

  getAllByEmployee(id: number): Observable<WorkTimeReport[]> {
    return this.http.get<WorkTimeReport[]>(this.apiUrl + 'user/' + id + '/work-time-reports').pipe(
      map(res => res.map(WorkTimeReport.adapt))
    );
  }

  getAllByProject(id: number): Observable<WorkTimeReport[]> {
    return this.http.get<WorkTimeReport[]>(this.apiUrl + 'project/' + id + '/work-time-reports').pipe(
      map(res => res.map(WorkTimeReport.adapt))
    );
  }

  createWorkTimeReport(workTimeReport) {
    return this.http.post(this.apiUrl + 'user/work-time-reports', workTimeReport).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono raport czasu pracy.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia raportu czasu pracy.');
        return err;
      })
    );
  }

  updateWorkTimeReport(id: number, workTimeReport) {
    return this.http.put(this.apiUrl + 'user/work-time-reports/' + id, workTimeReport).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano raport czasu pracy.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas edycji raportu czasu pracy.');
        return err;
      })
    );
  }

  removeWorkTimeReport(id: number) {
    return this.http.delete(this.apiUrl + 'user/work-time-reports/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto raport czasu pracy.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas usuwania raportu czasu pracy.');
        return err;
      })
    );
  }
}
