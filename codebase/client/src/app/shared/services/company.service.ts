import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {Company} from '../models/company.model';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private notificationService: NotificationService) {
  }

  getData() {
    return this.http.get<Company>(this.apiUrl + 'company').pipe(
      map(res => Company.adapt(res))
    );
  }

  updateData(company) {
    return this.http.put(this.apiUrl + 'company', company).pipe(
      tap(res => {
        this.notificationService.success('Poprawnie zapisano dane firmy.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas zapisywania danych firmy.');
        return err;
      })
    );
  }
}

