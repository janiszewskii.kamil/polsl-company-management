import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Client} from '../models/client.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyClientService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  companyClientForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    nip: ['', [Validators.minLength(10), Validators.maxLength(10)]],
    regon: ['', [Validators.minLength(9), Validators.maxLength(14)]],
    address1: ['', null],
    address2: ['', null],
    contactPhoneNumber: ['', [Validators.minLength(9), Validators.maxLength(14)]],
    contactEmail: ['', Validators.email],
  });

  initializeFormGroup() {
    this.companyClientForm.setValue({
      $key: null,
      name: '',
      nip: '',
      regon: '',
      address1: '',
      address2: '',
      contactPhoneNumber: '',
      contactEmail: '',
    });
  }

  populateForm(companyClient) {
    this.companyClientForm.setValue({
      $key: 'edit',
      name: companyClient.name,
      nip: companyClient.nip,
      regon: companyClient.regon,
      address1: companyClient.address1,
      address2: companyClient.address2,
      contactPhoneNumber: companyClient.contactPhoneNumber,
      contactEmail: companyClient.contactEmail,
    });
  }

  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(this.apiUrl + 'company/clients').pipe(
      map(res => res.map(Client.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Client>(this.apiUrl + 'company/clients/' + id).pipe(
      map(res => Client.adapt(res))
    );
  }

  createClient(client) {
    return this.http.post(this.apiUrl + 'company/clients', client).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono klienta.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia klienta.');
        return err;
      })
    );
  }

  updateClient(id: number, client) {
    return this.http.put(this.apiUrl + 'company/clients/' + id, client).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano klienta.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji klienta.');
        return err;
      })
    );
  }

  removeClient(id: number) {
    return this.http.delete(this.apiUrl + 'company/clients/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto klienta.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas usuwania klienta.');
        return err;
      })
    );
  }
}
