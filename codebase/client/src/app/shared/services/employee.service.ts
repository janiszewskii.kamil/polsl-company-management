import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../models/user.model';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  apiUrl = environment.apiUrl;
  employee: User;

  constructor(private http: HttpClient,
              private formBuilder: FormBuilder,
              private notificationService: NotificationService) {
  }

  employeeForm = this.formBuilder.group({
    $key: [null],
    firstName: ['', [Validators.required, Validators.minLength(3)]],
    lastName: ['', [Validators.required, Validators.minLength(3)]],
    gender: ['male'],
    birthDate: [''],
    phoneNumber: ['', [Validators.minLength(9), Validators.maxLength(9)]],
    email: ['', [Validators.required, Validators.email]],
    position: [''],
    hireDate: [''],
    city: [''],
    zipCode: [''],
    houseNo: [''],
    flatNo: [''],
    province: [''],
    department: ['']
  });

  initializeFormGroup() {
    this.employeeForm.setValue({
      $key: null,
      firstName: '',
      lastName: '',
      gender: 'male',
      birthDate: '',
      phoneNumber: '',
      email: '',
      position: '',
      hireDate: '',
      city: '',
      country: '',
      zipCode: '',
      houseNo: '',
      flatNo: '',
      province: '',
      department: '',
    });
  }

  populateForm(employee) {
    this.employeeForm.setValue({
      $key: 'edit',
      firstName: employee.data.firstName,
      lastName: employee.data.lastName,
      gender: employee.data.gender,
      birthDate: employee.data.birthDate,
      phoneNumber: employee.data.phoneNumber,
      email: employee.email,
      position: employee.position ? employee.position.id : null,
      hireDate: employee.hireDate,
      city: employee.data.city,
      zipCode: employee.data.zipCode,
      houseNo: employee.data.houseNo,
      flatNo: employee.data.flatNo,
      province: employee.data.province,
      department: employee.department ? employee.department.id : null,
    });
  }

  getAllDetailed(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + 'company/users/detailed').pipe(
      map(res => res.map(User.adapt))
    );
  }

  getAllGeneralized(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + 'company/users/generalized').pipe(
      map(res => res.map(User.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<User>(this.apiUrl + 'company/users/' + id).pipe(
      map(res => User.adapt(res))
    );
  }

  anonymizeById(id: number) {
    return this.http.post(this.apiUrl + 'company/users/' + id + '/anonymize', 'anonymize').pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zanonimizowano pracownika.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas anonimizacji pracownika.');
        return err;
      })
    );
  }

  createEmployee(employee) {
    return this.http.post(this.apiUrl + 'company/users', employee).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie dodano nowego pracownika.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas dodawania pracownika.');
        return err;
      })
    );
  }

  updateEmployee(id: number, employee) {
    console.log(id)
    return this.http.put(this.apiUrl + 'company/users/' + id, employee).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano pracownika.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji pracownika.');
        return err;
      })
    );
  }
}
