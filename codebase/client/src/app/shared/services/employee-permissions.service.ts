import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {catchError, map, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {UserPermissions} from '../models/user-permissions.model';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeePermissionsService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  employeePermissionsForm = this.formBuilder.group({
    canViewClients: ['', Validators.required],
    canEditClients: ['', Validators.required],
    canViewProjects: ['', Validators.required],
    canEditProjects: ['', Validators.required],
    canViewItems: ['', Validators.required],
    canEditItems: ['', Validators.required],
    canViewWorkTimeReports: ['', Validators.required],
    canViewCompany: ['', Validators.required],
    canEditCompany: ['', Validators.required],
    canEditEmployees: ['', Validators.required],
    canViewEmployees: ['', Validators.required],
    canServeApplications: ['', Validators.required],
    canViewApplications: ['', Validators.required],
    canEditEvents: ['', Validators.required],
    canViewEvents: ['', Validators.required],
    canPublishNews: ['', Validators.required],
  });

  initializeFormGroup() {
    this.employeePermissionsForm.setValue({
      canViewClients: '',
      canEditClients: '',
      canViewProjects: '',
      canEditProjects: '',
      canViewItems: '',
      canEditItems: '',
      canViewWorkTimeReports: '',
      canViewCompany: '',
      canEditCompany: '',
      canEditEmployees: '',
      canViewEmployees: '',
      canServeApplications: '',
      canViewApplications: '',
      canEditEvents: '',
      canViewEvents: '',
      canPublishNews: '',
    });
  }

  populateForm(employeePermissions) {
    this.employeePermissionsForm.setValue({
      canViewClients: employeePermissions.canViewClients,
      canEditClients: employeePermissions.canEditClients,
      canViewProjects: employeePermissions.canViewProjects,
      canEditProjects: employeePermissions.canEditProjects,
      canViewItems: employeePermissions.canViewItems,
      canEditItems: employeePermissions.canEditItems,
      canViewWorkTimeReports: employeePermissions.canViewWorkTimeReports,
      canViewCompany: employeePermissions.canViewCompany,
      canEditCompany: employeePermissions.canEditCompany,
      canEditEmployees: employeePermissions.canEditEmployees,
      canViewEmployees: employeePermissions.canViewEmployees,
      canServeApplications: employeePermissions.canServeApplications,
      canViewApplications: employeePermissions.canServeApplications,
      canEditEvents: employeePermissions.canEditEvents,
      canViewEvents: employeePermissions.canViewEvents,
      canPublishNews: employeePermissions.canPublishNews,
    });
  }

  getByEmployee(id: number) {
    return this.http.get<UserPermissions>(this.apiUrl + 'company/users/' + id + '/permissions').pipe(
      map(res => UserPermissions.adapt(res))
    );
  }

  updateEmployeePermissions(id: number, permissions) {
    return this.http.put(this.apiUrl + 'company/users/' + id + '/permissions', permissions).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano uprawnienia pracownika.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji uprawnień pracownika.');
        return err;
      })
    );
  }
}
