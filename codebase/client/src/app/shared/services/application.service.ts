import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Application} from '../models/application.model';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private notificationService: NotificationService) {
  }

  getAllByCurrentUser(status: string): Observable<Application[]> {
    return this.http.get<Application[]>(this.apiUrl + 'user/applications/' + status).pipe(
      map(res => res.map(Application.adapt))
    );
  }

  getAll(status: string): Observable<Application[]> {
    return this.http.get<Application[]>(this.apiUrl + 'company/applications/' + status).pipe(
      map(res => res.map(Application.adapt))
    );
  }

  getByEmployee(id: number, status: string = null): Observable<Application[]> {
    let url;

    if (status !== null) {
      url = this.apiUrl + 'user/' + id + '/applications/' + status;
    } else {
      url = this.apiUrl + 'user/' + id + '/applications/';
    }

    return this.http.get<Application[]>(url).pipe(
      map(res => res.map(Application.adapt))
    );
  }

  sendApplication(application) {
    return this.http.post(this.apiUrl + 'user/applications/send', application).pipe(
      tap(res => {
        this.notificationService.success('Poprawnie złożono wniosek do rozpatrzenia.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas składania wniosku. Spróbuj ponownie za chwilę.');
        return err;
      })
    );
  }

  acceptById(id: number) {
    return this.http.post(this.apiUrl + 'applications/' + id + '/accept', 'acceptApplication').pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaakceptowano wniosek.');
      }),
      catchError(err => {
        this.notificationService.success('Wystąpił błąd podczas akceptowania wniosku.');
        return err;
      })
    );
  }

  rejectById(id: number, rejectionReason) {
    return this.http.post(this.apiUrl + 'applications/' + id + '/reject', rejectionReason).pipe(
      tap(res => {
        this.notificationService.success('Wniosek został poprawnie odrzucony.');
      }),
      catchError(err => {
        this.notificationService.success('Wystąpił błąd podczas odrzucania wniosku.');
        return err;
      })
    );
  }
}

