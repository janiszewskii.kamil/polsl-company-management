import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Dashboard} from '../models/dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getData() {
    return this.http.get<Dashboard>(this.apiUrl + 'company/dashboard').pipe(
      map(res => Dashboard.adapt(res))
    );
  }
}
