import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Position} from '../models/position.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyPositionService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  companyPositionForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    description: ['', null],
    isManagerialPosition: ['', null],
  });

  initializeFormGroup() {
    this.companyPositionForm.setValue({
      $key: null,
      name: '',
      description: '',
      isManagerialPosition: '',
    });
  }

  populateForm(companyPosition) {
    this.companyPositionForm.setValue({
      $key: 'edit',
      name: companyPosition.name,
      description: companyPosition.description,
      isManagerialPosition: companyPosition.isManagerialPosition,
    });
  }

  getAll(): Observable<Position[]> {
    return this.http.get<Position[]>(this.apiUrl + 'company/positions').pipe(
      map(res => res.map(Position.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Position>(this.apiUrl + 'company/positions/' + id).pipe(
      map(res => Position.adapt(res))
    );
  }

  createPosition(companyPosition) {
    return this.http.post(this.apiUrl + 'company/positions', companyPosition).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono stanowisko.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia stanowiska.');
        return err;
      })
    );
  }

  updatePosition(id: number, companyPosition) {
    return this.http.put(this.apiUrl + 'company/positions/' + id, companyPosition).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano stanowisko.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji stanowiska.');
        return err;
      })
    );
  }

  removePosition(id: number) {
    return this.http.delete(this.apiUrl + 'company/positions/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto stanowisko.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas usuwania stanowiska.');
        return err;
      })
    );
  }
}
