import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../components/confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) {
  }

  openConfirmDialog(message) {
    return this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '30%',
      panelClass: 'confirm-dialog-container',
      disableClose: true,
      data: {msg: message}
    });
  }

}
