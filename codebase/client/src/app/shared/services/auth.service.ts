import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, tap, catchError} from 'rxjs/operators';
import {LocalStorageKeys} from '../models/local-storage.model';
import {LoginForm} from '../models/login-form.model';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import {UserService} from './user.service';
import {LoginResponse} from '../models/login-response.model';
import {NotificationService} from './notification.service';

@Injectable({providedIn: 'root'})
export class AuthService {
  authUser$: BehaviorSubject<User> = new BehaviorSubject<User>(new User());
  private isLoggedInSubject = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this.isLoggedInSubject.asObservable();
  apiUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private notificationService: NotificationService
  ) {
    const authUser = this.userService.getCurrentUser();
    if (authUser && authUser.data) {
      this.authUser$.next(authUser);
      this.isLoggedInSubject.next(true);
    }
  }

  login(credentials: LoginForm) {
    this.clearUserData();
    return this.http.post(this.apiUrl + 'auth/login', credentials).pipe(
      map(response => LoginResponse.adapt(response)),
      tap(response => localStorage.setItem(LocalStorageKeys.Token, JSON.stringify(response.token))),
      tap(response => {
        if (response) {
          localStorage.setItem(LocalStorageKeys.CurrentUser, JSON.stringify(response.user));
          this.authUser$.next(response.user);
          this.isLoggedInSubject.next(true);
        }
      })
    );
  }

  resetPassword(resetEmail) {
    this.clearUserData();
    return this.http.post(this.apiUrl + 'auth/reset-password', resetEmail).pipe(
      tap(res => {
        this.notificationService.success('Potwierdzenie zmiany hasła zostało wysłane na Twoją skrzynkę e-mail.');
        this.router.navigate(['/login']);
      }),
      catchError(err => {

        if (err.error.message === 'emailNotFound') {
          this.notificationService.warn('Nie znaleziono konta z takim adresm e-mail.');
          return err;
        }

        this.notificationService.warn('Wystąpił problem podczas próby wysłania potwierdzenia zmiany hasła.');
        return err;
      })
    );
  }

  logout() {
    return this.http.post(this.apiUrl + 'auth/logout', {message: 'logout'})
      .pipe(
        tap(res => {
          this.clearUserData();
          this.isLoggedInSubject.next(false);
          this.notificationService.success('Zostałeś poprawnie wylogowany.');
          this.router.navigate(['/login']);
        }),
        catchError(err => {
          this.notificationService.warn('Wystąpił problem z wylogowaniem.');
          return err;
        })
      );
  }

  clearUserData() {
    localStorage.removeItem(LocalStorageKeys.Token);
    localStorage.removeItem(LocalStorageKeys.CurrentUser);
  }
}
