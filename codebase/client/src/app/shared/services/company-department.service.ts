import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Department} from '../models/department.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyDepartmentService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  companyDepartmentForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    description: ['', null],
    manager: ['', null]
  });

  initializeFormGroup() {
    this.companyDepartmentForm.setValue({
      $key: null,
      name: '',
      description: '',
      manager: '',
    });
  }

  populateForm(companyDepartment) {
    this.companyDepartmentForm.setValue({
      $key: 'edit',
      name: companyDepartment.name,
      description: companyDepartment.description,
      manager: companyDepartment.manager ? companyDepartment.manager.id : null,
    });
  }

  getAll(): Observable<Department[]> {
    return this.http.get<Department[]>(this.apiUrl + 'company/departments').pipe(
      map(res => res.map(Department.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Department>(this.apiUrl + 'company/departments/' + id).pipe(
      map(res => Department.adapt(res))
    );
  }

  createDepartment(department) {
    return this.http.post(this.apiUrl + 'company/departments', department).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono nowy dział.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia działu.');
        return err;
      })
    );
  }

  updateDepartment(id: number, department) {
    return this.http.put(this.apiUrl + 'company/departments/' + id, department).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano dział.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji działu.');
        return err;
      })
    );
  }

  removeDepartment(id: number) {
    return this.http.delete(this.apiUrl + 'company/departments/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto dział.');
      }),
      catchError(err => {
        if (err.error.message === 'openedApplicationsForDepartment') {
          this.notificationService.warn('Nie możesz usunąć tego działu, ponieważ są do niego złożone nieobsłużone wnioski pracowników.');
        } else {
          this.notificationService.warn('Wystąpił problem podczas usuwania działu.');
        }
        return err;
      })
    );
  }
}
