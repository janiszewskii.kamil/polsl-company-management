import {Injectable} from '@angular/core';
import {catchError, map, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Message} from '../models/message.model';
import {Observable} from 'rxjs';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient,
              private notificationService: NotificationService) {
  }

  getAllSent(): Observable<Message[]> {
    return this.http.get<Message[]>(this.apiUrl + 'user/messages/sent').pipe(
      map(res => res.map(Message.adapt))
    );
  }

  getAllReceived(): Observable<Message[]> {
    return this.http.get<Message[]>(this.apiUrl + 'user/messages/received').pipe(
      map(res => res.map(Message.adapt))
    );
  }

  sendMessage(message, employee) {
    return this.http.post(this.apiUrl + 'user/messages/send', message).pipe(
      tap(res => {
        this.notificationService.success('Wysłano wiadomość do ' + employee.data.firstName + ' ' + employee.data.lastName + '.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas wysyłania wiadomości.');
        return err;
      })
    );
  }
}
