import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {catchError, map, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Project} from '../models/project.model';
import {ProjectStatistics} from '../models/project-statistics.model';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  apiUrl = environment.apiUrl;
  project: Project;

  constructor(private notificationService: NotificationService,
              private http: HttpClient,
              private formBuilder: FormBuilder) {
  }

  projectForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    code: [''],
    client: [''],
    description: [''],
    manager: [''],
    startDate: ['', Validators.required],
    plannedEndDate: ['', Validators.required],
    plannedBudget: ['', Validators.required],
    plannedIncome: ['', Validators.required],
  });

  initializeFormGroup() {
    this.projectForm.setValue({
      $key: null,
      name: '',
      code: '',
      client: '',
      description: '',
      manager: '',
      startDate: '',
      plannedEndDate: '',
      plannedBudget: '',
      plannedIncome: '',
    });
  }

  populateForm(project) {
    this.projectForm.setValue({
      $key: 'edit',
      name: project.name,
      code: project.code,
      client: project.client ? project.client.id : null,
      description: project.description,
      manager: project.manager ? project.manager.id : null,
      startDate: project.startDate,
      plannedEndDate: project.plannedEndDate,
      plannedBudget: project.plannedBudget,
      plannedIncome: project.plannedIncome,
    });
  }

  getAll() {
    return this.http.get<Project[]>(this.apiUrl + 'company/projects').pipe(
      map(res => res.map(Project.adapt))
    );
  }

  getAllOpened() {
    return this.http.get<Project[]>(this.apiUrl + 'company/projects/opened').pipe(
      map(res => res.map(Project.adapt))
    );
  }

  getAllClosed() {
    return this.http.get<Project[]>(this.apiUrl + 'company/projects/closed').pipe(
      map(res => res.map(Project.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Project>(this.apiUrl + 'company/projects/' + id).pipe(
      map(res => Project.adapt(res))
    );
  }

  getStatisticsById(id: number) {
    return this.http.get<ProjectStatistics>(this.apiUrl + 'company/projects/' + id + '/statistics').pipe(
      map(res => ProjectStatistics.adapt(res))
    );
  }

  closeProject(id: number, project) {
    return this.http.post(this.apiUrl + 'company/projects/' + id + '/close', project).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zamknięto projekt.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił błąd podczas zamykania projektu.');
        return err;
      })
    );
  }

  createProject(project) {
    return this.http.post(this.apiUrl + 'company/projects', project).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono projekt.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia projektu.');
        return err;
      })
    );
  }

  updateProject(id: number, project) {
    return this.http.put(this.apiUrl + 'company/projects/' + id, project).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano projekt.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas edycji projektu.');
        return err;
      })
    );
  }
}
