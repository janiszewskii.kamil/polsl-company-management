import {Injectable} from '@angular/core';
import {CompanyNews} from '../models/company-news.model';
import {catchError, map, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NotificationService} from './notification.service';
import {FormBuilder, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient,
              private formBuilder: FormBuilder,
              private notificationService: NotificationService) {
  }

  newsForm = this.formBuilder.group({
    $key: [null],
    title: ['', Validators.required],
    type: ['', Validators.required],
    text: ['', Validators.required],
  });

  initializeFormGroup() {
    this.newsForm.setValue({
      $key: null,
      text: '',
      title: '',
      type: '',
    });
  }

  populateForm(companyNews) {
    this.newsForm.setValue({
      $key: 'edit',
      title: companyNews.title,
      text: companyNews.text,
      type: companyNews.type,
    });
  }

  getAll(): Observable<CompanyNews[]> {
    return this.http.get<CompanyNews[]>(this.apiUrl + 'company/news').pipe(
      map(res => res.map(CompanyNews.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<CompanyNews>(this.apiUrl + 'company/news/' + id).pipe(
      map(res => CompanyNews.adapt(res))
    );
  }

  createNews(news) {
    return this.http.post(this.apiUrl + 'company/news', news).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie dodano wiadomość firmową.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia wiadomości firmowej.');
        return err;
      })
    );
  }

  updateNews(id, news) {
    return this.http.put(this.apiUrl + 'company/news/' + id, news).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano wiadomość firmową.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas edycji wiadomości firmowej.');
        return err;
      })
    );
  }

  removeNews(id: number) {
    return this.http.delete(this.apiUrl + 'company/news/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto wiadomość firmową.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas usuwania wiadomości firmowej.');
        return err;
      })
    );
  }
}
