import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {LocalStorageKeys} from '../models/local-storage.model';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../models/user.model';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private notificationService: NotificationService) {
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem(LocalStorageKeys.CurrentUser));
  }

  getProfile() {
    return this.http.get<User>(this.apiUrl + 'user/account').pipe(
      map(res => User.adapt(res))
    );
  }

  updateProfile(user: User) {
    return this.http.put(this.apiUrl + 'user/account', user).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano dane konta.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji danych konta.');
        return err;
      })
    );
  }

  changePassword(data) {
    return this.http.post(this.apiUrl + 'user/account/change-password', data).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano hasło do konta.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji hasła do konta.');
        return err;
      })
    );
  }
}
