import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Event} from '../models/event.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  eventForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    startDate: ['', Validators.required],
    endDate: ['', Validators.required],
    participant: [''],
    place: [''],
    time: [''],
  });

  initializeFormGroup() {
    this.eventForm.setValue({
      $key: null,
      name: '',
      startDate: '',
      endDate: '',
      participant: '',
      place: '',
      time: ''
    });
  }

  populateForm(event) {
    this.eventForm.setValue({
      $key: 'edit',
      name: event.name,
      startDate: event.startDate,
      endDate: event.endDate,
      participant: event.participant.id,
      place: event.place,
      time: event.time
    });
  }

  getAll(): Observable<Event[]> {
    return this.http.get<Event[]>(this.apiUrl + 'company/events').pipe(
      map(res => res.map(Event.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Event>(this.apiUrl + 'company/events/' + id).pipe(
      map(res => Event.adapt(res))
    );
  }

  createEvent(event) {
    return this.http.post(this.apiUrl + 'company/events', event).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono wydarzenie.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia wydarzenia.');
        return err;
      })
    );
  }

  getAllByEmployee(id: number): Observable<Event[]> {
    return this.http.get<Event[]>(this.apiUrl + 'user/' + id + '/events').pipe(
      map(res => res.map(Event.adapt))
    );
  }

  getAllByCurrentUser(): Observable<Event[]> {
    return this.http.get<Event[]>(this.apiUrl + 'user/events').pipe(
      map(res => res.map(Event.adapt))
    );
  }

  updateEvent(id: number, event) {
    return this.http.put(this.apiUrl + 'company/events/' + id, event).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano wydarzenie.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji wydarzenia.');
        return err;
      })
    );
  }

  cancelById(id: number) {
    return this.http.post(this.apiUrl + 'events/' + id + '/cancel', 'cancelEvent').pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie odwołano wydarzenie.');
      }),
      catchError(err => {
        this.notificationService.success('Wystąpił błąd podczas anulowania wydarzenia.');
        return err;
      })
    );
  }
}
