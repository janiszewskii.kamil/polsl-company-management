import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Item} from '../models/item.model';
import {environment} from '../../../environments/environment';
import {NotificationService} from './notification.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  apiUrl = environment.apiUrl;

  constructor(private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  itemForm = this.formBuilder.group({
    $key: [null],
    name: ['', Validators.required],
    description: ['', null],
    warrantyEndDate: ['', null],
    typeOfOwnership: ['', null],
    department: ['', null],
    employee: ['', null],
  });

  initializeFormGroup() {
    this.itemForm.setValue({
      $key: null,
      name: '',
      description: '',
      warrantyEndDate: '',
      typeOfOwnership: 'own',
      department: '',
      employee: '',
    });
  }

  populateForm(item) {
    this.itemForm.setValue({
      $key: 'edit',
      name: item.name,
      description: item.description,
      warrantyEndDate: item.warrantyEndDate,
      typeOfOwnership: item.typeOfOwnership,
      department: item.department ? item.department.id : null,
      employee: item.employee ? item.employee.id : null,
    });
  }

  getAll(): Observable<Item[]> {
    return this.http.get<Item[]>(this.apiUrl + 'company/items').pipe(
      map(res => res.map(Item.adapt))
    );
  }

  getById(id: number) {
    return this.http.get<Item>(this.apiUrl + 'company/items/' + id).pipe(
      map(res => Item.adapt(res))
    );
  }

  createItem(item) {
    return this.http.post(this.apiUrl + 'company/items', item).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie utworzono wpis do wyposażenia.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas tworzenia wpisu wyposażenia.');
        return err;
      })
    );
  }

  updateItem(id: number, item) {
    return this.http.put(this.apiUrl + 'company/items/' + id, item).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie zaktualizowano wpis wyposażenia.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas aktualizacji wpisu wyposażenia.');
        return err;
      })
    );
  }

  removeItem(id: number) {
    return this.http.delete(this.apiUrl + 'company/items/' + id).pipe(
      tap(res => {
        this.notificationService.success('Pomyślnie usunięto wpis wyposażenia.');
      }),
      catchError(err => {
        this.notificationService.warn('Wystąpił problem podczas usuwania wpisu wyposażenia.');
        return err;
      })
    );
  }
}
