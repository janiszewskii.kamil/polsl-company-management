import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './shared/helpers/guards/auth.guard';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {MainComponent} from './components/main/main.component';
import {AccountSettingsComponent} from './components/account-settings/account-settings.component';
import {EmployeesListComponent} from './components/employees/employees-list/employees-list.component';
import {CompanyManagementComponent} from './components/company-management/company-management.component';
import {ProjectsMainComponent} from './components/projects/projects-main/projects-main.component';
import {MessagesMainComponent} from './components/messages/messages-main/messages-main.component';
import {ApplicationsListComponent} from './components/applications/applications-list/applications-list.component';
import {UserApplicationsComponent} from './components/applications/user-applications/user-applications.component';
import {UserWorkTimeReportsListComponent} from './components/work-time-reports/user-work-time-reports-list/user-work-time-reports-list.component';
import {WorkTimeReportsListComponent} from './components/work-time-reports/work-time-reports-list/work-time-reports-list.component';
import {EmployeeComponent} from './components/employees/employee/employee.component';
import {ProjectComponent} from './components/projects/project/project.component';
import {EventsListComponent} from './components/events/events-list/events-list.component';
import {PageForbiddenComponent} from './error-pages/403/page-forbidden/page-forbidden.component';
import {PageNotFoundComponent} from './error-pages/404/page-not-found.component';
import {UserEventsListComponent} from './components/events/user-events-list/user-events-list.component';
import {ClientsListComponent} from './components/clients/clients-list/clients-list.component';
import {ItemsListComponent} from './components/items/items-list/items-list.component';
import {ResetPasswordComponent} from './components/auth/reset-password/reset-password.component';
import {LoginComponent} from './components/auth/login/login.component';

const routes: Routes = [
  {
    path: '', component: MainComponent, canActivateChild: [AuthGuard],
    children: [
      {path: '', component: DashboardComponent},
      {path: 'company/dashboard', component: DashboardComponent},
      {path: 'company/work-time-reports', component: WorkTimeReportsListComponent},
      {path: 'company/employees', component: EmployeesListComponent},
      {path: 'company/projects', component: ProjectsMainComponent},
      {path: 'company/settings', component: CompanyManagementComponent},
      {path: 'company/applications', component: ApplicationsListComponent},
      {path: 'company/events', component: EventsListComponent},
      {path: 'company/clients', component: ClientsListComponent},
      {path: 'company/items', component: ItemsListComponent},
      {path: 'user/work-time-reports', component: UserWorkTimeReportsListComponent},
      {path: 'user/messages', component: MessagesMainComponent},
      {path: 'user/account-settings', component: AccountSettingsComponent},
      {path: 'user/applications', component: UserApplicationsComponent},
      {path: 'user/events', component: UserEventsListComponent},
      {path: 'company/employees/:id', component: EmployeeComponent},
      {path: 'company/projects/:id', component: ProjectComponent},
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'reset-password', component: ResetPasswordComponent},
  {path: '403', component: PageForbiddenComponent},
  {path: '404', component: PageNotFoundComponent},
  {path: '**', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
