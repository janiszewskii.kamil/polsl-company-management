import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {WorkTimeReportService} from '../../../shared/services/work-time-report.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-working-time-reports-list',
  templateUrl: './work-time-reports-list.component.html'
})
export class WorkTimeReportsListComponent implements OnInit {
  displayedColumns: string[] = ['createdBy', 'project', 'message', 'time', 'type', 'reportForDate', 'createdAt', 'updatedAt'];
  workTimeRecordsDataSource: MatTableDataSource<any>;
  subscriptions = new Subscription();
  searchKey: string;

  constructor(private workTimeReportService: WorkTimeReportService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.initializeData();
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.workTimeRecordsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  initializeData(): void {
    this.subscriptions.add(this.workTimeReportService.getAll().subscribe(
      response => {
        this.workTimeRecordsDataSource = new MatTableDataSource(response);
        this.workTimeRecordsDataSource.sort = this.sort;
        this.workTimeRecordsDataSource.paginator = this.paginator;
      }));
  }
}
