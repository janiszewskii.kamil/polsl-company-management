import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {WorkTimeReportDialogComponent} from '../work-time-report-dialog/work-time-report-dialog.component';
import {MatPaginator} from '@angular/material/paginator';
import {Subscription} from 'rxjs';
import {WorkTimeReportService} from '../../../shared/services/work-time-report.service';
import {DialogService} from '../../../shared/services/dialog.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-user-working-time-reports',
  templateUrl: './user-work-time-reports-list.component.html'
})
export class UserWorkTimeReportsListComponent implements OnInit, OnDestroy {
  userWorkTimeRecordsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['project', 'message', 'type', 'time', 'reportForDate', 'createdAt', 'updatedAt', 'actions'];
  subscriptions = new Subscription();
  searchKey: string;

  constructor(private dialog: MatDialog,
              private workTimeReportService: WorkTimeReportService,
              private dialogService: DialogService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.initData();
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.userWorkTimeRecordsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  initData(): void {
    this.subscriptions.add(this.workTimeReportService.getAllByCurrentUser().subscribe(
      response => {
        this.userWorkTimeRecordsDataSource = new MatTableDataSource(response);
        this.userWorkTimeRecordsDataSource.sort = this.sort;
        this.userWorkTimeRecordsDataSource.paginator = this.paginator;
      }));
  }

  onCreateReportDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.minWidth = '40%';
    dialogConfig.maxWidth = '70%';
    this.dialog.open(WorkTimeReportDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateReportDialog(row): void {
    this.workTimeReportService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      workTimeReportId: row.id
    };
    this.dialog.open(WorkTimeReportDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemoveReport(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć raport czasu pracy za dzień ' + formatDate(row.reportForDate, 'd/MM/yyyy', 'pl-PL') + '? Proces jest nieodwracalny.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.workTimeReportService.removeWorkTimeReport(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
