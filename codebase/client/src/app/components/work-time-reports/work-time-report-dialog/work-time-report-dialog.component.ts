import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {WorkTimeReportService} from '../../../shared/services/work-time-report.service';
import {ProjectService} from '../../../shared/services/project.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-report',
  templateUrl: './work-time-report-dialog.component.html'
})
export class WorkTimeReportDialogComponent implements OnInit, OnDestroy {
  projects$;
  submitted = false;
  subscriptions = new Subscription();
  workTimeReportId: number;

  constructor(
    private workTimeReportService: WorkTimeReportService,
    private projectService: ProjectService,
    private dialogRef: MatDialogRef<WorkTimeReportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) dialogData
  ) {
    if (dialogData) {
      this.workTimeReportId = dialogData.workTimeReportId;
    }
  }

  ngOnInit() {
    this.projects$ = this.projectService.getAll();
  }

  onClear() {
    this.workTimeReportService.workReportTimeForm.reset();
    this.workTimeReportService.initializeFormGroup();
  }

  onClose() {
    this.projectService.projectForm.reset();
    this.projectService.initializeFormGroup();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.workTimeReportService.workReportTimeForm.valid) {
      if (!this.workTimeReportService.workReportTimeForm.get('$key').value) {
        this.subscriptions.add(this.workTimeReportService.createWorkTimeReport(this.workTimeReportService.workReportTimeForm.value).subscribe(
          (res) => this.submitted = true
        ));
      } else {
        this.subscriptions.add(this.workTimeReportService.updateWorkTimeReport(this.workTimeReportId, this.workTimeReportService.workReportTimeForm.value).subscribe(
          (res) => this.submitted = true
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
