import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ItemService} from '../../../shared/services/item.service';
import {EmployeeService} from '../../../shared/services/employee.service';
import {CompanyDepartmentService} from '../../../shared/services/company-department.service';

@Component({
  selector: 'app-items-dialog',
  templateUrl: './items-dialog.component.html'
})
export class ItemsDialogComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  itemId?: number;
  employees$;
  departments$;

  constructor(private dialogRef: MatDialogRef<ItemsDialogComponent>,
              private itemService: ItemService,
              private employeeService: EmployeeService,
              private departmentService: CompanyDepartmentService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.itemId = dialogData.itemId;
    }
  }

  ngOnInit() {
    this.initData();
  }

  initData(): void {
    this.departments$ = this.departmentService.getAll();
    this.employees$ = this.employeeService.getAllGeneralized();
  }

  onClear() {
    this.itemService.itemForm.reset();
    this.itemService.initializeFormGroup();
  }

  onClose() {
    this.dialogRef.close();
    this.itemService.itemForm.reset();
    this.itemService.initializeFormGroup();
  }

  onSubmit() {
    if (this.itemService.itemForm.valid) {
      if (!this.itemService.itemForm.get('$key').value) {
        this.subscriptions.add(this.itemService.createItem(this.itemService.itemForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      } else {
        this.subscriptions.add(this.itemService.updateItem(this.itemId, this.itemService.itemForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
