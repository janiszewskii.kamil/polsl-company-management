import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {ItemService} from '../../../shared/services/item.service';
import {ItemsDialogComponent} from '../items-dialog/items-dialog.component';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html'
})
export class ItemsListComponent implements OnInit, OnDestroy {
  itemsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'description', 'department', 'employee', 'typeOfOwnership', 'warrantyEndDate', 'actions'];
  searchKey: string;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(
    private itemService: ItemService,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private userService: UserService
  ) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.subscriptions.add(this.itemService.getAll().subscribe(
      (response) => {
        this.itemsDataSource = new MatTableDataSource(response);
        this.itemsDataSource.sort = this.sort;
        this.itemsDataSource.paginator = this.paginator;
      }));
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.itemsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onCreateItemDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    this.dialog.open(ItemsDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateItemDialog(row): void {
    this.itemService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      itemId: row.id
    };
    this.dialog.open(ItemsDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemoveItem(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć przedmiot ' + row.name + '? Proces jest nieodwracalny.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.itemService.removeItem(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
