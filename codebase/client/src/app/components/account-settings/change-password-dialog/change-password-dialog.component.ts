import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {UserService} from '../../../shared/services/user.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html'
})
export class ChangePasswordDialogComponent implements OnInit, OnDestroy {
  changePasswordForm: FormGroup;
  submitted = false;
  subscriptions = new Subscription();

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
  ) {
  }

  ngOnInit() {
    this.populateChangePasswordForm();
  }

  populateChangePasswordForm(): void {
    this.changePasswordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      confirmNewPassword: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  onSubmit() {
    if (this.changePasswordForm.valid) {
      this.subscriptions.add(this.userService.changePassword(this.changePasswordForm.value).subscribe(
        (res) => {
          this.submitted = true;
        }
      ));
    }
    this.onClose();
  }

  onClear() {
    this.changePasswordForm.reset();
  }

  onClose() {
    this.submitted = false;
    this.onClear();
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}

