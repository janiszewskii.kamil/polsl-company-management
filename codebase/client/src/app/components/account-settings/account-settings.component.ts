import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';
import {User} from '../../shared/models/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ChangePasswordDialogComponent} from './change-password-dialog/change-password-dialog.component';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html'
})
export class AccountSettingsComponent implements OnInit, OnDestroy {
  currentUser: User;
  accountSettingsForm: FormGroup;
  subscriptions = new Subscription();
  responseReceivedFlag = false;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.subscriptions.add(this.userService.getProfile().subscribe((user: User) => {
      this.currentUser = user;
      this.populateForm();
    }));
  }

  populateForm(): void {
    this.accountSettingsForm = this.formBuilder.group({
      firstName: [this.currentUser.data.firstName, Validators.required],
      lastName: [this.currentUser.data.lastName, Validators.required],
      gender: [this.currentUser.data.gender, Validators.required],
      birthDate: [this.currentUser.data.birthDate],
      phoneNumber: [this.currentUser.data.phoneNumber, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      email: [this.currentUser.email, [Validators.required, Validators.email]],
      position: [this.currentUser.position.name],
      department: [this.currentUser.department.name],
      hireDate: [this.currentUser.hireDate],
      city: [this.currentUser.data.city],
      province: [this.currentUser.data.province],
      zipCode: [this.currentUser.data.zipCode],
      street: [this.currentUser.data.street],
      houseNo: [this.currentUser.data.houseNo],
      flatNo: [this.currentUser.data.flatNo],
    });
    this.accountSettingsForm.get('position').disable();
    this.accountSettingsForm.get('department').disable();
    this.accountSettingsForm.get('hireDate').disable();
  }

  onSubmit() {
    if (this.accountSettingsForm.valid) {
      this.subscriptions.add(this.userService.updateProfile(this.accountSettingsForm.value).subscribe(
        (user: User) => {
          this.responseReceivedFlag = true;
          this.currentUser = user;
          this.populateForm();
        }
      ));
    }
  }

  onClear() {
    this.accountSettingsForm.reset();
  }

  onChangePasswordDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30%';
    this.dialog.open(ChangePasswordDialogComponent, dialogConfig);
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
