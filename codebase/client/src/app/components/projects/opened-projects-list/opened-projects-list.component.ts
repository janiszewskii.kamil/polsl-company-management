import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ProjectDialogComponent} from '../project-dialog/project-dialog.component';
import {ProjectService} from '../../../shared/services/project.service';
import {Subscription} from 'rxjs';
import {ProjectCloseDialogComponent} from '../project-close-dialog/project-close-dialog.component';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-opened-projects-list',
  templateUrl: './opened-projects-list.component.html'
})
export class OpenedProjectsListComponent implements OnInit, OnDestroy {
  openedProjectsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'code', 'client', 'startDate', 'plannedEndDate', 'projectManager', 'plannedBudget', 'plannedIncome', 'actions'];
  subscriptions = new Subscription();
  searchKey: string;
  currentUser: User;

  constructor(private dialog: MatDialog,
              private projectService: ProjectService,
              private userService: UserService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.openedProjectsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  initData(): void {
    this.subscriptions.add(this.projectService.getAllOpened().subscribe(
      response => {
        this.openedProjectsDataSource = new MatTableDataSource(response);
        this.openedProjectsDataSource.sort = this.sort;
        this.openedProjectsDataSource.paginator = this.paginator;
      }));
  }

  onCreateProjectDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40%';
    dialogConfig.maxWidth = '80%';
    dialogConfig.maxHeight = '80%';
    this.dialog.open(ProjectDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateProjectDialog(row): void {
    this.projectService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40%';
    dialogConfig.maxWidth = '80%';
    dialogConfig.maxHeight = '80%';
    dialogConfig.data = {
      projectId: row.id
    };
    this.dialog.open(ProjectDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onCloseProject(row): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '50%';
    dialogConfig.data = {
      project: row
    };
    this.dialog.open(ProjectCloseDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
