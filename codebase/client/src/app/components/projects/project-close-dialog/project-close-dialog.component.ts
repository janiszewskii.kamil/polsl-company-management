import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '../../../shared/services/project.service';
import {Subscription} from 'rxjs';
import {Project} from '../../../shared/models/project.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-project-close-dialog',
  templateUrl: './project-close-dialog.component.html'
})
export class ProjectCloseDialogComponent implements OnInit, OnDestroy {
  project: Project;
  projectCloseForm: FormGroup;
  subscriptions = new Subscription();
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private projectService: ProjectService,
              private dialogRef: MatDialogRef<ProjectCloseDialogComponent>,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.project = dialogData.project;
    }
  }

  ngOnInit() {
    this.projectCloseForm = this.formBuilder.group({
      endDate: ['', Validators.required],
      finalIncome: ['', Validators.required],
      spentBudget: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.projectCloseForm.valid) {
      this.subscriptions.add(this.projectService.closeProject(this.project.id, this.projectCloseForm.value).subscribe(
        (res) => this.submitted = true
      ));
      this.onClose();
    }
  }

  onClear(): void {
    this.projectCloseForm.reset();
  }

  onClose() {
    this.dialogRef.close();
    this.submitted = false;
    this.projectCloseForm.reset();
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
