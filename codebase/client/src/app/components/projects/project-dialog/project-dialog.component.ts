import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ProjectService} from '../../../shared/services/project.service';
import {MatDialogRef} from '@angular/material/dialog';
import {CompanyClientService} from '../../../shared/services/company-client.service';
import {EmployeeService} from '../../../shared/services/employee.service';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html'
})
export class ProjectDialogComponent implements OnInit, OnDestroy {
  companyClients$;
  employees$;
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  projectId: number;

  constructor(private clientService: CompanyClientService,
              private projectService: ProjectService,
              private employeeService: EmployeeService,
              private dialogRef: MatDialogRef<ProjectDialogComponent>,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.projectId = dialogData.projectId;
    }
  }

  ngOnInit() {
    this.companyClients$ = this.clientService.getAll();
    this.employees$ = this.employeeService.getAllGeneralized();
  }

  onClear() {
    this.projectService.projectForm.reset();
    this.projectService.initializeFormGroup();
  }

  onClose() {
    this.dialogRef.close();
    this.onClear();
  }

  onSubmit() {
    if (this.projectService.projectForm.valid) {
      if (!this.projectService.projectForm.get('$key').value) {
        this.subscriptions.add(this.projectService.createProject(this.projectService.projectForm.value).subscribe(
          (res) => this.responseReceivedFlag = true
        ));
      } else {
        this.subscriptions.add(this.projectService.updateProject(this.projectId, this.projectService.projectForm.value).subscribe(
          (res) => this.responseReceivedFlag = true
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
