import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ProjectService} from '../../../shared/services/project.service';
import {UserService} from '../../../shared/services/user.service';
import {User} from '../../../shared/models/user.model';

@Component({
  selector: 'app-closed-projects-list',
  templateUrl: './closed-projects-list.component.html'
})
export class ClosedProjectsListComponent implements OnInit, OnDestroy {
  closedProjectsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'code', 'client', 'startDate', 'endDate', 'projectManager', 'spentBudget', 'finalIncome', 'actions'];
  subscriptions = new Subscription();
  searchKey: string;
  currentUser: User;

  constructor(private dialog: MatDialog,
              private projectService: ProjectService,
              private userService: UserService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initializeData();
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.closedProjectsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  initializeData(): void {
    this.subscriptions.add(this.projectService.getAllClosed().subscribe(
      response => {
        this.closedProjectsDataSource = new MatTableDataSource(response);
        this.closedProjectsDataSource.sort = this.sort;
        this.closedProjectsDataSource.paginator = this.paginator;
      }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
