import {Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../../shared/models/project.model';
import {Subscription} from 'rxjs';
import {ProjectService} from '../../../shared/services/project.service';
import {ActivatedRoute} from '@angular/router';
import {WorkTimeReportService} from '../../../shared/services/work-time-report.service';
import {ProjectStatistics} from '../../../shared/models/project-statistics.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html'
})
export class ProjectComponent implements OnInit, OnDestroy {
  project: Project;
  projectStatistics: ProjectStatistics;
  projectWorkTimeReport$;
  subscriptions = new Subscription();

  constructor(private route: ActivatedRoute,
              private projectService: ProjectService,
              private workTimeRecordService: WorkTimeReportService) {
  }

  ngOnInit() {
    const projectId = Number(this.route.snapshot.paramMap.get('id'));
    this.subscriptions.add(this.projectService.getById(projectId).subscribe((project: Project) => {
      this.project = project;
      if (!project.isOpen) {
        this.subscriptions.add(this.projectService.getStatisticsById(projectId).subscribe((projectStatistics: ProjectStatistics) => {
          this.projectStatistics = projectStatistics;
        }));
      }
    }));
    this.projectWorkTimeReport$ = this.workTimeRecordService.getAllByProject(projectId);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
