import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {EventService} from '../../../shared/services/event.service';
import {EmployeeService} from '../../../shared/services/employee.service';
import {Subscription} from 'rxjs';
import {MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-event-dialog',
  templateUrl: './event-dialog.component.html'
})
export class EventDialogComponent implements OnInit, OnDestroy {
  employees$;
  eventId: number;
  subscriptions = new Subscription();
  submitted = false;
  privateEventEdit: boolean;

  constructor(private dialogRef: MatDialogRef<EventDialogComponent>,
              private eventService: EventService,
              private employeeService: EmployeeService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.eventId = dialogData.eventId;
      this.privateEventEdit = dialogData.privateEventEdit;
    }
  }

  ngOnInit() {
    this.employees$ = this.employeeService.getAllGeneralized();
  }

  onClose() {
    this.eventService.eventForm.reset();
    this.eventService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.eventService.eventForm.reset();
    this.eventService.initializeFormGroup();
  }

  onSubmit() {
    if (this.eventService.eventForm.valid) {
      if (!this.eventService.eventForm.get('$key').value) {
        this.subscriptions.add(this.eventService.createEvent(this.eventService.eventForm.value).subscribe(
          (res) => {
            this.submitted = true;
          }
        ));
      } else {
        this.subscriptions.add(this.eventService.updateEvent(this.eventId, this.eventService.eventForm.value).subscribe(
          (res) => {
            this.submitted = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
