import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {EventService} from '../../../shared/services/event.service';
import {EventDialogComponent} from '../event-dialog/event-dialog.component';
import {Event} from '../../../shared/models/event.model';

@Component({
  selector: 'app-user-events-list',
  templateUrl: './user-events-list.component.html'
})
export class UserEventsListComponent implements OnInit, OnDestroy {
  eventsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'startDate', 'endDate', 'time', 'place', 'createdBy', 'createdAt', 'updatedAt', 'updatedBy', 'isCanceled', 'actions'];
  searchKey: string;
  subscriptions = new Subscription();

  constructor(private dialogService: DialogService,
              private dialog: MatDialog,
              private eventService: EventService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.initData();
  }

  initData(): void {
    this.subscriptions.add(this.eventService.getAllByCurrentUser().subscribe(
      response => {
        this.eventsDataSource = new MatTableDataSource(response);
        this.eventsDataSource.sort = this.sort;
        this.eventsDataSource.paginator = this.paginator;
      }));
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.eventsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onCreateEventDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      privateEventEdit: true
    };
    this.dialog.open(EventDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateEventDialog(row): void {
    this.subscriptions.add(this.eventService.getById(row.id).subscribe((event: Event) => {
      this.eventService.populateForm(event);
    }));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      eventId: row.id,
      privateEventEdit: true
    };
    this.dialog.open(EventDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onCancelEvent(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz anulować to spotkanie? Ta akcje jest nieodwracalna.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.eventService.cancelById(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
