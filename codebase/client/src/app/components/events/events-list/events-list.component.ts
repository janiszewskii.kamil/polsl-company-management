import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {EventService} from '../../../shared/services/event.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {EventDialogComponent} from '../event-dialog/event-dialog.component';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-company-calendar',
  templateUrl: './events-list.component.html'
})
export class EventsListComponent implements OnInit, OnDestroy {
  eventsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'startDate', 'endDate', 'time', 'participant', 'place', 'createdBy', 'createdAt', 'updatedAt', 'updatedBy', 'isCanceled', 'actions'];
  searchKey: string;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(private dialogService: DialogService,
              private dialog: MatDialog,
              private userService: UserService,
              private eventService: EventService) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.subscriptions.add(this.eventService.getAll().subscribe(
      response => {
        this.eventsDataSource = new MatTableDataSource(response);
        this.eventsDataSource.sort = this.sort;
        this.eventsDataSource.paginator = this.paginator;
      }));
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.eventsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onCreateEventDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      privateEventEdit: false
    };
    this.dialog.open(EventDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateEventDialog(row): void {
    this.eventService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '70%';
    dialogConfig.data = {
      eventId: row.id,
      privateEventEdit: false
    };
    this.dialog.open(EventDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onCancelEvent(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz anulować to spotkanie? Ta akcje jest nieodwracalna.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.eventService.cancelById(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
