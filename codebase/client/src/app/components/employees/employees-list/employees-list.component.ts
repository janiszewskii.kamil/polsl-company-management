import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EmployeeService} from '../../../shared/services/employee.service';
import {UserService} from '../../../shared/services/user.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {EmployeeDialogComponent} from '../employee-dialog/employee-dialog.component';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MessageDialogComponent} from '../../messages/message-dialog/message-dialog.component';
import {EmployeePermissionsDialogComponent} from '../employee-permissions-dialog/employee-permissions-dialog.component';
import {EmployeePermissionsService} from '../../../shared/services/employee-permissions.service';
import {UserPermissions} from '../../../shared/models/user-permissions.model';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html'
})
export class EmployeesListComponent implements OnInit, OnDestroy {
  currentUser;
  employeesDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['email', 'firstName', 'lastName', 'position', 'department', 'phoneNumber', 'actions'];
  searchKey: string;
  subscriptions = new Subscription();

  constructor(
    private employeeService: EmployeeService,
    private employeePermissionsService: EmployeePermissionsService,
    private userService: UserService,
    private dialogService: DialogService,
    private dialog: MatDialog
  ) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.subscriptions.add(this.employeeService.getAllDetailed().subscribe(
      response => {
        this.employeesDataSource = new MatTableDataSource(response);
        this.employeesDataSource.sort = this.sort;
        this.employeesDataSource.paginator = this.paginator;
      }));
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.employeesDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onCreateEmployeeDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '50%';
    dialogConfig.maxWidth = '80%';
    this.dialog.open(EmployeeDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateEmployeeDialog(row): void {
    this.employeeService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '50%';
    dialogConfig.maxWidth = '80%';
    dialogConfig.data = {
      employeeId: row.id
    };
    this.dialog.open(EmployeeDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onPermissionsEmployeeDialog(row): void {
    let subscribeFlag = false;
    this.subscriptions.add(this.employeePermissionsService.getByEmployee(row.id).subscribe(
      (permissions: UserPermissions) => {
        this.employeePermissionsService.populateForm(permissions);
        subscribeFlag = true;
      }
    ));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '50%';
    dialogConfig.data = {
      employee: row
    };
    this.dialog.open(EmployeePermissionsDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onAnonymizeEmployee(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz anonimizować użytkownika ' + row.data.firstName + ' ' + row.data.lastName + ' ?'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.employeeService.anonymizeById(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  onMessageEmployee(row): void {
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      height: '500px',
      minWidth: '20%',
      data: {
        employeeData: row
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
