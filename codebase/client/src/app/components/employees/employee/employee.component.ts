import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../shared/models/user.model';
import {EmployeeService} from '../../../shared/services/employee.service';
import {ApplicationService} from '../../../shared/services/application.service';
import {WorkTimeReportService} from '../../../shared/services/work-time-report.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {EventService} from '../../../shared/services/event.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html'
})
export class EmployeeComponent implements OnInit, OnDestroy {
  employee: User;
  employeeApplications$;
  employeeEvents$;
  employeeWorkTimeReports$;
  subscriptions = new Subscription();

  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private eventService: EventService,
              private applicationService: ApplicationService,
              private workTimeRecordService: WorkTimeReportService) {
  }

  ngOnInit() {
    const employeeId = Number(this.route.snapshot.paramMap.get('id'));
    this.subscriptions.add(this.employeeService.getById(employeeId).subscribe((employee: User) => {
      this.employee = employee;
    }));
    this.employeeApplications$ = this.applicationService.getByEmployee(employeeId);
    this.employeeEvents$ = this.eventService.getAllByEmployee(employeeId);
    this.employeeWorkTimeReports$ = this.workTimeRecordService.getAllByEmployee(employeeId);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
