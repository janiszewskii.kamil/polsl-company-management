import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {EmployeeService} from '../../../shared/services/employee.service';
import {CompanyDepartmentService} from '../../../shared/services/company-department.service';
import {CompanyPositionService} from '../../../shared/services/company-position.service';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-employee-dialog',
  templateUrl: './employee-dialog.component.html'
})
export class EmployeeDialogComponent implements OnInit, OnDestroy {
  positions$;
  departments$;
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  employeeId?: number;

  constructor(private dialogRef: MatDialogRef<EmployeeDialogComponent>,
              private employeeService: EmployeeService,
              private departmentService: CompanyDepartmentService,
              private positionService: CompanyPositionService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.employeeId = dialogData.employeeId;
    }
  }

  ngOnInit() {
    this.departments$ = this.departmentService.getAll();
    this.positions$ = this.positionService.getAll();
  }

  onClear() {
    this.employeeService.employeeForm.reset();
    this.employeeService.initializeFormGroup();
  }

  onClose() {
    this.employeeService.employeeForm.reset();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.employeeService.employeeForm.valid) {
      if (!this.employeeService.employeeForm.get('$key').value) {
        this.subscriptions.add(this.employeeService.createEmployee(this.employeeService.employeeForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      } else {
        this.subscriptions.add(this.employeeService.updateEmployee(this.employeeId, this.employeeService.employeeForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
