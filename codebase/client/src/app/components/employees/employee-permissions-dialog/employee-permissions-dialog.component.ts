import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {User} from '../../../shared/models/user.model';
import {EmployeePermissionsService} from '../../../shared/services/employee-permissions.service';

@Component({
  selector: 'app-employee-permissions-dialog',
  templateUrl: './employee-permissions-dialog.component.html'
})
export class EmployeePermissionsDialogComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  employee: User;

  constructor(private employeePermissionsService: EmployeePermissionsService,
              private dialogRef: MatDialogRef<EmployeePermissionsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.employee = dialogData.employee;
    }
  }

  ngOnInit() {
  }

  onClear() {
    this.employeePermissionsService.employeePermissionsForm.reset();
    this.employeePermissionsService.initializeFormGroup();
  }

  onClose() {
    this.employeePermissionsService.employeePermissionsForm.reset();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.employeePermissionsService.employeePermissionsForm.valid) {
      this.subscriptions.add(this.employeePermissionsService.updateEmployeePermissions(
        this.employee.id, this.employeePermissionsService.employeePermissionsForm.value).subscribe(
        (res) => {
          this.responseReceivedFlag = true;
        }
      ));
    }
    this.onClose();
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
