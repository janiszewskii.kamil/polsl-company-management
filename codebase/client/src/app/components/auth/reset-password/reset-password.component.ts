import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AuthService} from '../../../shared/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  resetPasswordForm: FormGroup;
  resetPasswordSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      resetEmail: ['', [Validators.required, Validators.email]],
    });
  }

  resetPassword(): void {
    if (this.resetPasswordForm.valid) {
      this.resetPasswordSubscription = this.authService.resetPassword(this.resetPasswordForm.value).subscribe();
    }
  }

  ngOnDestroy() {
    if (this.resetPasswordSubscription) {
      this.resetPasswordSubscription.unsubscribe();
    }
  }
}
