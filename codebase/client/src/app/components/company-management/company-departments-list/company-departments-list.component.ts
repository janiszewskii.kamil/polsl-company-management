import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {CompanyDepartmentDialogComponent} from '../dialogs/company-department-dialog/company-department-dialog.component';
import {CompanyDepartmentService} from '../../../shared/services/company-department.service';
import {Department} from '../../../shared/models/department.model';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-company-departments-list',
  templateUrl: './company-departments-list.component.html'
})
export class CompanyDepartmentsListComponent implements OnInit, OnDestroy {
  departments$;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(private dialog: MatDialog,
              private departmentService: CompanyDepartmentService,
              private userService: UserService,
              private dialogService: DialogService) {
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData() {
    this.departments$ = this.departmentService.getAll();
  }

  onCreateDepartmentDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '20%';
    this.dialog.open(CompanyDepartmentDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateDepartmentDialog(id: number): void {
    this.subscriptions.add(this.departmentService.getById(id).subscribe((department: Department) => {
      this.departmentService.populateForm(department);
    }));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '20%';
    dialogConfig.data = {
      departmentId: id
    };
    this.dialog.open(CompanyDepartmentDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemoveDepartment(id: number, departmentName: string): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć dział ' + departmentName + '? Proces jest nieodwracalny.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.departmentService.removeDepartment(id).subscribe(
          () => this.initData()
        ));
      }
    });
  }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
