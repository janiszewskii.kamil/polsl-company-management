import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {CompanyPositionService} from '../../../../shared/services/company-position.service';
import {MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-company-position-dialog',
  templateUrl: './company-position-dialog.component.html'
})
export class CompanyPositionDialogComponent implements OnInit, OnDestroy {
  positionId?: number;
  subscriptions = new Subscription();
  responseReceivedFlag = false;

  constructor(private dialogRef: MatDialogRef<CompanyPositionDialogComponent>,
              private companyPositionService: CompanyPositionService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.positionId = dialogData.positionId;
    }
  }

  ngOnInit() {
  }

  onClear() {
    this.companyPositionService.companyPositionForm.reset();
  }

  onClose() {
    this.onClear();
    this.companyPositionService.initializeFormGroup();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.companyPositionService.companyPositionForm.valid) {
      if (!this.companyPositionService.companyPositionForm.get('$key').value) {
        this.subscriptions.add(this.companyPositionService.createPosition(this.companyPositionService.companyPositionForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      } else {
        this.subscriptions.add(this.companyPositionService.updatePosition(this.positionId, this.companyPositionService.companyPositionForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag
    ) {
      this.subscriptions.unsubscribe();
    }
  }
}
