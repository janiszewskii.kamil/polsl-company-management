import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {CompanyDepartmentService} from '../../../../shared/services/company-department.service';
import {MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {EmployeeService} from '../../../../shared/services/employee.service';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-company-department-dialog',
  templateUrl: './company-department-dialog.component.html'
})
export class CompanyDepartmentDialogComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  departmentId?: number;
  employees$;

  constructor(private dialogRef: MatDialogRef<CompanyDepartmentDialogComponent>,
              private companyDepartmentService: CompanyDepartmentService,
              private employeeService: EmployeeService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.departmentId = dialogData.departmentId;
    }
  }

  ngOnInit() {
    this.employees$ = this.employeeService.getAllGeneralized();
  }

  onClear() {
    this.companyDepartmentService.companyDepartmentForm.reset();
  }
  onClose() {
    this.onClear();
    this.companyDepartmentService.initializeFormGroup();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.companyDepartmentService.companyDepartmentForm.valid) {
      if (!this.companyDepartmentService.companyDepartmentForm.get('$key').value) {
        this.subscriptions.add(this.companyDepartmentService.createDepartment(this.companyDepartmentService.companyDepartmentForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      } else {
        this.subscriptions.add(this.companyDepartmentService.updateDepartment(this.departmentId, this.companyDepartmentService.companyDepartmentForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
