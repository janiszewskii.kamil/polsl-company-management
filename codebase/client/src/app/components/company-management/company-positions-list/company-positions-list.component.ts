import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {CompanyPositionDialogComponent} from '../dialogs/company-position-dialog/company-position-dialog.component';
import {CompanyPositionService} from '../../../shared/services/company-position.service';
import {Subscription} from 'rxjs';
import {Position} from '../../../shared/models/position.model';
import {DialogService} from '../../../shared/services/dialog.service';
import {UserService} from '../../../shared/services/user.service';
import {User} from '../../../shared/models/user.model';

@Component({
  selector: 'app-company-positions-list',
  templateUrl: './company-positions-list.component.html'
})
export class CompanyPositionsListComponent implements OnInit, OnDestroy {
  positions$;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(private dialog: MatDialog,
              private positionService: CompanyPositionService,
              private userService: UserService,
              private dialogService: DialogService) {
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.positions$ = this.positionService.getAll();
  }

  onCreatePositionDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '20%';
    this.dialog.open(CompanyPositionDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdatePositionDialog(id: number): void {
    this.subscriptions.add(this.positionService.getById(id).subscribe((position: Position) => {
      this.positionService.populateForm(position);
    }));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '20%';
    dialogConfig.data = {
      positionId: id
    };
    this.dialog.open(CompanyPositionDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemovePosition(id: number, positionName: string): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć stanowisko ' + positionName + '? Proces jest nieodwracalny.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.positionService.removePosition(id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
