import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {CompanyService} from '../../shared/services/company.service';
import {Company} from '../../shared/models/company.model';
import {User} from '../../shared/models/user.model';
import {UserService} from '../../shared/services/user.service';

@Component({
  selector: 'app-company-management',
  templateUrl: './company-management.component.html'
})
export class CompanyManagementComponent implements OnInit, OnDestroy {
  company: Company;
  companySettingsForm: FormGroup;
  responseReceivedFlag = false;
  currentUser: User;
  subscriptions = new Subscription();
  updatedAt: string;
  updatedBy = null;

  constructor(private companyService: CompanyService,
              private formBuilder: FormBuilder,
              private userService: UserService) {
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.subscriptions.add(this.companyService.getData().subscribe((company: Company) => {
      this.company = company;
      this.populateForm();
      this.updatedAt = company.updatedAt;
      this.updatedBy = this.company.updatedBy;
    }));
  }

  populateForm(): void {
    this.companySettingsForm = this.formBuilder.group({
      name: [this.company.name, Validators.required],
      typeOfBusiness: [this.company.typeOfBusiness],
      nip: [this.company.nip, [Validators.minLength(10), Validators.maxLength(10)]],
      regon: [this.company.regon, [Validators.minLength(9), Validators.maxLength(14)]],
      address1: [this.company.address1],
      address2: [this.company.address2],
    });
  }

  onClear() {
    this.companySettingsForm.reset();
  }

  onSubmit() {
    if (this.companySettingsForm.valid) {
      this.subscriptions.add(this.companyService.updateData(this.companySettingsForm.value).subscribe(
        (company) => {
          this.responseReceivedFlag = true;
          this.company = company;
          this.populateForm();
        }
      ));
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
