import { Component, OnInit } from '@angular/core';
import {MessageService} from '../../../shared/services/message.service';

@Component({
  selector: 'app-messages-sent-list',
  templateUrl: './messages-sent-list.component.html'
})
export class MessagesSentListComponent implements OnInit {
  messagesSent$;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messagesSent$ = this.messageService.getAllSent();
  }

}
