import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../../shared/services/message.service';

@Component({
  selector: 'app-messages-received-list',
  templateUrl: './messages-received-list.component.html'
})
export class MessagesReceivedListComponent implements OnInit {
  messagesReceived$;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.messagesReceived$ = this.messageService.getAllReceived();
  }

}
