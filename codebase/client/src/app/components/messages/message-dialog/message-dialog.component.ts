import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {User} from '../../../shared/models/user.model';
import {Subscription} from 'rxjs';
import {MessageService} from '../../../shared/services/message.service';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html'
})
export class MessageDialogComponent implements OnInit, OnDestroy {
  messageForm: FormGroup;
  submitted = false;
  employee?: User;
  subscriptions = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private dialogRef: MatDialogRef<MessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) dialogData
  ) {
    if (dialogData) {
      this.employee = dialogData.employeeData;
    }
  }

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      recipient: [this.employee.id],
      topic: ['', Validators.required],
      text: ['', Validators.required],
    });
  }

  onClear() {
    this.messageForm.reset();
  }

  onSend() {
    if (this.messageForm.valid) {
      this.subscriptions.add(this.messageService.sendMessage(this.messageForm.value, this.employee).subscribe(
        (res) => {
          this.submitted = true;
        }
      ));
    }
    this.onClose();
  }

  onClose() {
    this.onClear();
    this.dialogRef.close();
    this.submitted = false;
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
