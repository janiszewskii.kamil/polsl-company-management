import {Component, OnInit} from '@angular/core';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {EmployeeService} from '../../../shared/services/employee.service';

@Component({
  selector: 'app-messages-main',
  templateUrl: './messages-main.component.html'
})
export class MessagesMainComponent implements OnInit {
  contacts$;

  constructor(private dialog: MatDialog, private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.contacts$ = this.employeeService.getAllGeneralized();
  }

  onMessageEmployee(employee): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '500px';
    dialogConfig.minWidth = '20%';
    dialogConfig.data = {
      employeeData: employee,
    };
    this.dialog.open(MessageDialogComponent, dialogConfig);
  }

}
