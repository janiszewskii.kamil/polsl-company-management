import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Application} from '../../../shared/models/application.model';
import {Subscription} from 'rxjs';
import {ApplicationService} from '../../../shared/services/application.service';

@Component({
  selector: 'app-application-reject-dialog',
  templateUrl: './application-reject-dialog.component.html'
})
export class ApplicationRejectDialogComponent implements OnInit, OnDestroy {
  application: Application;
  applicationRejectForm: FormGroup;
  subscriptions = new Subscription();
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private applicationService: ApplicationService,
    private dialogRef: MatDialogRef<ApplicationRejectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) dialogData
  ) {
    if (dialogData) {
      this.application = dialogData.application;
    }
  }

  ngOnInit() {
    this.applicationRejectForm = this.formBuilder.group({
      rejectionReason: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.applicationRejectForm.valid) {
      this.subscriptions.add(this.applicationService.rejectById(this.application.id, this.applicationRejectForm.value).subscribe(
        (res) => {
          this.submitted = true;
        }
      ));
    }
    this.onClose();
  }

  onClear(): void {
    this.applicationRejectForm.reset();
  }

  onClose() {
    this.submitted = false;
    this.applicationRejectForm.reset();
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
