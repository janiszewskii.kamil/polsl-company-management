import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ApplicationDialogComponent} from '../application-dialog/application-dialog.component';
import {ApplicationService} from '../../../shared/services/application.service';

@Component({
  selector: 'app-user-applications',
  templateUrl: './user-applications.component.html'
})
export class UserApplicationsComponent implements OnInit {
  openedApplications$;
  passedApplications$;
  rejectedApplications$;

  constructor(private dialog: MatDialog, private applicationService: ApplicationService) {
  }

  ngOnInit() {
    this.initData();
  }

  initData(): void {
    this.openedApplications$ = this.applicationService.getAllByCurrentUser('opened');
    this.passedApplications$ = this.applicationService.getAllByCurrentUser('passed');
    this.rejectedApplications$ = this.applicationService.getAllByCurrentUser('rejected');
  }

  onCreateApplicationDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40%';
    this.dialog.open(ApplicationDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }
}
