import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApplicationService} from '../../../shared/services/application.service';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ApplicationRejectDialogComponent} from '../application-reject-dialog/application-reject-dialog.component';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-applications-list',
  templateUrl: './applications-list.component.html'
})
export class ApplicationsListComponent implements OnInit, OnDestroy {
  openedApplications$;
  passedApplications$;
  rejectedApplications$;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(private applicationService: ApplicationService,
              private dialogService: DialogService,
              private userService: UserService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.openedApplications$ = this.applicationService.getAll('opened');
    this.passedApplications$ = this.applicationService.getAll('passed');
    this.rejectedApplications$ = this.applicationService.getAll('rejected');
  }

  onAcceptApplication(id: number) {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz zaakceptować ten wniosek?'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.applicationService.acceptById(id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  onRejectApplication(applicationData) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30%';
    dialogConfig.height = '400px';
    dialogConfig.data = {
      application: applicationData
    };
    this.dialog.open(ApplicationRejectDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
