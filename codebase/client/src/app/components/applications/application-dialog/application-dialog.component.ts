import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {CompanyDepartmentService} from '../../../shared/services/company-department.service';
import {Subscription} from 'rxjs';
import {ApplicationService} from '../../../shared/services/application.service';

@Component({
  selector: 'app-application-dialog',
  templateUrl: './application-dialog.component.html'
})
export class ApplicationDialogComponent implements OnInit, OnDestroy {
  applicationForm: FormGroup;
  departments$;
  submitted = false;
  subscriptions = new Subscription();

  constructor(private dialogRef: MatDialogRef<ApplicationDialogComponent>,
              private formBuilder: FormBuilder,
              private departmentService: CompanyDepartmentService,
              private applicationService: ApplicationService) {
  }

  ngOnInit() {
    this.departments$ = this.departmentService.getAll();
    this.applicationForm = this.formBuilder.group({
      title: ['', Validators.required],
      text: ['', Validators.required],
      targetDepartment: ['', Validators.required],
      amount: [''],
    });
  }

  onClear() {
    this.applicationForm.reset();
  }

  onClose() {
    this.applicationForm.reset();
    this.dialogRef.close();
  }

  onSend() {
    if (this.applicationForm.valid) {
      this.subscriptions.add(this.applicationService.sendApplication(this.applicationForm.value).subscribe(
        (res) => this.submitted = true
      ));
    }
    this.onClose();
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
