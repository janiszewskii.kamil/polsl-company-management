import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA} from '@angular/material';
import {CompanyClientService} from '../../../shared/services/company-client.service';

@Component({
  selector: 'app-company-client-dialog',
  templateUrl: './client-dialog.component.html'
})
export class ClientDialogComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  responseReceivedFlag = false;
  clientId?: number;

  constructor(private dialogRef: MatDialogRef<ClientDialogComponent>,
              private companyClientService: CompanyClientService,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.clientId = dialogData.clientId;
    }
  }

  ngOnInit() {
  }

  onClear() {
    this.companyClientService.companyClientForm.reset();
    this.companyClientService.initializeFormGroup();
  }

  onClose() {
    this.companyClientService.companyClientForm.reset();
    this.companyClientService.initializeFormGroup();
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.companyClientService.companyClientForm.valid) {
      if (!this.companyClientService.companyClientForm.get('$key').value) {
        this.subscriptions.add(this.companyClientService.createClient(this.companyClientService.companyClientForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      } else {
        this.subscriptions.add(this.companyClientService.updateClient(this.clientId, this.companyClientService.companyClientForm.value).subscribe(
          (res) => {
            this.responseReceivedFlag = true;
          }
        ));
      }
      this.onClose();
    }
  }

  ngOnDestroy(): void {
    if (this.responseReceivedFlag) {
      this.subscriptions.unsubscribe();
    }
  }
}
