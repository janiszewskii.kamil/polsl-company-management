import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {DialogService} from '../../../shared/services/dialog.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {CompanyClientService} from '../../../shared/services/company-client.service';
import {ClientDialogComponent} from '../client-dialog/client-dialog.component';
import {UserService} from '../../../shared/services/user.service';
import {User} from '../../../shared/models/user.model';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html'
})
export class ClientsListComponent implements OnInit, OnDestroy {
  clientsDataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'nip', 'regon', 'address1', 'address2', 'contactPhoneNumber', 'contactEmail', 'numberOfProjects', 'numberOfClosedProjects', 'actions'];
  searchKey: string;
  subscriptions = new Subscription();
  currentUser: User;

  constructor(
    private clientService: CompanyClientService,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private userService: UserService
  ) {
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.initData();
  }

  initData(): void {
    this.subscriptions.add(this.clientService.getAll().subscribe(
      (response) => {
        this.clientsDataSource = new MatTableDataSource(response);
        this.clientsDataSource.sort = this.sort;
        this.clientsDataSource.paginator = this.paginator;
      }));
  }

  onSearchClear(): void {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter(): void {
    this.clientsDataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onCreateClientDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '80%';
    this.dialog.open(ClientDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateClientDialog(row): void {
    this.clientService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.maxWidth = '80%';
    dialogConfig.data = {
      clientId: row.id
    };
    this.dialog.open(ClientDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemoveClient(row): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć klienta ' + row.name + '? Proces jest nieodwracalny.'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.clientService.removeClient(row.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
