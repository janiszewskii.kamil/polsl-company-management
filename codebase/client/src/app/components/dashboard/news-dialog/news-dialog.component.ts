import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NewsService} from '../../../shared/services/news.service';

@Component({
  selector: 'app-news-dialog',
  templateUrl: './news-dialog.component.html'
})
export class NewsDialogComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  submitted = false;
  newsId?: number;

  constructor(private formBuilder: FormBuilder,
              private newsService: NewsService,
              private dialogRef: MatDialogRef<NewsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) dialogData) {
    if (dialogData) {
      this.newsId = dialogData.newsId;
    }
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.newsService.newsForm.valid) {
      if (!this.newsService.newsForm.get('$key').value) {
        this.subscriptions.add(this.newsService.createNews(this.newsService.newsForm.value).subscribe(
          (res) => {
            this.submitted = true;
          }
        ));
      } else {
        this.subscriptions.add(this.newsService.updateNews(this.newsId, this.newsService.newsForm.value).subscribe(
          (res) => {
            this.submitted = true;
          }
        ));
      }
      this.onClose();
    }
  }

  onClear(): void {
    this.newsService.newsForm.reset();
  }

  onClose(): void {
    this.submitted = false;
    this.onClear();
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (this.submitted) {
      this.subscriptions.unsubscribe();
    }
  }
}
