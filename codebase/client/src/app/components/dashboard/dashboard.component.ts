import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';
import {NewsService} from '../../shared/services/news.service';
import {Dashboard} from '../../shared/models/dashboard.model';
import {DashboardService} from '../../shared/services/dashboard.service';
import {Subscription} from 'rxjs';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {NewsDialogComponent} from './news-dialog/news-dialog.component';
import {DialogService} from '../../shared/services/dialog.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit, OnDestroy {
  currentUser;
  newsList$;
  dashboard: Dashboard;
  subscriptions = new Subscription();


  constructor(private dialog: MatDialog,
              private userService: UserService,
              private newsService: NewsService,
              private dialogService: DialogService,
              private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.subscriptions.add(this.dashboardService.getData().subscribe(
      (dashboard: Dashboard) => {
        this.dashboard = dashboard;
      }
    ));
    this.initData();
  }

  initData(): void {
    this.newsList$ = this.newsService.getAll();
  }

  onCreateNewsDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    this.dialog.open(NewsDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onUpdateNewsDialog(news): void {
    this.newsService.populateForm(news);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    dialogConfig.data = {
      newsId: news.id
    };
    this.dialog.open(NewsDialogComponent, dialogConfig).afterClosed().subscribe(
      () => this.initData()
    );
  }

  onRemoveNews(news): void {
    this.dialogService.openConfirmDialog(
      'Jesteś pewien, że chcesz usunąć tę wiadomość? Proces jest nieodwracalny/'
    ).afterClosed().subscribe(response => {
      if (response) {
        this.subscriptions.add(this.newsService.removeNews(news.id).subscribe(
          () => this.initData()
        ));
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
