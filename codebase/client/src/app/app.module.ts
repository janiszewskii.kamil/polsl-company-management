import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MainComponent} from './components/main/main.component';
import {AngularMaterialModule} from './angular-material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {JwtInterceptor} from './shared/helpers/interceptors/jwt.interceptor';
import {ErrorInterceptor} from './shared/helpers/interceptors/error.interceptor';
import {AccountSettingsComponent} from './components/account-settings/account-settings.component';
import {EmployeesListComponent} from './components/employees/employees-list/employees-list.component';
import {EmployeeDialogComponent} from './components/employees/employee-dialog/employee-dialog.component';
import {EmployeeService} from './shared/services/employee.service';
import {HeaderComponent} from './shared/components/header/header.component';
import {SidebarComponent} from './shared/components/sidebar/sidebar.component';
import {ConfirmDialogComponent} from './shared/components/confirm-dialog/confirm-dialog.component';
import {FooterComponent} from './shared/components/footer/footer.component';
import {MatMenuModule} from '@angular/material/menu';
import {WorkTimeReportDialogComponent} from './components/work-time-reports/work-time-report-dialog/work-time-report-dialog.component';
import {WorkTimeReportsListComponent} from './components/work-time-reports/work-time-reports-list/work-time-reports-list.component';
import { CompanyManagementComponent } from './components/company-management/company-management.component';
import { ProjectsMainComponent } from './components/projects/projects-main/projects-main.component';
import { MessagesMainComponent } from './components/messages/messages-main/messages-main.component';
import {MessageDialogComponent} from './components/messages/message-dialog/message-dialog.component';
import { ProjectDialogComponent } from './components/projects/project-dialog/project-dialog.component';
import { OpenedProjectsListComponent } from './components/projects/opened-projects-list/opened-projects-list.component';
import { ClosedProjectsListComponent } from './components/projects/closed-projects-list/closed-projects-list.component';
import {MessagesSentListComponent} from './components/messages/messages-sent-list/messages-sent-list.component';
import { UserApplicationsComponent } from './components/applications/user-applications/user-applications.component';
import { CompanyDepartmentsListComponent } from './components/company-management/company-departments-list/company-departments-list.component';
import { CompanyPositionsListComponent } from './components/company-management/company-positions-list/company-positions-list.component';
import { CompanyDepartmentDialogComponent } from './components/company-management/dialogs/company-department-dialog/company-department-dialog.component';
import { ClientDialogComponent } from './components/clients/client-dialog/client-dialog.component';
import { CompanyPositionDialogComponent } from './components/company-management/dialogs/company-position-dialog/company-position-dialog.component';
import { ApplicationsListComponent } from './components/applications/applications-list/applications-list.component';
import { UserWorkTimeReportsListComponent } from './components/work-time-reports/user-work-time-reports-list/user-work-time-reports-list.component';
import { ProjectComponent } from './components/projects/project/project.component';
import { ApplicationDialogComponent } from './components/applications/application-dialog/application-dialog.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/pl';
import {MessagesReceivedListComponent} from './components/messages/messages-received-list/messages-received-list.component';
import { EmployeeComponent } from './components/employees/employee/employee.component';
import { EventsListComponent } from './components/events/events-list/events-list.component';
import { NewsDialogComponent } from './components/dashboard/news-dialog/news-dialog.component';
import {PageNotFoundComponent} from './error-pages/404/page-not-found.component';
import {PageForbiddenComponent} from './error-pages/403/page-forbidden/page-forbidden.component';
import {EventDialogComponent} from './components/events/event-dialog/event-dialog.component';
import {UserEventsListComponent} from './components/events/user-events-list/user-events-list.component';
import { ApplicationRejectDialogComponent } from './components/applications/application-reject-dialog/application-reject-dialog.component';
import { ChangePasswordDialogComponent } from './components/account-settings/change-password-dialog/change-password-dialog.component';
import { ProjectCloseDialogComponent } from './components/projects/project-close-dialog/project-close-dialog.component';
import { ClientsListComponent } from './components/clients/clients-list/clients-list.component';
import { ItemsListComponent } from './components/items/items-list/items-list.component';
import { ItemsDialogComponent } from './components/items/items-dialog/items-dialog.component';
import { EmployeePermissionsDialogComponent } from './components/employees/employee-permissions-dialog/employee-permissions-dialog.component';
import {LoginComponent} from './components/auth/login/login.component';
import {ResetPasswordComponent} from './components/auth/reset-password/reset-password.component';

registerLocaleData(localeFr, 'pl');

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    MainComponent,
    WorkTimeReportsListComponent,
    AccountSettingsComponent,
    EmployeesListComponent,
    EmployeeDialogComponent,
    ConfirmDialogComponent,
    MessageDialogComponent,
    HeaderComponent,
    FooterComponent,
    WorkTimeReportDialogComponent,
    CompanyManagementComponent,
    ProjectsMainComponent,
    MessagesMainComponent,
    MessagesSentListComponent,
    MessagesReceivedListComponent,
    ProjectDialogComponent,
    OpenedProjectsListComponent,
    ClosedProjectsListComponent,
    UserApplicationsComponent,
    CompanyDepartmentsListComponent,
    CompanyPositionsListComponent,
    CompanyDepartmentDialogComponent,
    ClientDialogComponent,
    CompanyPositionDialogComponent,
    ApplicationsListComponent,
    UserWorkTimeReportsListComponent,
    ProjectComponent,
    ApplicationDialogComponent,
    EmployeeComponent,
    EventsListComponent,
    NewsDialogComponent,
    PageNotFoundComponent,
    PageForbiddenComponent,
    EventDialogComponent,
    UserEventsListComponent,
    ApplicationRejectDialogComponent,
    ChangePasswordDialogComponent,
    ProjectCloseDialogComponent,
    ClientsListComponent,
    ItemsListComponent,
    ItemsDialogComponent,
    EmployeePermissionsDialogComponent,
    ResetPasswordComponent,
  ],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AngularFontAwesomeModule,
    MatMenuModule,
  ],
  providers: [
    EmployeeService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ClientDialogComponent,
    ApplicationDialogComponent,
    CompanyDepartmentDialogComponent,
    CompanyPositionDialogComponent,
    ProjectDialogComponent,
    ConfirmDialogComponent,
    WorkTimeReportDialogComponent,
    EmployeeDialogComponent,
    MessageDialogComponent,
    NewsDialogComponent,
    EventDialogComponent,
    ApplicationRejectDialogComponent,
    ChangePasswordDialogComponent,
    ProjectCloseDialogComponent,
    ItemsDialogComponent,
    EmployeePermissionsDialogComponent
  ]
})
export class AppModule {
}
