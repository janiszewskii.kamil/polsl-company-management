<?php

namespace App\Dto;

class UserPositionDto
{
    private $name;
    private $isManagerialPosition;
    private $description;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsManagerialPosition()
    {
        return $this->isManagerialPosition;
    }

    public function setIsManagerialPosition($isManagerialPosition): self
    {
        $this->isManagerialPosition = $isManagerialPosition;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }
}
