<?php

namespace App\Dto;

class UserDataDto
{
    private $firstName;
    private $lastName;
    private $gender;
    private $birthDate;
    private $phoneNumber;
    private $street;
    private $houseNo;
    private $flatNo;
    private $city;
    private $zipCode;
    private $province;

    public function getFirstName()
    {
        return $this->firstName;
    }


    public function setFirstName($firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthDate()
    {
        return $this->birthDate;
    }

    public function setBirthDate($birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouseNo()
    {
        return $this->houseNo;
    }

    public function setHouseNo($houseNo): self
    {
        $this->houseNo = $houseNo;

        return $this;
    }

    public function getFlatNo()
    {
        return $this->flatNo;
    }

    public function setFlatNo($flatNo): self
    {
        $this->flatNo = $flatNo;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function setZipCode($zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function setProvince($province): self
    {
        $this->province = $province;

        return $this;
    }
}
