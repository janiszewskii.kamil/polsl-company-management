<?php

namespace App\Dto;

class ItemDto
{
    private $name;
    private $department;
    private $employee;
    private $warrantyEndDate;
    private $description;
    private $typeOfOwnership;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function setDepartment($department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setEmployee($employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getWarrantyEndDate()
    {
        return $this->warrantyEndDate;
    }

    public function setWarrantyEndDate($warrantyEndDate): self
    {
        $this->warrantyEndDate = $warrantyEndDate;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTypeOfOwnership()
    {
        return $this->typeOfOwnership;
    }

    public function setTypeOfOwnership($typeOfOwnership): self
    {
        $this->typeOfOwnership = $typeOfOwnership;

        return $this;
    }
}
