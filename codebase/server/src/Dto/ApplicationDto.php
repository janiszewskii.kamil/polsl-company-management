<?php

namespace App\Dto;

class ApplicationDto
{
    private $title;
    private $text;
    private $amount;
    private $targetDepartment;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTargetDepartment()
    {
        return $this->targetDepartment;
    }

    public function setTargetDepartment($targetDepartment): self
    {
        $this->targetDepartment = $targetDepartment;

        return $this;
    }
}
