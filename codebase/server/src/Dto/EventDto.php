<?php

namespace App\Dto;

class EventDto
{
    private $name;
    private $startDate;
    private $endDate;
    private $participant;
    private $place;
    private $time;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getParticipant()
    {
        return $this->participant;
    }

    public function setParticipant($participant): self
    {
        $this->participant = $participant;

        return $this;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place): self
    {
        $this->place = $place;

        return $this;
    }
}
