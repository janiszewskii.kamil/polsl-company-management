<?php

namespace App\Dto;

class WorkTimeReportDto
{
    private $reportForDate;
    private $time;
    private $message;
    private $project;
    private $type;

    public function getReportForDate()
    {
        return $this->reportForDate;
    }

    public function setReportForDate($reportForDate): self
    {
        $this->reportForDate = $reportForDate;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject($project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }
}
