<?php

namespace App\Dto;

class DepartmentDto
{
    private $name;
    private $manager;
    private $description;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }
}
