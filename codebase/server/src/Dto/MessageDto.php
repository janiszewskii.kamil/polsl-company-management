<?php

namespace App\Dto;

class MessageDto
{
    private $topic;
    private $text;
    private $recipient;

    public function getTopic()
    {
        return $this->topic;
    }

    public function setTopic($topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRecipient()
    {
        return $this->recipient;
    }

    public function setRecipient($recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }
}