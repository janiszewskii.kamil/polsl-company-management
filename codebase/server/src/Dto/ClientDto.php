<?php

namespace App\Dto;

class ClientDto
{
    private $name;
    private $nip;
    private $regon;
    private $address1;
    private $address2;
    private $contactPhoneNumber;
    private $contactEmail;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNip()
    {
        return $this->nip;
    }

    public function setNip($nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getRegon()
    {
        return $this->regon;
    }

    public function setRegon($regon): self
    {
        $this->regon = $regon;

        return $this;
    }

    public function getAddress1()
    {
        return $this->address1;
    }

    public function setAddress1($address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setAddress2($address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getContactPhoneNumber()
    {
        return $this->contactPhoneNumber;
    }

    public function setContactPhoneNumber($contactPhoneNumber): self
    {
        $this->contactPhoneNumber = $contactPhoneNumber;

        return $this;
    }

    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    public function setContactEmail($contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }
}
