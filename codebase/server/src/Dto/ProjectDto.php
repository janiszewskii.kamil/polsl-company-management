<?php

namespace App\Dto;

class ProjectDto
{
    private $name;
    private $code;
    private $startDate;
    private $endDate;
    private $plannedBudget;
    private $spentBudget;
    private $plannedIncome;
    private $finalIncome;
    private $plannedEndDate;
    private $manager;
    private $client;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPlannedBudget()
    {
        return $this->plannedBudget;
    }

    public function setPlannedBudget($plannedBudget): self
    {
        $this->plannedBudget = $plannedBudget;

        return $this;
    }

    public function getSpentBudget()
    {
        return $this->spentBudget;
    }

    public function setSpentBudget($spentBudget): self
    {
        $this->spentBudget = $spentBudget;

        return $this;
    }

    public function getPlannedIncome()
    {
        return $this->plannedIncome;
    }

    public function setPlannedIncome($plannedIncome): self
    {
        $this->plannedIncome = $plannedIncome;

        return $this;
    }

    public function getFinalIncome()
    {
        return $this->finalIncome;
    }

    public function setFinalIncome($finalIncome): self
    {
        $this->finalIncome = $finalIncome;

        return $this;
    }

    public function getPlannedEndDate()
    {
        return $this->plannedEndDate;
    }

    public function setPlannedEndDate($plannedEndDate): self
    {
        $this->plannedEndDate = $plannedEndDate;

        return $this;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient($client): self
    {
        $this->client = $client;

        return $this;
    }
}
