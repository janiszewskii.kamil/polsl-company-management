<?php

namespace App\Dto;

class UserDto
{
    private $email;
    private $password;
    private $hireDate;
    private $position;
    private $department;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getHireDate()
    {
        return $this->hireDate;
    }


    public function setHireDate($hireDate): self
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function setDepartment($department): self
    {
        $this->department = $department;

        return $this;
    }
}
