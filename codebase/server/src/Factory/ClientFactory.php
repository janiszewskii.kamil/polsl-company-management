<?php

namespace App\Factory;

use App\Dto\ClientDto;
use App\Entity\Client;
use App\Helper\ContextHelper;

class ClientFactory
{
    private $contextHelper;

    public function __construct(ContextHelper $contextHelper)
    {
        $this->contextHelper = $contextHelper;
    }

    public function create(ClientDto $clientDto): Client
    {
        return $this->transform($clientDto);
    }

    public function update(ClientDto $clientDto, Client $client): Client
    {
        return $this->transform($clientDto, $client);
    }

    private function transform(ClientDto $clientDto, ?Client $client = null): Client
    {
        if (!$client) {
            $client = new Client();
            $client->setCompany($this->contextHelper->getCurrentCompanyContext())
                ->setNumberOfProjects(0)
                ->setNumberOfClosedProjects(0);
            return $this->populateObject($clientDto, $client);
        } else {
            return $this->populateObject($clientDto, $client);
        }
    }

    private function populateObject(ClientDto $clientDto, Client $client): Client
    {
        return $client->setName($clientDto->getName())
            ->setContactEmail($clientDto->getContactEmail())
            ->setContactPhoneNumber($clientDto->getContactPhoneNumber())
            ->setAddress1($clientDto->getAddress1())
            ->setAddress2($clientDto->getAddress2())
            ->setRegon($clientDto->getRegon())
            ->setNip($clientDto->getNip());
    }
}
