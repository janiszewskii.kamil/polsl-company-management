<?php

namespace App\Factory;

use App\Dto\UserDataDto;
use App\Entity\UserData;

class UserDataFactory
{
    public function __construct()
    {
    }

    public function create(UserDataDto $userDataDto): UserData
    {
        return $this->transform($userDataDto);
    }

    public function update(UserDataDto $userDataDto, UserData $userData): UserData
    {
        return $this->transform($userDataDto, $userData);
    }

    private function transform(UserDataDto $userDataDto, ?UserData $userData = null): UserData
    {
        if (!$userData) {
            $userData = $this->createInstance($userDataDto);
        } else {
            $userData->setFirstName($userDataDto->getLastName())
                ->setLastName($userDataDto->getLastName())
                ->setBirthDate(new \DateTime($userDataDto->getBirthDate()))
                ->setCity($userDataDto->getCity())
                ->setHouseNo($userDataDto->getHouseNo())
                ->setFlatNo($userDataDto->getFlatNo())
                ->setGender($userDataDto->getGender())
                ->setProvince($userDataDto->getProvince())
                ->setZipCode($userDataDto->getZipCode())
                ->setPhoneNumber($userDataDto->getPhoneNumber());
        }

        return $userData;
    }

    private function createInstance(UserDataDto $userDataDto): UserData
    {
        $userData = new UserData();

        return $userData->setFirstName($userDataDto->getLastName())
            ->setLastName($userDataDto->getLastName())
            ->setBirthDate(new \DateTime($userDataDto->getBirthDate()))
            ->setCity($userDataDto->getCity())
            ->setHouseNo($userDataDto->getHouseNo())
            ->setFlatNo($userDataDto->getFlatNo())
            ->setGender($userDataDto->getGender())
            ->setProvince($userDataDto->getProvince())
            ->setZipCode($userDataDto->getZipCode())
            ->setPhoneNumber($userDataDto->getPhoneNumber());
    }
}
