<?php

namespace App\Factory;

use App\Dto\CompanyDto;
use App\Entity\Company;
use App\Helper\ContextHelper;

class CompanyFactory
{
    private $contextHelper;

    public function __construct(ContextHelper $contextHelper)
    {
        $this->contextHelper = $contextHelper;
    }

    public function create(CompanyDto $companyDto): Company
    {
        return $this->transform($companyDto);
    }

    public function update(CompanyDto $companyDto, Company $company): Company
    {
        return $this->transform($companyDto, $company);
    }

    private function transform(CompanyDto $companyDto, ?Company $company = null): Company
    {
        if (!$company) {
            $company = new Company();
            $company->setUpdatedBy($this->contextHelper->getCurrentUser());
            return $this->populateObject($companyDto, $company);
        } else {
            $company->setUpdatedAt(new \DateTime());
            return $this->populateObject($companyDto, $company);
        }
    }

    private function populateObject(CompanyDto $companyDto, Company $company): Company
    {
        return $company->setName($companyDto->getName())
            ->setTypeOfBusiness($companyDto->getTypeOfBusiness())
            ->setRegon($companyDto->getRegon())
            ->setNip($companyDto->getNip())
            ->setAddress1($companyDto->getAddress1())
            ->setAddress2($companyDto->getAddress2());
    }
}
