<?php

namespace App\Factory;

use App\Dto\UserDto;
use App\Entity\User;
use App\Helper\ContextHelper;
use App\Service\DepartmentService;
use App\Service\UserPositionService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    private const STANDARD_PERMISSIONS = [
        'canViewClients' => true,
        'canEditClients' => false,
        'canViewProjects' => true,
        'canEditProjects' => false,
        'canViewItems' => true,
        'canEditItems' => false,
        'canViewWorkTimeReports' => false,
        'canViewCompany' => true,
        'canEditCompany' => false,
        'canEditEmployees' => false,
        'canViewEmployees' => true,
        'canEditEvents' => false,
        'canViewEvents' => true,
        'canViewApplications' => false,
        'canServeApplications' => false,
        'canPublishNews' => false,
    ];

    private $passwordEncoder;
    private $positionService;
    private $contextHelper;
    private $departmentService;

    public function __construct(
        DepartmentService $departmentService,
        UserPositionService $positionService,
        ContextHelper $contextHelper,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->departmentService = $departmentService;
        $this->positionService = $positionService;
        $this->contextHelper = $contextHelper;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function create(UserDto $userDto): User
    {
        return $this->transform($userDto);
    }

    public function update(UserDto $userDto, User $user): User
    {
        return $this->transform($userDto, $user);
    }

    private function transform(UserDto $userDto, ?User $user = null): User
    {
        if (!$user) {
            $user = $this->createInstance($userDto);
        } else {
            $user->setHireDate(new \DateTime($userDto->getHireDate()));

            if ($userDto->getDepartment()) {
                $user->setDepartment($this->departmentService->getById($userDto->getDepartment()));
            }

            if ($userDto->getPosition()) {
                $user->setPosition($this->positionService->getById($userDto->getPosition()));
            }
        }

        return $user;
    }

    private function createInstance(UserDto $userDto): User
    {
        $user = new User();
        $temporaryPassword = sha1(random_bytes(10));

        if ($userDto->getDepartment()) {
            $user->setDepartment($this->departmentService->getById($userDto->getDepartment()));
        }

        if ($userDto->getPosition()) {
            $user->setPosition($this->positionService->getById($userDto->getPosition()));
        }

        return $user->setEmail($userDto->getEmail())
            ->setHireDate(new \DateTime($userDto->getHireDate()))
            ->setCompany($this->contextHelper->getCurrentCompanyContext())
            ->setPermissions(self::STANDARD_PERMISSIONS)
            ->setTemporaryPlainPassword($temporaryPassword)
            ->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $temporaryPassword
                )
            );
    }
}
