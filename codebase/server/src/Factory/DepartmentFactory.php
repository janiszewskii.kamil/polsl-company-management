<?php

namespace App\Factory;

use App\Dto\DepartmentDto;
use App\Entity\Department;
use App\Helper\ContextHelper;
use App\Repository\UserRepository;

class DepartmentFactory
{
    private $userRepository;
    private $contextHelper;

    public function __construct(UserRepository $userRepository, ContextHelper $contextHelper)
    {
        $this->userRepository = $userRepository;
        $this->contextHelper = $contextHelper;
    }

    public function create(DepartmentDto $departmentDto): Department
    {
        return $this->transform($departmentDto);
    }

    public function update(DepartmentDto $departmentDto, Department $department): Department
    {
        return $this->transform($departmentDto, $department);
    }

    private function transform(DepartmentDto $departmentDto, ?Department $department = null): Department
    {
        if (!$department) {
            $department = new Department();
            $department->setCompany($this->contextHelper->getCurrentCompanyContext());
            return $this->populateObject($departmentDto, $department);
        } else {
            return $this->populateObject($departmentDto, $department);
        }
    }

    private function populateObject(DepartmentDto $departmentDto, Department $department): Department
    {
        return $department->setName($departmentDto->getName())
            ->setManager($this->userRepository->find($departmentDto->getManager()))
            ->setDescription($departmentDto->getDescription());
    }
}
