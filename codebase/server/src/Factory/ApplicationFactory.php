<?php

namespace App\Factory;

use App\Dto\ApplicationDto;
use App\Entity\Application;
use App\Helper\ContextHelper;
use App\Repository\DepartmentRepository;

class ApplicationFactory
{
    private $contextHelper;
    private $departmentRepository;

    public function __construct(ContextHelper $contextHelper, DepartmentRepository $departmentRepository)
    {
        $this->contextHelper = $contextHelper;
        $this->departmentRepository = $departmentRepository;
    }

    public function create(ApplicationDto $applicationDto): Application
    {
        return $this->transform($applicationDto);
    }

    private function transform(ApplicationDto $applicationDto): Application
    {
        $application = new Application();
        return $this->populateObject($applicationDto, $application);
    }

    private function populateObject(ApplicationDto $applicationDto, Application $application): Application
    {
        return $application->setTitle($applicationDto->getTitle())
            ->setText($applicationDto->getText())
            ->setAmount($applicationDto->getAmount())
            ->setTargetDepartment($this->departmentRepository->find($applicationDto->getTargetDepartment()))
            ->setCreatedBy($this->contextHelper->getCurrentUser());
    }
}
