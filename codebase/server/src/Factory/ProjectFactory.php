<?php

namespace App\Factory;

use App\Dto\ProjectDto;
use App\Entity\Project;
use App\Helper\ContextHelper;
use App\Service\ClientService;
use App\Service\UserService;

class ProjectFactory
{
    private $userService;
    private $clientService;
    private $contextHelper;

    public function __construct(UserService $userService, ClientService $clientService, ContextHelper $contextHelper)
    {
        $this->userService = $userService;
        $this->clientService = $clientService;
        $this->contextHelper = $contextHelper;
    }

    public function create(ProjectDto $projectDto): Project
    {
        return $this->transform($projectDto);
    }

    public function update(ProjectDto $projectDto, Project $project): Project
    {
        return $this->transform($projectDto, $project);
    }

    private function transform(ProjectDto $projectDto, ?Project $project = null): Project
    {
        if (!$project) {
            $project = new Project();
            $project->setCompany($this->contextHelper->getCurrentCompanyContext());
            return $this->populateObject($projectDto, $project);
        } else {
            return $this->populateObject($projectDto, $project);
        }
    }

    private function populateObject(ProjectDto $projectDto, Project $project): Project
    {
       return $project->setName($projectDto->getName())
            ->setPlannedIncome($projectDto->getPlannedIncome())
            ->setPlannedBudget($projectDto->getPlannedBudget())
            ->setPlannedEndDate(new \DateTime($projectDto->getPlannedEndDate()))
            ->setStartDate(new \DateTime($projectDto->getStartDate()))
            ->setCode($projectDto->getCode())
            ->setManager($this->userService->getById($projectDto->getManager()))
            ->setClient($this->clientService->getById($projectDto->getClient()));
    }
}
