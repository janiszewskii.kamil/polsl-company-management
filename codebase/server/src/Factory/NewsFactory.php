<?php

namespace App\Factory;

use App\Dto\NewsDto;
use App\Entity\News;
use App\Helper\ContextHelper;

class NewsFactory
{
    private $contextHelper;

    public function __construct(ContextHelper $contextHelper)
    {
        $this->contextHelper = $contextHelper;
    }

    public function create(NewsDto $newsDto): News
    {
        return $this->transform($newsDto);
    }

    public function update(NewsDto $clientDto, News $news): News
    {
        return $this->transform($clientDto, $news);
    }

    private function transform(NewsDto $newsDto, News $news = null): News
    {
        if (!$news) {
            $news = new News();
            $news->setUpdatedBy($this->contextHelper->getCurrentUser())
                ->setUpdatedAt(new \DateTime());
            return $this->populateObject($newsDto, $news);
        } else {
            return $this->populateObject($newsDto, $news);
        }
    }

    private function populateObject(NewsDto $newsDto, News $news): News
    {
        return $news->setText($newsDto->getText())
            ->setTitle($newsDto->getTitle())
            ->setType($newsDto->getType())
            ->setCreatedAt(new \DateTime())
            ->setCreatedBy($this->contextHelper->getCurrentUser())
            ->setCompany($this->contextHelper->getCurrentCompanyContext());
    }
}
