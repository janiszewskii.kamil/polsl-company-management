<?php

namespace App\Factory;

use App\Dto\UserPositionDto;
use App\Entity\UserPosition;
use App\Helper\ContextHelper;

class UserPositionFactory
{
    private $contextHelper;

    public function __construct(ContextHelper $contextHelper)
    {
        $this->contextHelper = $contextHelper;
    }

    public function create(UserPositionDto $userPositionDto): UserPosition
    {
        return $this->transform($userPositionDto);
    }

    public function update(UserPositionDto $userPositionDto, UserPosition $userPosition): UserPosition
    {
        return $this->transform($userPositionDto, $userPosition);
    }

    private function transform(UserPositionDto $userPositionDto, ?UserPosition $userPosition = null): UserPosition
    {
        if (!$userPosition) {
            $userPosition = new UserPosition();
            $userPosition->setCompany($this->contextHelper->getCurrentCompanyContext());
            return $this->populateObject($userPositionDto, $userPosition);
        } else {
            return $this->populateObject($userPositionDto, $userPosition);
        }
    }

    private function populateObject(UserPositionDto $userPositionDto, UserPosition $userPosition): UserPosition
    {
        return $userPosition->setName($userPositionDto->getName())
            ->setIsManagerialPosition($userPositionDto->getIsManagerialposition());
    }
}
