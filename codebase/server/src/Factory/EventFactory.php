<?php

namespace App\Factory;

use App\Dto\EventDto;
use App\Entity\Event;
use App\Helper\ContextHelper;
use App\Repository\UserRepository;

class EventFactory
{
    private $contextHelper;
    private $userRepository;

    public function __construct(ContextHelper $contextHelper, UserRepository $userRepository)
    {
        $this->contextHelper = $contextHelper;
        $this->userRepository = $userRepository;
    }

    public function create(EventDto $eventDto): Event
    {
        return $this->transform($eventDto);
    }

    public function update(EventDto $eventDto, Event $event): Event
    {
        return $this->transform($eventDto, $event);
    }

    private function transform(EventDto $eventDto, ?Event $event = null): Event
    {
        if (!$event) {
            $event = new Event();
            $event->setCompany($this->contextHelper->getCurrentCompanyContext())
                ->setCreatedBy($this->contextHelper->getCurrentUser());
            return $this->populateObject($eventDto, $event);
        } else {
            $event->setUpdatedAt(new \DateTime())
                ->setUpdatedBy($this->contextHelper->getCurrentUser());
            return $this->populateObject($eventDto, $event);
        }
    }

    private function populateObject(EventDto $eventDto, Event $event): Event
    {
        if ($eventDto->getParticipant()) {
            $event->setParticipant($this->userRepository->find($eventDto->getParticipant()));
        } else {
            $event->setParticipant($this->contextHelper->getCurrentUser());
        }

        return $event->setName($eventDto->getName())
            ->setPlace($eventDto->getPlace())
            ->setStartDate(new \DateTime($eventDto->getStartDate()))
            ->setEndDate(new \DateTime($eventDto->getEndDate()))
            ->setTime($eventDto->getTime());
    }
}
