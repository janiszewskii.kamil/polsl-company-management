<?php

namespace App\Factory;

use App\Dto\WorkTimeReportDto;
use App\Entity\WorkTimeReport;
use App\Helper\ContextHelper;
use App\Repository\ProjectRepository;

class WorkTimeReportFactory
{
    private $contextHelper;
    private $projectRepository;

    public function __construct(ContextHelper $contextHelper, ProjectRepository $projectRepository)
    {
        $this->contextHelper = $contextHelper;
        $this->projectRepository = $projectRepository;
    }

    public function create(WorkTimeReportDto $workTimeReportDto): WorkTimeReport
    {
        return $this->transform($workTimeReportDto);
    }

    public function update(WorkTimeReportDto $workTimeReportDto, WorkTimeReport $workTimeReport): WorkTimeReport
    {
        return $this->transform($workTimeReportDto, $workTimeReport);
    }

    private function transform(
        WorkTimeReportDto $workTimeReportDto,
        ?WorkTimeReport $workTimeReport = null
    ): WorkTimeReport {
        if (!$workTimeReport) {
            $workTimeReport = new WorkTimeReport();
            $workTimeReport->setCreatedBy($this->contextHelper->getCurrentUser());
            return $this->populateObject($workTimeReportDto, $workTimeReport);
        } else {
            $workTimeReport->setUpdateAt(new \DateTime());
            return $this->populateObject($workTimeReportDto, $workTimeReport);
        }
    }

    private function populateObject(
        WorkTimeReportDto $workTimeReportDto,
        WorkTimeReport $workTimeReport
    ): WorkTimeReport {
        return $workTimeReport->setCreatedAt(new \DateTime())
            ->setReportForDate(new \DateTime($workTimeReportDto->getReportForDate()))
            ->setTime($workTimeReportDto->getTime())
            ->setMessage($workTimeReportDto->getMessage())
            ->setType($workTimeReportDto->getType())
            ->setProject($this->projectRepository->find($workTimeReportDto->getProject()));
    }
}
