<?php

namespace App\Factory;

use App\Dto\MessageDto;
use App\Entity\Message;
use App\Helper\ContextHelper;
use App\Repository\UserRepository;

class MessageFactory
{
    private $userRepository;
    private $contextHelper;

    public function __construct(UserRepository $userRepository, ContextHelper $contextHelper)
    {
        $this->userRepository = $userRepository;
        $this->contextHelper = $contextHelper;
    }

    public function create(MessageDto $messageDto): Message
    {
        return $this->transform($messageDto);
    }

    private function transform(MessageDto $messageDto): Message
    {
        $message = new Message();
        return $this->populateObject($messageDto, $message);
    }

    private function populateObject(MessageDto $messageDto, Message $message): Message
    {
        return $message->setText($messageDto->getText())
            ->setTopic($messageDto->getTopic())
            ->setRecipient($this->userRepository->find($messageDto->getRecipient()))
            ->setCreatedAt(new \DateTime())
            ->setCreatedBy($this->contextHelper->getCurrentUser());
    }
}
