<?php

namespace App\Factory;

use App\Dto\ItemDto;
use App\Entity\Item;
use App\Helper\ContextHelper;
use App\Repository\DepartmentRepository;
use App\Repository\UserRepository;

class ItemFactory
{
    private $userRepository;
    private $departmentRepository;
    private $contextHelper;

    public function __construct(
        UserRepository $userRepository,
        DepartmentRepository $departmentRepository,
        ContextHelper $contextHelper
    ) {
        $this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
        $this->contextHelper = $contextHelper;
    }

    public function create(ItemDto $itemDto): Item
    {
        return $this->transform($itemDto);
    }

    public function update(ItemDto $itemDto, Item $item): Item
    {
        return $this->transform($itemDto, $item);
    }

    private function transform(ItemDto $itemDto, ?Item $item = null): Item
    {
        if (!$item) {
            $item = new Item();
            $item->setCompany($this->contextHelper->getCurrentCompanyContext());
            return $this->populateObject($itemDto, $item);
        } else {
            return $this->populateObject($itemDto, $item);
        }
    }

    private function populateObject(ItemDto $itemDto, Item $item): Item
    {
        if ($itemDto->getDepartment()) {
            $item->setDepartment($this->departmentRepository->find($itemDto->getDepartment()));
        }

        if ($itemDto->getEmployee()) {
            $item->setUser($this->userRepository->find($itemDto->getEmployee()));
        }

        return $item->setName($itemDto->getName())
            ->setWarrantyEndDate(new \DateTime($itemDto->getWarrantyEndDate()))
            ->setTypeOfOwnership($itemDto->getTypeOfOwnership())
            ->setDescription($itemDto->getDescription());
    }
}
