<?php

namespace App\MessageHandler;

use App\Mailer\SenderInterface;
use App\Message\NewUserMail;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NewUserMailHandler implements MessageHandlerInterface
{
    const MAIL_FROM_ADDRESS = 'company.management@dev.dev';

    private $userRepository;
    private $sender;
    private $entityManager;

    public function __construct(
        UserRepository $userRepository,
        SenderInterface $sender,
        EntityManagerInterface $entityManager
    ) {
        $this->userRepository = $userRepository;
        $this->sender = $sender;
        $this->entityManager = $entityManager;
    }

    public function __invoke(NewUserMail $newUserMail)
    {
        $this->send($newUserMail);
    }

    private function send(NewUserMail $newEmployeeMail): void
    {
        $user = $this->userRepository->find($newEmployeeMail->getUserId());
        $userData = $user->getData();
        $this->sender->send(
            self::MAIL_FROM_ADDRESS,
            [$user->getEmail()],
            'Witam w aplikacji do zarządzania firmą!',
            'emails/welcome-user.html.twig',
            [
                'firstName' => $userData->getFirstName(),
                'lastName' => $userData->getLastName(),
                'temporaryPlainPassword' => $newEmployeeMail->getTemporaryPassword()
            ]
        );
        $user->setTemporaryPlainPassword(null);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
