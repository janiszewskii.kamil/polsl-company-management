<?php

namespace App\MessageHandler;

use App\Mailer\SenderInterface;
use App\Message\ResetPasswordMail;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ResetPasswordMailHandler implements MessageHandlerInterface
{
    const MAIL_FROM_ADDRESS = 'company.management@dev.dev';

    private $userRepository;
    private $sender;
    private $entityManager;

    public function __construct(
        UserRepository $userRepository,
        SenderInterface $sender,
        EntityManagerInterface $entityManager
    ) {
        $this->userRepository = $userRepository;
        $this->sender = $sender;
        $this->entityManager = $entityManager;
    }

    public function __invoke(ResetPasswordMail $resetPasswordMail)
    {
        $this->send($resetPasswordMail);
    }

    private function send(ResetPasswordMail $resetPasswordMail): void
    {
        $user = $this->userRepository->find($resetPasswordMail->getUserId());
        $userData = $user->getData();
        $this->sender->send(
            self::MAIL_FROM_ADDRESS,
            [$user->getEmail()],
            'Przypomnienie hasła do aplikacji',
            'emails/reset-password.html.twig',
            [
                'firstName' => $userData->getFirstName(),
                'lastName' => $userData->getLastName(),
                'newPassword' => $user->getTemporaryPlainPassword()
            ]
        );
        $user->setTemporaryPlainPassword(null);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
