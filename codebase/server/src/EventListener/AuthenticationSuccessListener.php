<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $userData = $user->getData();

        if (!in_array('ROLE_ADMIN', $user->getRoles())) {
            $company = $user->getCompany();
            $companyData = [
                'id' => $company->getId(),
                'name' => $company->getName()
            ];
        } else {
            $companyData = null;
        }


        $data['user'] = [
            'id' => $user->getId(),
            'permissions' => $user->getPermissions(),
            'data' => [
                'firstName' => $userData->getFirstName(),
                'lastName' => $userData->getLastName()
            ],
            'company' => $companyData
        ];

        $event->setData($data);
    }
}
