<?php

namespace App\Message;

class NewUserMail extends AsyncMessage
{
    private $userId;
    private $temporaryPassword;

    public function __construct(int $userId, string $temporaryPassword)
    {
        $this->userId = $userId;
        $this->temporaryPassword = $temporaryPassword;
    }

    public function getTemporaryPassword(): string
    {
        return $this->temporaryPassword;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
