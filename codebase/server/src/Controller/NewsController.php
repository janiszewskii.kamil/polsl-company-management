<?php

namespace App\Controller;

use App\Entity\News;
use App\Helper\ContextHelper;
use App\Service\NewsService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/news")
 */
class NewsController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $newsService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        NewsService $newsService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->newsService = $newsService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_all_news", methods={"GET"})
     */
    public function showAll()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->newsService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => ['newsDetails']]
            )
        );
    }

    /**
     * @Route(name="create_news", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canPublishNews'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->newsService->create($request)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="show_news", methods={"GET"})
     */
    public function show(News $news)
    {
        try {
            return $this->json($this->serializer->normalize($news, 'json', ['groups' => 'newsDetails']));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_news", methods={"PUT"})
     */
    public function update(Request $request, News $news)
    {
        try {
            if ($this->getUser()->getPermissions()['canPublishNews'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->newsService->update($request, $news)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="remove_news", methods={"DELETE"})
     */
    public function remove(News $news)
    {
        try {
            if ($this->getUser()->getPermissions()['canPublishNews'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            $this->newsService->remove($news);
            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
