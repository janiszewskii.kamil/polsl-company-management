<?php

namespace App\Controller;

use App\Entity\Department;
use App\Helper\ContextHelper;
use App\Service\DepartmentService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/departments")
 */
class DepartmentController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $departmentService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        DepartmentService $departmentService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->departmentService = $departmentService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_departments", methods={"GET"})
     */
    public function showAll()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->departmentService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => ['departmentDetails']]
            )
        );
    }

    /**
     * @Route("/{id}", name="show_department_by_id", methods={"GET"})
     */
    public function show(Department $department)
    {
        return $this->json($this->serializer->normalize($department, 'json', ['groups' => 'departmentDetails']));
    }

    /**
     * @Route(name="create_department", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->departmentService->create($request)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_department", methods={"PUT"})
     */
    public function update(Request $request, Department $department)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json(
                $this->handleSuccessResponse($this->departmentService->update($request, $department)->getId())
            );
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="remove_department", methods={"DELETE"})
     */
    public function remove(Department $department)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            $this->departmentService->remove($department);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
