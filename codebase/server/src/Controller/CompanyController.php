<?php

namespace App\Controller;

use App\Helper\ContextHelper;
use App\Service\CompanyService;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CompanyController extends AbstractController
{
    use ResponseTrait;

    private $companyService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        CompanyService $companyService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->companyService = $companyService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/company", name="show_company", methods={"GET"})
     */
    public function show()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->contextHelper->getCurrentCompanyContext(),
                'json',
                ['groups' => 'companyDetails']
            )
        );
    }

    /**
     * @Route("/company", name="update_company", methods={"PUT"})
     */
    public function update(Request $request)
    {
        try {
            return $this->json(
                $this->serializer->normalize(
                    $this->companyService->update($request, $this->contextHelper->getCurrentCompanyContext()),
                    'json',
                    ['groups' => 'companyDetails']
                )
            );
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
