<?php

namespace App\Controller;

use App\Helper\ContextHelper;
use App\Service\DashboardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/company/dashboard")
 */
class DashboardController extends AbstractController
{
    private $contextHelper;
    private $dashboardService;

    public function __construct(ContextHelper $contextHelper, DashboardService $dashboardService)
    {
        $this->contextHelper = $contextHelper;
        $this->dashboardService = $dashboardService;
    }

    /**
     * @Route(name="show_dashboard", methods={"GET"})
     */
    public function showByCompany()
    {
        return $this->json(
            $this->dashboardService->getDashboardByCompany($this->contextHelper->getCurrentCompanyContext())
        );
    }
}
