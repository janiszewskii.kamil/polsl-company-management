<?php

namespace App\Controller;

use App\Helper\ContextHelper;
use App\Service\AccountService;
use App\Service\UserService;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/user/account")
 */
class AccountController extends AbstractController
{
    use ResponseTrait;

    private $userService;
    private $accountService;
    private $contextHelper;
    private $serializer;

    public function __construct(UserService $userService,AccountService $accountService, ContextHelper $contextHelper, SerializerInterface $serializer)
    {
        $this->userService = $userService;
        $this->accountService = $accountService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_account_data", methods={"GET"})
     */
    public function showData()
    {
        return $this->json($this->serializer->normalize($this->getUser(), 'json', ['groups' => 'userDetails']));
    }


    /**
     * @Route(name="update_account_data", methods={"PUT"})
     */
    public function update(Request $request)
    {
        try {
            return $this->json(
                $this->serializer->normalize(
                    $this->userService->update($request, $this->contextHelper->getCurrentUser()),
                    'json',
                    ['groups' => 'userDetails']
                )
            );
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/change-password", name="change_password", methods={"POST"})
     */
    public function changePassword(Request $request)
    {
        try {
            $this->accountService->changePassword($request);
            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
