<?php

namespace App\Controller;

use App\Service\MessageService;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/user/messages")
 */
class MessageController extends AbstractController
{
    use ResponseTrait;

    private $messageService;
    private $serializer;

    public function __construct(MessageService $messageService, SerializerInterface $serializer)
    {
        $this->messageService = $messageService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/sent", name="show_messages_sent", methods={"GET"})
     */
    public function showSent()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->messageService->getAllSentByUser($this->getUser()),
                'json',
                ['groups' => 'messageSent']
            )
        );
    }

    /**
     * @Route("/received", name="show_messages_received", methods={"GET"})
     */
    public function showReceived()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->messageService->getAllReceivedByUser($this->getUser()),
                'json',
                ['groups' => 'messageReceived']
            )
        );
    }

    /**
     * @Route("/send", name="send_message", methods={"POST"})
     */
    public function send(Request $request)
    {
        try {
            $this->messageService->send($request);
            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
