<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\WorkTimeReport;
use App\Helper\ContextHelper;
use App\Service\WorkTimeReportService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class WorkTimeReportController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $contextHelper;
    private $workTimeRecordService;
    private $serializer;

    public function __construct(
        ContextHelper $contextHelper,
        WorkTimeReportService $workTimeRecordService,
        SerializerInterface $serializer
    ) {
        $this->contextHelper = $contextHelper;
        $this->workTimeRecordService = $workTimeRecordService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/company/work-time-reports", name="show_work_time_reports", methods={"GET"})
     */
    public function showAll()
    {
        if ($this->getUser()->getPermissions()['canViewWorkTimeReports'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->workTimeRecordService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'workTimeReportDetails']
            )
        );
    }

    /**
     * @Route("/user/work-time-reports", name="show_logged_user_work_time_reports", methods={"GET"})
     */
    public function showAllByLoggedUser()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->workTimeRecordService->getAllByUser($this->getUser()),
                'json',
                ['groups' => 'workTimeReportDetails']
            )
        );
    }

    /**
     * @Route("/user/{id}/work-time-reports", name="show_work_time_reports_by_user", methods={"GET"})
     */
    public function showAllByUser(User $user)
    {
        if ($this->getUser()->getPermissions()['canViewWorkTimeReports'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->workTimeRecordService->getAllByUser($user),
                'json',
                ['groups' => 'workTimeReportDetails']
            )
        );
    }

    /**
     * @Route("/project/{id}/work-time-reports", name="show_work_time_reports_by_project", methods={"GET"})
     */
    public function showAllByProject(Project $project)
    {
        if ($this->getUser()->getPermissions()['canViewWorkTimeReports'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->workTimeRecordService->getAllByProject($project),
                'json',
                ['groups' => 'workTimeReportDetails']
            )
        );
    }

    /**
     * @Route("/user/work-time-reports", name="create_work_time_report", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->workTimeRecordService->create($request)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/user/work-time-reports/{id}", name="update_work_time_report", methods={"PUT"})
     */
    public function update(Request $request, WorkTimeReport $workTimeReport)
    {
        try {
            return $this->json(
                $this->handleSuccessResponse($this->workTimeRecordService->update($request, $workTimeReport)->getId())
            );
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/user/work-time-reports/{id}", name="remove_work_time_report", methods={"DELETE"})
     */
    public function remove(WorkTimeReport $workTimeReport)
    {
        try {
            $this->workTimeRecordService->remove($workTimeReport);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/work-time-reports/{id}", name="show_work_time_report_by_id", methods={"GET"})
     */
    public function show(WorkTimeReport $workTimeReport)
    {
        return $this->json(
            $this->serializer->normalize($workTimeReport, 'json', ['groups' => 'workTimeReportDetails'])
        );
    }
}
