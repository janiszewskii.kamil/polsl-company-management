<?php

namespace App\Controller;

use App\Entity\Item;
use App\Helper\ContextHelper;
use App\Service\ItemService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/items")
 */
class ItemController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $itemService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        ItemService $itemService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->itemService = $itemService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_items", methods={"GET"})
     */
    public function showAll()
    {
        if ($this->getUser()->getPermissions()['canViewItems'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->itemService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => ['itemDetails']]
            )
        );
    }

    /**
     * @Route("/{id}", name="show_item_by_id", methods={"GET"})
     */
    public function show(Item $item)
    {
        if ($this->getUser()->getPermissions()['canViewItems'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json($this->serializer->normalize($item, 'json', ['groups' => 'itemDetails']));
    }

    /**
     * @Route(name="create_item", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditItems'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->itemService->create($request)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_item", methods={"PUT"})
     */
    public function update(Request $request, Item $item)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditItems'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json(
                $this->handleSuccessResponse($this->itemService->update($request, $item)->getId())
            );
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="remove_item", methods={"DELETE"})
     */
    public function remove(Item $item)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditItems'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            $this->itemService->remove($item);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
