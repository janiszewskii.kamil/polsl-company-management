<?php

namespace App\Controller;

use App\Entity\Application;
use App\Entity\User;
use App\Helper\ContextHelper;
use App\Service\ApplicationService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApplicationController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $contextHelper;
    private $applicationService;
    private $serializer;

    public function __construct(
        ContextHelper $contextHelper,
        ApplicationService $applicationService,
        SerializerInterface $serializer
    ) {
        $this->contextHelper = $contextHelper;
        $this->applicationService = $applicationService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/company/applications/{status}", name="show_applications", methods={"GET"})
     */
    public function showAll(string $status)
    {
        if ($this->getUser()->getPermissions()['canViewApplications'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->applicationService->getAllByCompany($this->contextHelper->getCurrentCompanyContext(), $status),
                'json',
                ['groups' => 'applicationDetails']
            )
        );
    }

    /**
     * @Route("/user/applications/{status}", name="show_applications_by_logged_user", methods={"GET"})
     */
    public function showAllByLoggedUser(string $status)
    {
        return $this->json(
            $this->serializer->normalize(
                $this->applicationService->getAllByUser($this->getUser(), $status),
                'json',
                ['groups' => 'applicationDetails']
            )
        );
    }

    /**
     * @Route("/user/{id}/applications/{status}", name="show_applications_by_user", defaults={"status" : ""}, methods={"GET"})
     */
    public function showAllByUser(User $user, ?string $status)
    {
        if ($this->getUser()->getPermissions()['canViewApplications'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->applicationService->getAllByUser($user, $status),
                'json',
                ['groups' => 'applicationDetails']
            )
        );
    }

    /**
     * @Route("/user/applications/send", name="create_application", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->applicationService->create($request)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/applications/{id}/accept", name="accept_application", methods={"POST"})
     */
    public function accept(Application $application)
    {
        try {
            if ($this->getUser()->getPermissions()['canServeApplications'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->applicationService->accept($application);
            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/applications/{id}/reject", name="reject_application", methods={"POST"})
     */
    public function reject(Request $request, Application $application)
    {
        try {
            if ($this->getUser()->getPermissions()['canServeApplications'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->applicationService->reject($application, $request);
            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
