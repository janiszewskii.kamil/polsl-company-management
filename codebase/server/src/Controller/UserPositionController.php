<?php

namespace App\Controller;

use App\Entity\UserPosition;
use App\Helper\ContextHelper;
use App\Service\UserPositionService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/positions")
 */
class UserPositionController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $userPositionService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        UserPositionService $userPositionService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->userPositionService = $userPositionService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_positions", methods={"GET"})
     */
    public function showAll()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->userPositionService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => ['userPositionDetails']]
            )
        );
    }

    /**
     * @Route("/{id}", name="show_user_position_by_id", methods={"GET"})
     */
    public function show(UserPosition $userPosition)
    {
        return $this->json($this->serializer->normalize($userPosition, 'json', ['groups' => 'userPositionDetails']));
    }

    /**
     * @Route(name="create_user_position", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            return $this->json($this->handleSuccessResponse($this->userPositionService->create($request)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_user_position", methods={"PUT"})
     */
    public function update(Request $request, UserPosition $userPosition)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            return $this->json(
                $this->handleSuccessResponse($this->userPositionService->update($request, $userPosition)->getId())
            );
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="remove_user_position", methods={"DELETE"})
     */
    public function remove(UserPosition $userPosition)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditCompany'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->userPositionService->remove($userPosition);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
