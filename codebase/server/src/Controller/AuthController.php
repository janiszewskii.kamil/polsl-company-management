<?php

namespace App\Controller;

use App\Service\AuthService;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/auth")
 */
class AuthController extends AbstractController
{
    use ResponseTrait;

    /**
     * @Route("/login", name="api_login", methods={"POST"})
     */
    public function login()
    {
        return $this->json($this->handleSuccessResponse());
    }

    /**
     * @Route("/logout", name="api_logout")
     */
    public function logout(AuthService $authService)
    {
        $authService->logout();
        return $this->json($this->handleSuccessResponse());
    }

    /**
     * @Route("/reset-password", name="api_reset_password", methods={"POST"})
     */
    public function resetPassword(AuthService $authService, Request $request)
    {
        try {
            $authService->resetPassword($request);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
