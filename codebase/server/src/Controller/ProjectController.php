<?php

namespace App\Controller;

use App\Entity\Project;
use App\Helper\ContextHelper;
use App\Service\ProjectService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/projects")
 */
class ProjectController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $projectService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        ProjectService $projectService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->projectService = $projectService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/opened", name="show_opened_projects", methods={"GET"})
     */
    public function showAllOpened()
    {
        if ($this->getUser()->getPermissions()['canViewProjects'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->projectService->getAllOpenedByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'projectDetails']
            )
        );
    }

    /**
     * @Route("/closed", name="show_closed_projects", methods={"GET"})
     */
    public function showAllClosed()
    {
        if ($this->getUser()->getPermissions()['canViewProjects'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->projectService->getAllClosedByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'projectDetails']
            )
        );
    }

    /**
     * @Route(name="show_all_projects", methods={"GET"})
     */
    public function showAll()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->projectService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'projectGeneral']
            )
        );
    }

    /**
     * @Route("/{id}", name="show_project", methods={"GET"})
     */
    public function show(Project $project)
    {
        if ($this->getUser()->getPermissions()['canViewProjects'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json($this->serializer->normalize($project, 'json', ['groups' => 'projectDetails']));
    }

    /**
     * @Route("/{id}/close", name="close_project", methods={"POST"})
     */
    public function close(Request $request, Project $project)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditProjects'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->projectService->close($request, $project);

            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}/statistics", name="show_project_statistics", methods={"GET"})
     */
    public function showStatistics(Project $project)
    {
        if ($this->getUser()->getPermissions()['canEditProjects'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json($this->projectService->getStatisticsByProject($project));
    }

    /**
     * @Route(name="create_project", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditProjects'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->projectService->create($request)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_project", methods={"PUT"})
     */
    public function update(Request $request, Project $project)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditProjects'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json(
                $this->handleSuccessResponse($this->projectService->update($request, $project)->getId())
            );
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
