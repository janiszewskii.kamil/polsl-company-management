<?php

namespace App\Controller;

use App\Entity\Client;
use App\Helper\ContextHelper;
use App\Service\ClientService;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/clients")
 */
class ClientController extends AbstractController
{
    use ResponseTrait;

    private $contextHelper;
    private $clientService;
    private $serializer;

    public function __construct(
        ContextHelper $contextHelper,
        ClientService $clientService,
        SerializerInterface $serializer
    ) {
        $this->contextHelper = $contextHelper;
        $this->clientService = $clientService;
        $this->serializer = $serializer;
    }

    /**
     * @Route(name="show_clients", methods={"GET"})
     */
    public function showAll()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->clientService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'clientDetails']
            )
        );
    }

    /**
     * @Route("/{id}", name="show_client_by_id", methods={"GET"})
     */
    public function show(Client $client)
    {
        return $this->json($this->serializer->normalize($client, 'json', ['groups' => 'clientDetails']));
    }

    /**
     * @Route(name="create_client", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->clientService->create($request)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_client", methods={"PUT"})
     */
    public function update(Request $request, Client $client)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->clientService->update($request, $client)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="remove_client", methods={"DELETE"})
     */
    public function remove(Client $client)
    {
        try {
            $this->clientService->remove($client);
            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
