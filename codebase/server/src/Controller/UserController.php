<?php

namespace App\Controller;

use App\Entity\User;
use App\Helper\ContextHelper;
use App\Service\UserService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/company/users")
 */
class UserController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $userService;
    private $contextHelper;
    private $serializer;

    public function __construct(
        UserService $userService,
        ContextHelper $contextHelper,
        SerializerInterface $serializer
    ) {
        $this->userService = $userService;
        $this->contextHelper = $contextHelper;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/{id<\d+>}", name="show_user_by_id", methods={"GET"})
     */
    public function showById(User $user)
    {
        if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json($this->serializer->normalize($user, 'json', ['groups' => 'userDetails']));
    }

    /**
     * @Route("/{id<\d+>}/permissions", name="update_user_permission", methods={"PUT"})
     */
    public function updatePermissions(Request $request, User $user)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->userService->updatePermissions($request, $user);

            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getMessage(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id<\d+>}/permissions", name="show_user_permissions", methods={"GET"})
     */
    public function showPermissions(User $user)
    {
        if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json($this->userService->getPermissionsByUser($user));
    }

    /**
     * @Route("/{type}", name="show_users", methods={"GET"})
     */
    public function showAll(string $type)
    {
        return $this->json(
            $this->serializer->normalize(
                $this->userService->getAllByCompany(
                    $this->contextHelper->getCurrentCompanyContext()
                ),
                'json',
                ['groups' => $this->getGroupByType($type)]
            )
        );
    }

    /**
     * @Route(name="create_user", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->userService->create($request)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getMessage(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}", name="update_user", methods={"PUT"})
     */
    public function update(Request $request, User $user)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
                $this->throwHttpAccessDeniedException();
            }

            return $this->json($this->handleSuccessResponse($this->userService->update($request, $user)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getMessage(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/{id}/anonymize", name="anonymize_user", methods={"POST"})
     */
    public function anonymize(User $user)
    {
        try {
            if ($this->getUser()->getPermissions()['canEditEmployees'] !== true) {
                $this->throwHttpAccessDeniedException();
            }
            $this->userService->anonymize($user);

            return $this->json($this->handleSuccessResponse());
        } catch (Exception $e) {
            return $this->json(['status' => $e->getMessage(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    private function getGroupByType(string $type): string
    {
        $group = '';

        if ($type === 'generalized') {
            $group = 'userGeneral';
        } else {
            if ($type === 'detailed') {
                $group = 'userDetails';
            }
        }

        return $group;
    }
}
