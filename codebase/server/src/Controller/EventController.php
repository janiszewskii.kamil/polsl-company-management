<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Helper\ContextHelper;
use App\Service\EventService;
use App\Traits\ExceptionTrait;
use App\Traits\ResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class EventController extends AbstractController
{
    use ResponseTrait;
    use ExceptionTrait;

    private $contextHelper;
    private $eventService;
    private $serializer;

    public function __construct(
        ContextHelper $contextHelper,
        EventService $eventService,
        SerializerInterface $serializer
    ) {
        $this->contextHelper = $contextHelper;
        $this->eventService = $eventService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/company/events", name="show_all_events", methods={"GET"})
     */
    public function showAll()
    {
        if ($this->getUser()->getPermissions()['canViewEvents'] !== true) {
            $this->throwHttpAccessDeniedException();
        }

        return $this->json(
            $this->serializer->normalize(
                $this->eventService->getAllByCompany($this->contextHelper->getCurrentCompanyContext()),
                'json',
                ['groups' => 'eventDetails']
            )
        );
    }

    /**
     * @Route("/user/{id}/events", name="show_events_by_user", methods={"GET"})
     */
    public function showAllByUser(User $user)
    {
        return $this->json(
            $this->serializer->normalize(
                $this->eventService->getAllByUser($user),
                'json',
                ['groups' => 'eventDetails']
            )
        );
    }

    /**
     * @Route("/user/events", name="show_events_by_logged_user", methods={"GET"})
     */
    public function showAllByLoggedUser()
    {
        return $this->json(
            $this->serializer->normalize(
                $this->eventService->getAllByUser($this->contextHelper->getCurrentUser()),
                'json',
                ['groups' => 'eventDetails']
            )
        );
    }

    /**
     * @Route("/company/events/{id}", name="show_event_by_id", methods={"GET"})
     */
    public function show(Event $event)
    {
        return $this->json($this->serializer->normalize($event, 'json', ['groups' => 'eventDetails']));
    }

    /**
     * @Route("/company/events", name="create_event", methods={"POST"})
     */
    public function create(Request $request)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->eventService->create($request)->getId()));
        } catch (Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/company/events/{id}", name="update_event", methods={"PUT"})
     */
    public function update(Request $request, Event $event)
    {
        try {
            return $this->json($this->handleSuccessResponse($this->eventService->update($request, $event)->getId()));
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/events/{id}/cancel", name="cancel_event", methods={"POST"})
     */
    public function cancel(Event $event)
    {
        try {
            $this->eventService->cancel($event);

            return $this->json($this->handleSuccessResponse());
        } catch (\Exception $e) {
            return $this->json(['status' => $e->getCode(), 'message' => $e->getMessage()], $e->getCode());
        }
    }

}
