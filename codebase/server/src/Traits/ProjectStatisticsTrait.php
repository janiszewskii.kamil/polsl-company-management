<?php

namespace App\Traits;

use App\Entity\Project;
use Doctrine\Common\Collections\ArrayCollection;

trait ProjectStatisticsTrait
{
    protected function countTotalFullDays(Project $project): int
    {
        return date_diff($project->getEndDate(), $project->getStartDate())->format('%a');
    }

    protected function countTotalTime(Project $project): float
    {
        $workTimeReports = $project->getTimeRecords();
        $time = 0;

        foreach ($workTimeReports as $workTimeRecord) {
            $time += $workTimeRecord->getTime();
        }

        return $time;
    }

    protected function countTotalDelayedDays(Project $project): int
    {
        return date_diff($project->getEndDate(), $project->getPlannedEndDate())->format('%a');
    }

    protected function countTotalProfit(Project $project): int
    {
        return $project->getFinalIncome() - $project->getSpentBudget();
    }

    protected function countTotalSubmittedReports(Project $project): int
    {
        return count($project->getTimeRecords());
    }

    protected function countTotalEmployees(Project $project): int
    {
        $workTimeReports = $project->getTimeRecords();
        $employees = new ArrayCollection();

        foreach ($workTimeReports as $workTimeRecord) {
            $employee = $workTimeRecord->getCreatedBy();
            if(!$employees->contains($employee)) {
                $employees[] = $employee;
            }
        }

        return count($employees);
    }
}
