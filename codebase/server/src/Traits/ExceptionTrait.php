<?php

namespace App\Traits;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    public function throwHttpNotFoundException(string $message): void
    {
        throw new NotFoundHttpException(
            $message,
            null,
            Response::HTTP_NOT_FOUND
        );
    }

    public function throwHttpBadRequestException(string $message): void
    {
        throw new BadRequestHttpException(
            $message,
            null,
            Response::HTTP_BAD_REQUEST
        );
    }

    public function throwHttpAccessDeniedException(string $message = 'accessDenied'): void
    {
        throw new AccessDeniedException(
            $message,
            Response::HTTP_FORBIDDEN
        );
    }
}
