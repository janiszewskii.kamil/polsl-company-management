<?php

namespace App\Traits;

use Exception;
use Symfony\Component\HttpFoundation\Response;

trait ResponseTrait
{
    public function handleSuccessResponse(int $id = null): array
    {
        if ($id !== null) {
            return [
                'status' => RESPONSE::HTTP_OK,
                'id' => $id
            ];
        }

        return [
            'status' => RESPONSE::HTTP_OK,
            'message' => 'success'
        ];
    }

    public function handleException(Exception $e): array
    {
        $this->logger->error($e->getMessage());

        return [
            'status' => $e->getCode(),
            'message' => $e->getMessage()
        ];
    }
}
