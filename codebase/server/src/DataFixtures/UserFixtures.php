<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use App\Entity\Department;
use App\Entity\UserData;
use App\Entity\UserPosition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private const CAN_ALL_PERMISSIONS = [
        'canViewClients' => true,
        'canEditClients' => true,
        'canViewProjects' => true,
        'canEditProjects' => true,
        'canViewItems' => true,
        'canEditItems' => true,
        'canViewWorkTimeReports' => true,
        'canViewCompany' => true,
        'canEditCompany' => true,
        'canViewEmployees' => true,
        'canEditEmployees' => true,
        'canViewEvents' => true,
        'canEditEvents' => true,
        'canViewApplications' => true,
        'canServeApplications' => true,
        'canPublishNews' => true,
    ];

    private const CAN_VIEW_EDIT_EMPLOYEES_REPORTS_EVENTS = [
        'canViewClients' => false,
        'canEditClients' => false,
        'canViewProjects' => false,
        'canEditProjects' => false,
        'canViewItems' => false,
        'canEditItems' => false,
        'canViewWorkTimeReports' => true,
        'canViewCompany' => true,
        'canEditCompany' => false,
        'canEditEmployees' => true,
        'canViewEmployees' => true,
        'canViewEvents' => true,
        'canEditEvents' => false,
        'canViewApplications' => false,
        'canServeApplications' => false,
        'canPublishNews' => false,
    ];

    private const CAN_VIEW_EDIT_PROJECTS_CLIENTS_ITEMS_COMPANY_APPLICATIONS_EVENTS = [
        'canViewClients' => true,
        'canEditClients' => true,
        'canViewProjects' => true,
        'canEditProjects' => true,
        'canViewItems' => true,
        'canEditItems' => true,
        'canViewWorkTimeReports' => true,
        'canViewCompany' => true,
        'canEditCompany' => true,
        'canEditEmployees' => false,
        'canViewEmployees' => true,
        'canViewEvents' => true,
        'canEditEvents' => true,
        'canViewApplications' => true,
        'canServeApplications' => true,
        'canPublishNews' => true,
    ];

    private const CAN_STANDARD = [
        'canViewClients' => true,
        'canEditClients' => false,
        'canViewProjects' => true,
        'canEditProjects' => false,
        'canViewItems' => true,
        'canEditItems' => false,
        'canViewWorkTimeReports' => false,
        'canViewCompany' => true,
        'canEditCompany' => false,
        'canEditEmployees' => false,
        'canViewEmployees' => true,
        'canViewEvents' => true,
        'canEditEvents' => false,
        'canViewApplications' => false,
        'canServeApplications' => false,
        'canPublishNews' => false,
    ];

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    private function getUsers(): array
    {
        return [
            ['admin@dev.dev', 'test', 1, null, null, null, null, null, null],
            [
                'szef@dev.dev',
                'test',
                2,
                1,
                1,
                1,
                '2015-02-10 00:00:00',
                self::CAN_ALL_PERMISSIONS,
                true
            ],
            [
                'kadrowy1@dev.dev',
                'test',
                3,
                2,
                1,
                1,
                '2015-02-10 00:00:00',
                self::CAN_VIEW_EDIT_EMPLOYEES_REPORTS_EVENTS,
                true
            ],
            [
                'kadrowy2@dev.dev',
                'test',
                4,
                2,
                1,
                1,
                '2015-02-10 00:00:00',
                self::CAN_VIEW_EDIT_EMPLOYEES_REPORTS_EVENTS,
                false
            ],
            [
                'ksiegowy1@dev.dev',
                'test',
                5,
                3,
                2,
                1,
                '2015-02-10 00:00:00',
                self::CAN_VIEW_EDIT_PROJECTS_CLIENTS_ITEMS_COMPANY_APPLICATIONS_EVENTS,
                true
            ],
            [
                'ksiegowy2@dev.dev',
                'test',
                6,
                3,
                2,
                1,
                '2015-02-10 00:00:00',
                self::CAN_VIEW_EDIT_PROJECTS_CLIENTS_ITEMS_COMPANY_APPLICATIONS_EVENTS,
                false
            ],
            ['programista1@dev.dev', 'test', 7, 4, 3, 1, '2015-02-10 00:00:00', self::CAN_STANDARD, false],
            ['programista2@dev.dev', 'test', 8, 4, 3, 1, '2015-02-10 00:00:00', self::CAN_STANDARD, false],
            ['programista3@dev.dev', 'test', 9, 5, 3, 1, '2015-02-10 00:00:00', self::CAN_STANDARD, true],
            ['programista4@dev.dev', 'test', 10, 5, 3, 1, '2015-02-10 00:00:00', self::CAN_STANDARD, false],
            ['stazysta0@dev.dev', 'test', 11, 6, 3, 1, '2015-02-10 00:00:00', self::CAN_STANDARD, false]
        ];
    }

    private function getUsersData(): array
    {
        return [
            ['SuperAdmin', 'SuperAdmin', '000000000', 'male', 'Wąska', '53', null, '40-749', 'Katowice'],
            ['Adam', 'Nowakowski', '999000222', 'male', 'Panewnicka', '10', '12', '40-749', 'Katowice'],
            ['Michał', 'Olszewski', '999000222', 'male', 'Bocheńskiego Jana', '3', null, '40-749', 'Katowice'],
            ['Andrzej ', 'Lewandowski', '888000222', 'male', 'Wyszyńskiego', '44', null, '40-749', 'Katowice'],
            ['Aleks', 'Mikułka', '456000222', 'male', 'Konarskiego', '13', null, '40-749', 'Katowice'],
            ['Wiktor', 'Kozieradzki', '123000222', 'male', 'Pogodna', '53', null, '40-749', 'Katowice'],
            ['Janina', 'Nowak', '999000222', 'female', 'Barwinków', '10A', '20', '40-749', 'Katowice'],
            ['Martyna', 'Zagajewska', '211002233', 'female', 'Skalna', '5B', '12', '40-749', 'Katowice'],
            ['Oliwia', 'Wierzbicka', '777888232', 'female', 'Wierzbowa', '46', '15', '40-749', 'Katowice'],
            ['Jacek', 'Nowogórski', '232234756', 'male', 'Jana Pawła', '9', null, '40-749', 'Katowice'],
            ['Igor', 'Siwy', '232234756', 'male', 'Kurpiowska', '12', null, '40-749', 'Katowice'],
        ];
    }

    private function loadUsersData(ObjectManager $manager): void
    {
        foreach (
            $this->getUsersData(
            ) as [$firstName, $lastName, $phoneNumber, $gender, $street, $houseNo, $flatNo, $zipCode, $city]
        ) {
            $userData = new UserData();
            $userData->setFirstName($firstName)
                ->setLastName($lastName)
                ->setPhoneNumber($phoneNumber)
                ->setGender($gender)
                ->setStreet($street)
                ->setHouseNo($houseNo)
                ->setFlatNo($flatNo)
                ->setZipCode($zipCode)
                ->setCity($city);

            $manager->persist($userData);
        }
    }

    private function loadUsers(ObjectManager $manager): void
    {
        foreach (
            $this->getUsers(
            ) as [$email, $password, $userDataId, $positionId, $departmentId, $companyId, $hireData, $permissions, $isDepartmentManager]
        ) {
            $user = new User();
            $department = null;
            $passwordEncoded = $this->passwordEncoder->encodePassword($user, $password);
            $user->setEmail($email)
                ->setPassword($passwordEncoded)
                ->setIsActive(true)
                ->setHireDate(new \DateTime($hireData))
                ->setPermissions($permissions)
                ->setData($manager->getRepository(UserData::class)->find($userDataId));

            if ($email === 'admin@dev.dev') {
                $user->setRoles(['ROLE_ADMIN']);
            } else {
                $user->setRoles(['ROLE_USER']);
            }

            if ($departmentId !== null) {
                $department = $manager->getRepository(Department::class)->find($departmentId);
            }

            if ($isDepartmentManager !== null && $isDepartmentManager === true) {
                $department->setManager($user);
                $manager->persist($department);
            }

            if ($companyId === null) {
                $user->setCompany(null);
            } else {
                $user->setCompany($manager->getRepository(Company::class)->find($companyId));
            }

            if ($departmentId === null) {
                $user->setDepartment(null);
            } else {
                $user->setDepartment($department);
            }

            if ($positionId === null) {
                $user->setPosition(null);
            } else {
                $user->setPosition($manager->getRepository(UserPosition::class)->find($positionId));
            }

            $manager->persist($user);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsersData($manager);
        $this->loadUsers($manager);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
            PositionFixutres::class,
            DepartmentFixtures::class,
        ];
    }
}
