<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\WorkTimeReport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class WorkTimeReportFixtures extends Fixture implements DependentFixtureInterface
{
    private function getWorkTimeRecords(): array
    {
        return [
            [1, 'Testowy raport do projektu', 8, '10-01-2020', 3, 'nominal'],
            [2, 'Analiza wymagań technicznych', 8, '10-01-2020', 2, 'nominal'],
            [3, 'Testowy raport do projektu', 8, '16-01-2020', 2, 'nominal'],
            [1, 'Testowanie starej funkcjonalności', 8, '16-01-2020', 4, 'overtime100'],
            [1, 'Praca nad mock-upami', 8, '10-01-2020', 7, 'overtime50'],
            [1, 'Przygotowywanie endpointów', 8, '20-10-2019', 5, 'nominalCopyright'],
            [1, 'Przygotowywanie endpointów', 8, '01-01-2020', 6, 'nominalCopyright'],
            [null, 'Rozliczanie faktur', 8, '10-12-2019', 4, 'nominalCopyright']
        ];
    }

    private function loadWorkTimeRecords($manager): void
    {
        foreach ($this->getWorkTimeRecords() as [$projectId, $message, $time, $reportForDate, $authorId, $type]) {
            $workTimeRecord = new WorkTimeReport();
            $workTimeRecord->setCreatedBy($manager->getRepository(User::class)->find($authorId))
                ->setMessage($message)
                ->setTime($time)
                ->setType($type)
                ->setReportForDate(new \DateTime($reportForDate));

            if ($projectId !== null) {
                $workTimeRecord->setProject($manager->getRepository(Project::class)->find($projectId));
            } else {
                $workTimeRecord->setProject(null);
            }

            $manager->persist($workTimeRecord);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadWorkTimeRecords($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProjectFixtures::class,
            UserFixtures::class
        ];
    }
}
