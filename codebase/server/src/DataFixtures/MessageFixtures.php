<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    private function getMessages(): array
    {
        return [
            ['Temat pierwszy', 'Hej to jest testowa wiadomość!', 2, 6],
            ['Temat inny', 'A to jest inna wiadomość, też testowa.', 2, 5],
            ['Temat drugi', 'Sprawdzam czy ten moduł działa!', 2, 8],
            ['Temat trzeci', 'Sialala to taka śpiewająca wiadomość to tylko.', 2, 3],
            ['Temat czwarty', 'Ej, a widziałeś nowego Harrego Pottera?', 9, 2],
            ['Test test', 'Oby zwolnili tego nowego kolegę...', 7, 2],
            ['To już 10 wiadomość!', 'O kurde, dostałem awans! Idziemy świętować!!!', 10, 2]
        ];
    }

    private function loadMessages(ObjectManager $manager): void
    {
        foreach (
            $this->getMessages() as [$topic, $text, $recipientId, $senderId]
        ) {
            $message = new Message();
            $message->setText($text)
                ->setTopic($topic)
                ->setRecipient($manager->getRepository(User::class)->find($recipientId))
                ->setCreatedBy($manager->getRepository(User::class)->find($senderId));

            $manager->persist($message);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadMessages($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
