<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Company;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    private function getProjects(): array
    {
        return [
            [
                'Aplikacja do zarządzania firmą',
                '2019-04-20 00:00:00',
                '2021-01-01 00:00:00',
                160000,
                1000000,
                9,
                1,
                null,
                true,
                '12/222-231'
            ],
            [
                'Strona WWW firmy Corporate sp. z o.o.',
                '2018-10-10 00:00:00',
                '2020-05-10 00:00:00',
                25000,
                76000,
                10,
                1,
                1,
                true,
                'TEST/111'
            ],
            [
                'Mobilna aplikacja do paragonów',
                '2019-11-05 00:00:00',
                '2020-02-01 00:00:00',
                100000,
                100000,
                10,
                1,
                2,
                true,
                null
            ],
            [
                'E-sklep dla firmy Reserved',
                '2019-01-02 00:00:00',
                '2019-12-01 00:00:00',
                600000,
                1200000,
                10,
                1,
                2,
                false,
                null
            ],
        ];
    }

    private function loadProjects(ObjectManager $manager): void
    {
        foreach (
            $this->getProjects(
            ) as [$name, $startDate, $plannedEndDate, $plannedBudget, $plannedIncome, $managerId, $companyId, $clientId, $isOpen, $code]
        ) {
            $project = new Project();
            $project->setName($name)
                ->setStartDate(new \DateTime($startDate))
                ->setPlannedEndDate(new \DateTime($plannedEndDate))
                ->setPlannedBudget($plannedBudget)
                ->setPlannedIncome($plannedIncome)
                ->setCode($code)
                ->setManager($manager->getRepository(User::class)->find($managerId))
                ->setCompany($manager->getRepository(Company::class)->find($companyId))
                ->setIsOpen($isOpen);

            if ($clientId !== null) {
                $project->setClient($manager->getRepository(Client::class)->find($clientId));
            }

            if ($isOpen === false) {
                $project->setEndDate(new \DateTime('2020-01-01 00:00:00'))
                    ->setSpentBudget(500000)
                    ->setFinalIncome(1000000);
            }

            $manager->persist($project);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadProjects($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
            ClientFixtures::class,
            UserFixtures::class,
        ];
    }
}
