<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture implements DependentFixtureInterface
{
    private function getClients(): array
    {
        return [
            [
                'Corporate Sp. z o.o.',
                1,
                '7575591012',
                '072558766',
                'ul. Architektów 109',
                '81-528 Gdynia',
                1,
                0,
                '605554683',
                'corporate@gmail.com'
            ],
            [
                'Polski Bus',
                1,
                '5696440050',
                '658087514',
                'ul. Hodowlana 24',
                '03-543 Warszawa',
                2,
                0,
                '725558344',
                'polski-bus@gmail.com'
            ],
            [
                'Cupsel',
                1,
                '5264216498',
                '570149150',
                'ul. Podhalańska 21',
                '93-224 Łódź',
                0,
                0,
                '515559227',
                'cupsel@gmail.com'
            ],
        ];
    }

    private function loadClients(ObjectManager $manager): void
    {
        foreach (
            $this->getClients(
            ) as [$name, $companyId, $nip, $regon, $address1, $address2,
            $numberOfProjects, $numberOfClosedProjects, $contactPhoneNumber, $contactEmail]
        ) {
            $client = new Client();
            $client->setName($name)
                ->setCompany($manager->getRepository(Company::class)->find($companyId))
                ->setNip($nip)
                ->setRegon($regon)
                ->setAddress1($address1)
                ->setAddress2($address2)
                ->setNumberOfProjects($numberOfProjects)
                ->setNumberOfClosedProjects($numberOfClosedProjects)
                ->setContactPhoneNumber($contactPhoneNumber)
                ->setContactEmail($contactEmail);

            $manager->persist($client);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadClients($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
        ];
    }
}
