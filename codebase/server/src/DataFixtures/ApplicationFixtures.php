<?php

namespace App\DataFixtures;

use App\Entity\Application;
use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ApplicationFixtures extends Fixture implements DependentFixtureInterface
{
    private function getApplications(): array
    {
        return [
            [
                'Wniosek o dotację na szkolenie',
                'Proszę o przyznanie mi dofinansowania na szkolenie z zakresu zarzadzania projektami',
                10000,
                5,
                null,
                'opened',
                2
            ],
            [
                'Wniosek o zwrot pieniędzy za fakturę',
                'Proszę o zwrócenie kosztów, które poniosłem w ramach zakupów firmowych',
                150,
                3,
                null,
                'opened',
                2
            ],
            [
                'Wniosek o podwyżkę',
                'Proszę o przyznanie mi podwyżki mojego wynagrodzenia o kwotę podaną poniżej',
                1500,
                7,
                2,
                'rejected',
                1
            ],
            [
                'Wniosek o dotację na szkolenie',
                'Proszę o przyznanie mi dotacji na szkolenie z Angulara w Pradze (Czechy)',
                2000,
                8,
                2,
                'passed',
                2
            ],
            [
                'Wniosek o dotację na szkolenie',
                'Proszę o przyznanie mi dotacji na szkolenie z Symfony w Berlinie (Niemcy)',
                5000,
                7,
                null,
                'opened',
                2
            ],
            [
                'Wniosek o zgłoszenie do certyfikacji z Symfony',
                'Proszę o zgłoszenie mojej osoby jako kandydata na zostanie certyfikowanym specjalistą frameworka Symfony',
                10000,
                9,
                null,
                'opened',
                2
            ],
            [
                'Wniosek o przeniesienie do działu IT',
                'Bardzo prosiłbym o przeniesienie mnie do działu IT',
                null,
                3,
                4,
                'rejected',
                1
            ],
            [
                'Test wniosek',
                'Lorem Ipsum już nie jest takie modne jak kiedyś!',
                null,
                2,
                null,
                'opened',
                1
            ],
        ];
    }

    private function loadApplications($manager): void
    {
        foreach ($this->getApplications() as [$title, $text, $amount, $userId, $servedById, $status, $departmentId]) {
            $application = new Application();
            $application->setTitle($title)
                ->setText($text)
                ->setStatus($status)
                ->setCreatedBy($manager->getRepository(User::class)->find($userId))
                ->setTargetDepartment($manager->getRepository(Department::class)->find($departmentId));

            if ($amount !== null) {
                $application->setAmount($amount);
            } else {
                $application->setAmount(null);
            }

            if ($servedById !== null) {
                $application->setServedBy($manager->getRepository(User::class)->find($servedById));
            } else {
                $application->setServedBy(null);
            }

            $manager->persist($application);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadApplications($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
