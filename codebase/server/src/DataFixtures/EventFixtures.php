<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    private function getEvents(): array
    {
        return [
            [
                'Spotkanie z klientem NovaBus Sp. z o.o.',
                '2020-01-30 10:30:00',
                '2020-01-30 12:30:00',
                1,
                'Kawiarnia na rynku',
                4,
                false,
                1,
                '10:30'
            ],
            [
                'Spotkanie z zarządem',
                '2020-02-02 08:00:00',
                '2020-01-02 10:30:00',
                1,
                'Duża sala konferencyjna',
                3,
                false,
                1,
                '08:00'
            ],
            [
                'Spotkanie z klientem TechnoMax. z o.o.',
                '2020-02-10 14:30:00',
                '2020-02-10 18:30:00',
                1,
                'Siedziba firmy klienta w Rybniku',
                4,
                false,
                1,
                '14:30'
            ],
        ];
    }

    private function loadEvents(ObjectManager $manager): void
    {
        foreach (
            $this->getEvents(
            ) as [$name, $startDate, $endDate, $participantId, $place, $createdById, $isCanceled, $companyId, $time]
        ) {
            $event = new Event();
            $event->setName($name)
                ->setStartDate(new \DateTime($startDate))
                ->setEndDate(new \DateTime($endDate))
                ->setParticipant($manager->getRepository(User::class)->find($participantId))
                ->setPlace($place)
                ->setCreatedBy($manager->getRepository(User::class)->find($createdById))
                ->setIsCanceled($isCanceled)
                ->setTime($time)
                ->setCompany($manager->getRepository(Company::class)->find($companyId));

            $manager->persist($event);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadEvents($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
            UserFixtures::class
        ];
    }
}
