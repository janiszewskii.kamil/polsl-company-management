<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Department;
use App\Entity\Item;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ItemFixtures extends Fixture implements DependentFixtureInterface
{
    private function getItems(): array
    {
        return [
            ['Samochód Audi A8', 1, null, '2023-02-02', 'Samochód służbowy', 'leasing', 1],
            ['Laptop DELL Latitude 5490', null, 6, '2021-01-15', 'Laptop pracowniczy', 'own', 1],
            ['Drukarka CANON', 2, null, null, 'Drukarka firmowa', 'leasing', 1],
            ['Laptop DELL Latitude 5490', null, 7, '2021-01-15', 'Laptop pracowniczy', 'own', 1],
            ['Komputer APPLE iMac 27', null, 8, '2025-11-01', 'Komputer pracowniczy', 'leasing', 1],
        ];
    }

    private function loadItems(ObjectManager $manager): void
    {
        foreach (
            $this->getItems(
            ) as [$name, $departmentId, $employeeId, $warrantyEndDate, $description, $typeOfOwnership, $companyId]
        ) {
            $item = new Item();
            $item->setName($name)
                ->setCompany($manager->getRepository(Company::class)->find($companyId))
                ->setTypeOfOwnership($typeOfOwnership)
                ->setDescription($description)
                ->setWarrantyEndDate(new \DateTime($warrantyEndDate));

            if ($employeeId) {
                $item->setUser($manager->getRepository(User::class)->find($employeeId));
            }

            if ($departmentId) {
                $item->setDepartment($manager->getRepository(Department::class)->find($departmentId));
            }

            $manager->persist($item);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadItems($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DepartmentFixtures::class,
            UserFixtures::class,
        ];
    }
}
