<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\News;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    private function getNews(): array
    {
        return [
            [
                'Witamy kolegę!',
                'Do naszego zespołu dołączył nowy kolega Jan Nowask, powitajmy go wszyscy!',
                'advertisement',
                1,
                3,
                '2020-01-25 15:30:00'
            ],
            [
                'Wyjazd integracyjny',
                'Chcemy poinformować, że oficjalnie rozpoczęliśmy zapisy na wyjazd integracyjny, który odbędzie się w maju.',
                'advertisement',
                1,
                3,
                '2020-01-15 09:25:00'
            ],
            [
                'Szkolenie z Angulara',
                'Zbieramy chętnych na wyjazd do Pragi na szkolenie z Angulara za tydzień.',
                'information',
                1,
                3,
                '2020-01-14 10:11:00'
            ],
            [
                'Szkolenie z Symfony',
                'Zbieramy chętnych na wyjazd do Paryża na szkolenie z Symfony za 2 miesiące (marzec).',
                'information',
                1,
                3,
                '2020-01-13 07:01:00'
            ],
            [
                'Zbiórka na WOŚP',
                'Z okazji zbliżającej się Wielkiej Orkierstry Światecznej Pomocy, chcielibyśmy przypomnieć, że w Kuchni znajdują się puszki do których można wrzucać pieniądze.',
                'advertisement',
                1,
                3,
                '2020-01-02 13:18:00'
            ],
            [
                'Zbliża się remont open-space na 2 piętrze',
                'Pragniemy przypomnieć, że w przyszłym tygodniu rozpocznie się zapowiadany remont na 2 piętrze. W związku z tym, na ten czas prosimy o zebranie swoich rzeczy prywatnych.',
                'important',
                1,
                3,
                '2019-12-29 10:23:00'
            ],
        ];
    }

    private function loadNews(ObjectManager $manager): void
    {
        foreach (
            $this->getNews() as [$title, $text, $type, $companyId, $userId, $createdAt]
        ) {
            $news = new News();
            $news->setText($text)
                ->setTitle($title)
                ->setType($type)
                ->setCreatedAt(new \DateTime($createdAt))
                ->setCreatedBy($manager->getRepository(User::class)->find($userId))
                ->setCompany($manager->getRepository(Company::class)->find($companyId));

            $manager->persist($news);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadNews($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CompanyFixtures::class,
        ];
    }
}
