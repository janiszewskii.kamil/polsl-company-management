<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Department;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DepartmentFixtures extends Fixture implements DependentFixtureInterface
{
    private function getDepartmens(): array
    {
        return [
            ['Zarząd', 'Dział kierowniczy firmy', 1],
            ['Księgowość', 'Dział kierowniczy firmy', 1],
            ['IT', 'Dział kierowniczy firmy', 1]
        ];
    }

    private function loadDepartments(ObjectManager $manager): void
    {
        foreach (
            $this->getDepartmens() as [$name, $description, $companyId]
        ) {
            $department = new Department();
            $department->setName($name)
                ->setDescription($description)
                ->setCompany($manager->getRepository(Company::class)->find($companyId));

            $manager->persist($department);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadDepartments($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CompanyFixtures::class,
        ];
    }
}
