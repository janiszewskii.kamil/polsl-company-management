<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\UserPosition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PositionFixutres extends Fixture implements DependentFixtureInterface
{
    private function getPositions(): array
    {
        return [
            ['Prezes zarządu', true, 1, 'Najwyższe stanowisko dostępne w firmie.'],
            ['Kadrowy', true, 1, 'Zajmuje się sprawami związanymi z pracownikami.'],
            ['Księgowy', false, 1, 'Głównie zajmuje się finansami i dokumentami w firmie.'],
            ['Młodszy programista', false, 1, 'Stanowisko wykonawcze.'],
            ['Starszy programista', false, 1, 'Stanowisko wykonawcze.'],
            ['Stażysta programista', false, 1, 'Stanowisko bez etatu.'],
        ];
    }

    private function loadPositions(ObjectManager $manager): void
    {
        foreach (
            $this->getPositions() as [$name, $isManagerialPosition, $companyId, $description]
        ) {
            $position = new UserPosition();
            $position->setName($name)
                ->setIsManagerialPosition($isManagerialPosition)
                ->setDescription($description)
                ->setCompany($manager->getRepository(Company::class)->find($companyId));

            $manager->persist($position);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadPositions($manager);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CompanyFixtures::class,
        );
    }
}
