<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CompanyFixtures extends Fixture
{

    private function getCompanies(): array
    {
        return [
            [
                'The Big Software House Sp. z o.o.',
                'Usługi IT',
                '1226243953',
                '12345678901234',
                'ul. Nowa 12/23',
                '33-333 Szczecin'
            ]
        ];
    }

    private function loadCompanies(ObjectManager $manager): void
    {
        foreach ($this->getCompanies() as [$name, $typeOfBusiness, $nip, $regon, $address1, $address2]) {
            $company = new Company();
            $company->setName($name)
                ->setNip($nip)
                ->setRegon($regon)
                ->setAddress1($address1)
                ->setAddress2($address2)
                ->setTypeOfBusiness($typeOfBusiness);

            $manager->persist($company);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadCompanies($manager);

        $manager->flush();
    }
}
