<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPositionRepository")
 */
class UserPosition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"userPositionDetails", "userDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"userPositionDetails", "userDetails", "userGeneral"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"userPositionDetails", "userDetails"})
     */
    private $isManagerialPosition;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="userPositions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"userPositionDetails"})
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsManagerialPosition(): ?bool
    {
        return $this->isManagerialPosition;
    }

    public function setIsManagerialPosition(bool $isManagerialPosition): self
    {
        $this->isManagerialPosition = $isManagerialPosition;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
