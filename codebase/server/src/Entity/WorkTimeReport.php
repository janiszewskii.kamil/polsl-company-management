<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WorkTimeReportRepository")
 */
class WorkTimeReport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"workTimeReportDetails"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="timeRecords")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"workTimeReportDetails"})
     */
    private $project;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"workTimeReportDetails"})
     */
    private $message;

    /**
     * @ORM\Column(type="float")
     * @Groups({"workTimeReportDetails"})
     */
    private $time;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"workTimeReportDetails"})
     */
    private $reportForDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"workTimeReportDetails"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workingTimeRecords")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"workTimeReportDetails"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"workTimeReportDetails"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"workTimeReportDetails"})
     */
    private $type;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getTime(): ?float
    {
        return $this->time;
    }

    public function setTime(float $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getReportForDate(): ?\DateTimeInterface
    {
        return $this->reportForDate;
    }

    public function setReportForDate(\DateTimeInterface $reportForDate): self
    {
        $this->reportForDate = $reportForDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdateAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
