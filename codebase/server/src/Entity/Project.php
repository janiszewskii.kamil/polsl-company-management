<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"projectDetails", "projectGeneral", "workTimeReportDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"projectDetails", "projectGeneral", "workTimeReportDetails"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"projectDetails"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $plannedEndDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $plannedBudget;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $spentBudget;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $plannedIncome;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $finalIncome;

    /**
     * @ORM\OneToMany(targetEntity="WorkTimeReport", mappedBy="project", orphanRemoval=true)
     * @Groups({"projectDetails"})
     */
    private $timeRecords;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"projectDetails"})
     */
    private $manager;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"projectDetails"})
     */
    private $isOpen;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"projectDetails"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"projectDetails"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="projects")
     * @Groups({"projectDetails"})
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"projectDetails"})
     */
    private $code;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"projectDetails"})
     */
    private $description;

    public function __construct()
    {
        $this->timeRecords = new ArrayCollection();
        $this->isOpen = true;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getPlannedEndDate(): ?\DateTimeInterface
    {
        return $this->plannedEndDate;
    }

    public function setPlannedEndDate(?\DateTimeInterface $plannedEndDate): self
    {
        $this->plannedEndDate = $plannedEndDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPlannedBudget(): ?float
    {
        return $this->plannedBudget;
    }

    public function setPlannedBudget(?float $plannedBudget): self
    {
        $this->plannedBudget = $plannedBudget;

        return $this;
    }

    public function getSpentBudget(): ?float
    {
        return $this->spentBudget;
    }

    public function setSpentBudget(?float $spentBudget): self
    {
        $this->spentBudget = $spentBudget;

        return $this;
    }

    public function getPlannedIncome(): ?float
    {
        return $this->plannedIncome;
    }

    public function setPlannedIncome(?float $plannedIncome): self
    {
        $this->plannedIncome = $plannedIncome;

        return $this;
    }

    public function getFinalIncome(): ?float
    {
        return $this->finalIncome;
    }

    public function setFinalIncome(?float $finalIncome): self
    {
        $this->finalIncome = $finalIncome;

        return $this;
    }

    /**
     * @return Collection|WorkTimeReport[]
     */
    public function getTimeRecords(): Collection
    {
        return $this->timeRecords;
    }

    public function addTimeRecord(WorkTimeReport $timeRecord): self
    {
        if (!$this->timeRecords->contains($timeRecord)) {
            $this->timeRecords[] = $timeRecord;
            $timeRecord->setProject($this);
        }

        return $this;
    }

    public function removeTimeRecord(WorkTimeReport $timeRecord): self
    {
        if ($this->timeRecords->contains($timeRecord)) {
            $this->timeRecords->removeElement($timeRecord);
            // set the owning side to null (unless already changed)
            if ($timeRecord->getProject() === $this) {
                $timeRecord->setProject(null);
            }
        }

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getIsOpen(): ?bool
    {
        return $this->isOpen;
    }

    public function setIsOpen(bool $isOpen): self
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
