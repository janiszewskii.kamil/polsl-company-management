<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="`user`")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    public const ALLOWED_PERMISSIONS = [
        'canViewClients',
        'canEditClients',
        'canViewProjects',
        'canEditProjects',
        'canViewItems',
        'canEditItems',
        'canViewWorkTimeReports',
        'canViewCompany',
        'canEditCompany',
        'canEditEmployees',
        'canViewEmployees',
        'canEditEvents',
        'canViewEvents',
        'canServeApplications',
        'canViewApplications',
        'canPublishNews'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({
     *     "userDetails",
     *     "userGeneral",
     *     "projectDetails",
     *     "workTimeReportDetails",
     *     "applicationDetails",
     *     "departmentDetails",
     *     "eventDetails",
     *     "itemDetails"
     *     })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"userDetails", "userGeneral"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="WorkTimeReport", mappedBy="createdBy", orphanRemoval=true)
     */
    private $workingTimeRecords;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Application", mappedBy="createdBy", orphanRemoval=true)
     */
    private $applications;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"userDetails"})
     */
    private $hireDate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserData", cascade={"persist", "remove"})
     * @Groups({
     *     "userDetails",
     *     "userGeneral",
     *     "newsDetails",
     *     "projectDetails",
     *     "departmentDetails",
     *     "messageSent",
     *     "messageReceived",
     *     "workTimeReportDetails",
     *     "applicationDetails",
     *     "companyDetails",
     *     "eventDetails",
     *     "itemDetails"
     * })
     */
    private $data;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"userDetails"})
     */
    private $permissions = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAnonymized;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"userDetails"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"userDetails"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserPosition")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"userDetails", "userGeneral"})
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"userDetails"})
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temporaryPlainPassword;

    public function __construct()
    {
        $this->workingTimeRecords = new ArrayCollection();
        $this->applications = new ArrayCollection();
        $this->isActive = true;
        $this->isAnonymized = false;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|WorkTimeReport[]
     */
    public function getWorkingTimeRecords(): Collection
    {
        return $this->workingTimeRecords;
    }

    public function addWorkingTimeRecord(WorkTimeReport $timeRecord): self
    {
        if (!$this->workingTimeRecords->contains($timeRecord)) {
            $this->workingTimeRecords[] = $timeRecord;
            $timeRecord->setCreatedBy($this);
        }

        return $this;
    }

    public function removeWorkingTimeRecord(WorkTimeReport $timeRecord): self
    {
        if ($this->workingTimeRecords->contains($timeRecord)) {
            $this->workingTimeRecords->removeElement($timeRecord);
            // set the owning side to null (unless already changed)
            if ($timeRecord->getCreatedBy() === $this) {
                $timeRecord->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setCreatedBy($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->contains($application)) {
            $this->applications->removeElement($application);
            // set the owning side to null (unless already changed)
            if ($application->getCreatedBy() === $this) {
                $application->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getHireDate(): ?\DateTimeInterface
    {
        return $this->hireDate;
    }

    public function setHireDate(?\DateTimeInterface $hireDate): self
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    public function getData(): ?UserData
    {
        return $this->data;
    }

    public function setData(?UserData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getPermissions(): ?array
    {
        return $this->permissions;
    }

    public function setPermissions(?array $permissions): self
    {
        $this->permissions = $permissions;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsAnonymized(): ?bool
    {
        return $this->isAnonymized;
    }

    public function setIsAnonymized(bool $isAnonymized): self
    {
        $this->isAnonymized = $isAnonymized;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPosition(): ?UserPosition
    {
        return $this->position;
    }

    public function setPosition(?UserPosition $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getTemporaryPlainPassword(): ?string
    {
        return $this->temporaryPlainPassword;
    }

    public function setTemporaryPlainPassword(?string $temporaryPlainPassword): self
    {
        $this->temporaryPlainPassword = $temporaryPlainPassword;

        return $this;
    }
}
