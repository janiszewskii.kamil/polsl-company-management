<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationRepository")
 */
class Application
{
    private const OPENED = 'opened';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"applicationDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"applicationDetails"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"applicationDetails"})
     */
    private $text;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"applicationDetails"})
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"applicationDetails"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"applicationDetails"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"applicationDetails"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"applicationDetails"})
     */
    private $servedBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"applicationDetails"})
     */
    private $targetDepartment;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $rejectionReason;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = self::OPENED;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getServedBy(): ?User
    {
        return $this->servedBy;
    }

    public function setServedBy(?User $servedBy): self
    {
        $this->servedBy = $servedBy;

        return $this;
    }

    public function getTargetDepartment(): ?Department
    {
        return $this->targetDepartment;
    }

    public function setTargetDepartment(?Department $targetDepartment): self
    {
        $this->targetDepartment = $targetDepartment;

        return $this;
    }

    public function getRejectionReason(): ?string
    {
        return $this->rejectionReason;
    }

    public function setRejectionReason(?string $rejectionReason): self
    {
        $this->rejectionReason = $rejectionReason;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
