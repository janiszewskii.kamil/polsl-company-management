<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"clientDetails", "projectDetails"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"clientDetails", "projectDetails"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="clients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="client")
     */
    private $projects;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $regon;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $nip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $contactPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"clientDetails"})
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"clientDetails"})
     */
    private $numberOfProjects;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"clientDetails"})
     */
    private $numberOfClosedProjects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setClient($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getClient() === $this) {
                $project->setClient(null);
            }
        }

        return $this;
    }

    public function getRegon(): ?string
    {
        return $this->regon;
    }

    public function setRegon(?string $regon): self
    {
        $this->regon = $regon;

        return $this;
    }

    public function getNip(): ?string
    {
        return $this->nip;
    }

    public function setNip(?string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getContactPhoneNumber(): ?string
    {
        return $this->contactPhoneNumber;
    }

    public function setContactPhoneNumber(?string $contactPhoneNumber): self
    {
        $this->contactPhoneNumber = $contactPhoneNumber;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getNumberOfProjects(): ?int
    {
        return $this->numberOfProjects;
    }

    public function setNumberOfProjects(?int $numberOfProjects): self
    {
        $this->numberOfProjects = $numberOfProjects;

        return $this;
    }

    public function getNumberOfClosedProjects(): ?int
    {
        return $this->numberOfClosedProjects;
    }

    public function setNumberOfClosedProjects(?int $numberOfClosedProjects): self
    {
        $this->numberOfClosedProjects = $numberOfClosedProjects;

        return $this;
    }
}
