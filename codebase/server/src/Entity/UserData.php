<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserDataRepository")
 */
class UserData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "userDetails",
           "userGeneral",
     *     "newsDetails",
     *     "projectDetails",
     *     "departmentDetails",
     *     "messageSent",
     *     "messageReceived",
     *     "workTimeReportDetails",
     *     "applicationDetails",
     *     "companyDetails",
     *     "eventDetails",
     *     "itemDetails"
     * })
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "userDetails",
     *     "userGeneral",
     *     "newsDetails",
     *     "projectDetails",
     *     "departmentDetails",
     *     "messageSent",
     *     "messageReceived",
     *     "workTimeReportDetails",
     *     "applicationDetails",
     *     "companyDetails",
     *     "eventDetails",
     *     "itemDetails"
     * })
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"userDetails"})
     */
    private $gender;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"userDetails"})
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"userDetails"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"userDetails"})
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"userDetails"})
     */
    private $houseNo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"userDetails"})
     */
    private $flatNo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"userDetails"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"userDetails"})
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"userDetails"})
     */
    private $province;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouseNo(): ?string
    {
        return $this->houseNo;
    }

    public function setHouseNo(?string $houseNo): self
    {
        $this->houseNo = $houseNo;

        return $this;
    }

    public function getFlatNo(): ?string
    {
        return $this->flatNo;
    }

    public function setFlatNo(?string $flatNo): self
    {
        $this->flatNo = $flatNo;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getProvince(): ?string
    {
        return $this->province;
    }

    public function setProvince(?string $province): self
    {
        $this->province = $province;

        return $this;
    }
}
