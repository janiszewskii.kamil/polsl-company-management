<?php

namespace App\Mailer;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;


final class Sender implements SenderInterface
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(
        string $from,
        array $recipients,
        string $subject,
        string $htmlTemplate,
        array $data,
        string $txtTemplate = null,
        array $attachments = null
    ): void {
        $toAddresses = $this->prepareToAddresses($recipients);
        $email = (new TemplatedEmail())
            ->from($from)
            ->to(...$toAddresses)
            ->subject($subject)
            ->htmlTemplate($htmlTemplate)
            ->context($data);

        $this->mailer->send($email);
    }

    private function prepareToAddresses(array $recipients): array
    {
        $toAddresses = [];
        $counter = 0;

        foreach ($recipients as $recipient) {
            if ($counter === 0) {
                $toAddresses[] = $recipient;
            } else {
                $toAddresses[] = new Address($recipient);
            }
            $counter++;
        }

        return $toAddresses;
    }
}
