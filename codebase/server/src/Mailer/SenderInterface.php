<?php

namespace App\Mailer;

interface SenderInterface
{
    public function send(
        string $from,
        array $recipients,
        string $subject,
        string $htmlTemplate,
        array $data,
        string $txtTemplate = null,
        array $attachments = null
    ): void;
}
