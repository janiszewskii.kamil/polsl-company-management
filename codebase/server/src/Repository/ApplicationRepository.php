<?php

namespace App\Repository;

use App\Entity\Application;
use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Application::class);
    }

    public function findAllByCompany(Company $company, string $status): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('u.company = :company')
            ->andWhere('a.status = :status')
            ->leftJoin('a.createdBy', 'u')
            ->setParameter('company', $company)
            ->setParameter('status', $status);

        return $qb->getQuery()->execute();
    }
}
