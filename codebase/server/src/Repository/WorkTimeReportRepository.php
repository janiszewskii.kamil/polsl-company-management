<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\WorkTimeReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WorkTimeReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkTimeReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkTimeReport[]    findAll()
 * @method WorkTimeReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkTimeReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkTimeReport::class);
    }

    public function findAllByCompany(Company $company): array
    {
        $qb = $this->createQueryBuilder('wtr')
            ->where('u.company = :company')
            ->leftJoin('wtr.createdBy', 'u')
            ->setParameter('company', $company);

        return $qb->getQuery()->execute();
    }
}
