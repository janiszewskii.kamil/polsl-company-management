<?php

namespace App\Repository;

use App\Entity\BlackListJwt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BlackListJwt|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlackListJwt|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlackListJwt[]    findAll()
 * @method BlackListJwt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlackListJwtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlackListJwt::class);
    }

    // /**
    //  * @return BlackListJwt[] Returns an array of BlackListJwt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlackListJwt
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
