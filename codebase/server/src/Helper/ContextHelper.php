<?php

namespace App\Helper;

use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ContextHelper
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getCurrentCompanyContext(): Company
    {
        return $this->security->getUser()->getCompany();
    }

    public function getCurrentUser(): UserInterface
    {
        return $this->security->getUser();
    }
}
