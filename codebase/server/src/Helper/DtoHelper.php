<?php
declare(strict_types=1);

namespace App\Helper;

use Symfony\Component\Validator\Validation;

class DtoHelper
{
    public static function validate(array $dto, $constraints): array
    {
        $errors = [];
        $validator = Validation::createValidator();
        $violations = $validator->validate($dto, $constraints);

        if ($violations->count()) {
            foreach ($violations as $violation) {
                $message = str_replace(['[', ']'], '', $violation->getMessage());
                $field = str_replace(['[', ']'], '', $violation->getPropertyPath());
                $errors[] = $field . ' ' . $message;
            }
        }

        return $errors;
    }
}
