<?php

namespace App\Enum;

class RegexEnum
{
    public const LETTERS_ONLY_REGEX = '/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$/';
    public const NUMBERS_ONLY_REGEX = '/^[0-9]+$/';
    public const FLOAT_REGEX = '/^[0-9]+(?:\.[0-9]{1,2})?$/';
}