<?php

namespace App\Service;

use App\Helper\ContextHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AccountService extends ApiService
{
    private $passwordEncoder;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        ContextHelper $contextHelper,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->passwordEncoder = $passwordEncoder;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function changePassword(Request $request): void
    {
        $currentUser = $this->contextHelper->getCurrentUser();
        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');
        $confirmNewPassword = $request->get('confirmNewPassword');

        if ($newPassword === '' ||
            $confirmNewPassword === '' ||
            $confirmNewPassword === 'null' ||
            $confirmNewPassword === null ||
            !($confirmNewPassword === $newPassword) ||
            !$this->passwordEncoder->isPasswordValid($currentUser, $currentPassword)) {
            $this->throwHttpBadRequestException('invalidNewPasswordRequest');
        }
        $currentUser->setPassword($this->passwordEncoder->encodePassword($currentUser, $newPassword));
        $this->entityManager->persist($currentUser);
        $this->entityManager->flush();
    }
}
