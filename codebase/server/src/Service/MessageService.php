<?php

namespace App\Service;

use App\Constraint\MessageConstraint;
use App\Dto\MessageDto;
use App\Entity\User;
use App\Factory\MessageFactory;
use App\Helper\DtoHelper;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class MessageService extends ApiService
{
    private $messageRepository;
    private $messageFactory;

    public function __construct(
        MessageRepository $messageRepository,
        MessageFactory $messageFactory,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->messageRepository = $messageRepository;
        $this->messageFactory = $messageFactory;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getAllReceivedByUser(User $user): array
    {
        return $this->messageRepository->findBy(['recipient' => $user]);
    }

    public function getAllSentByUser(User $user): array
    {
        return $this->messageRepository->findBy(['createdBy' => $user]);
    }

    public function send(Request $request): void
    {
        $message = $this->messageFactory->create($this->prepareDto($request));
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): MessageDto
    {
        $messageDto = $this->serializer->deserialize($request->getContent(), MessageDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($messageDto), MessageConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidMessageRequest');
        }

        return $messageDto;
    }
}
