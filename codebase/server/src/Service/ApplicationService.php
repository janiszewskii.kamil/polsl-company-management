<?php

namespace App\Service;

use App\Constraint\ApplicationConstraint;
use App\Dto\ApplicationDto;
use App\Entity\Application;
use App\Entity\Company;
use App\Entity\User;
use App\Factory\ApplicationFactory;
use App\Helper\ContextHelper;
use App\Helper\DtoHelper;
use App\Repository\ApplicationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ApplicationService extends ApiService
{
    private $applicationRepository;
    private $applicationFactory;

    public function __construct(
        ApplicationRepository $applicationRepository,
        ApplicationFactory $applicationFactory,
        ContextHelper $contextHelper,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->applicationFactory = $applicationFactory;
        $this->applicationRepository = $applicationRepository;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company, string $status): array
    {
        return $this->applicationRepository->findAllByCompany($company, $status);
    }

    public function getAllByUser(User $user, ?string $status): array
    {
        if ($status) {
            return $this->applicationRepository->findBy(['createdBy' => $user, 'status' => $status]);
        } else {
            return $this->applicationRepository->findBy(['createdBy' => $user]);
        }
    }

    public function create(Request $request): Application
    {
        $application = $this->applicationFactory->create($this->prepareDto($request));
        $this->entityManager->persist($application);
        $this->entityManager->flush();

        return $application;
    }

    public function accept(Application $application): void
    {
        $application->setStatus('passed')
            ->setUpdatedAt(new \DateTime())
            ->setServedBy($this->contextHelper->getCurrentUser());
        $this->entityManager->persist($application);
        $this->entityManager->flush();
    }

    public function reject(Application $application, Request $request): void
    {
        $application->setStatus('rejected')
            ->setUpdatedAt(new \DateTime())
            ->setServedBy($this->contextHelper->getCurrentUser())
            ->setRejectionReason($request->get('rejectionReason'));
        $this->entityManager->persist($application);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): ApplicationDto
    {
        $applicationDto = $this->serializer->deserialize($request->getContent(), ApplicationDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($applicationDto), ApplicationConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidApplicationRequest');
        }

        return $applicationDto;
    }
}
