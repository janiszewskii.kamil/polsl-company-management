<?php

namespace App\Service;

use App\Helper\ContextHelper;
use App\Traits\ExceptionTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class ApiService
{
    use ExceptionTrait;

    protected $contextHelper;
    protected $entityManager;
    protected $serializer;

    public function __construct(ContextHelper $contextHelper = null, EntityManagerInterface $entityManager = null, SerializerInterface $serializer = null)
    {
        $this->contextHelper = $contextHelper;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }
}
