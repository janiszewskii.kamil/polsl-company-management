<?php

namespace App\Service;

use App\Constraint\DepartmentConstraint;
use App\Dto\DepartmentDto;
use App\Entity\Company;
use App\Entity\Department;
use App\Factory\DepartmentFactory;
use App\Helper\DtoHelper;
use App\Repository\ApplicationRepository;
use App\Repository\DepartmentRepository;
use App\Repository\ItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class DepartmentService extends ApiService
{
    private $departmentFactory;
    private $departmentRepository;
    private $itemRepository;
    private $applicationRepository;
    private $companyService;

    public function __construct(
        DepartmentRepository $departmentRepository,
        DepartmentFactory $departmentFactory,
        ApplicationRepository $applicationRepository,
        ItemRepository $itemRepository,
        CompanyService $companyService,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->departmentRepository = $departmentRepository;
        $this->applicationRepository = $applicationRepository;
        $this->itemRepository = $itemRepository;
        $this->departmentFactory = $departmentFactory;
        $this->companyService = $companyService;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getById(int $id): Department
    {
        return $this->departmentRepository->find($id);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->departmentRepository->findBy(['company' => $company]);
    }

    public function create(Request $request): Department
    {
        $department = $this->departmentFactory->create($this->prepareDto($request));
        $this->entityManager->persist($department);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $department;
    }

    public function update(Request $request, Department $department): Department
    {
        $updatedDepartment = $this->departmentFactory->update($this->prepareDto($request), $department);
        $this->entityManager->persist($updatedDepartment);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $updatedDepartment;
    }

    public function remove(Department $department): void
    {
        $openedDepartmentApplications = $this->applicationRepository->findBy(
            ['status' => 'opened', 'targetDepartment' => $department]
        );

        if ($openedDepartmentApplications) {
            $this->throwHttpBadRequestException('openedApplicationsForDepartment');
        }

        $departmentUsers = $department->getUsers();
        foreach ($departmentUsers as $departmentUser) {
            $departmentUser->setDepartment(null);
            $this->entityManager->persist($departmentUser);
        }
        $departmentApplications = $this->applicationRepository->findBy(['targetDepartment' => $department]);
        foreach ($departmentApplications as $departmentApplication) {
            $departmentApplication->setTargetDepartment(null);
            $this->entityManager->persist($departmentApplication);
        }
        $departmentItems = $this->itemRepository->findBy(['department' => $department]);
        foreach ($departmentItems as $departmentItem) {
            $departmentItem->setDepartment(null);
            $this->entityManager->persist($departmentItem);
        }
        $this->entityManager->remove($department);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();
    }


    private function prepareDto(Request $request): DepartmentDto
    {
        $departmentDto = $this->serializer->deserialize($request->getContent(), DepartmentDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($departmentDto), DepartmentConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidDepartmentRequest');
        }

        return $departmentDto;
    }
}
