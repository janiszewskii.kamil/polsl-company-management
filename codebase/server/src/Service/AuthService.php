<?php

namespace App\Service;

use App\Message\ResetPasswordMail;
use App\Repository\UserRepository;
use App\Service\Token\ApiTokenInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthService extends ApiService
{
    private $userRepository;
    private $jwtTokenService;
    private $messageBus;
    private $passwordEncoder;

    public function __construct(
        UserRepository $userRepository,
        ApiTokenInterface $jwtTokenService,
        MessageBusInterface $messageBus,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ) {
        $this->userRepository = $userRepository;
        $this->jwtTokenService = $jwtTokenService;
        $this->messageBus = $messageBus;
        $this->passwordEncoder = $passwordEncoder;
        parent::__construct(null, $entityManager, null);
    }

    public function logout(): void
    {
        $this->jwtTokenService->deactivateToken();
    }

    public function resetPassword(Request $request): void
    {
        $email = $request->get('resetEmail');
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if (!$user) {
            $this->throwHttpNotFoundException('emailNotFound');
        } else {
            $randPassword = sha1(random_bytes(8));
            $newPassword = $this->passwordEncoder->encodePassword($user, $randPassword);
            $user->setPassword($newPassword)
                ->setTemporaryPlainPassword($randPassword);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->messageBus->dispatch(new ResetPasswordMail($user->getId()));
        }
    }
}
