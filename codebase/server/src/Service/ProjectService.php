<?php

namespace App\Service;

use App\Constraint\ProjectConstraint;
use App\Dto\ProjectDto;
use App\Entity\Company;
use App\Entity\Project;
use App\Enum\RegexEnum;
use App\Factory\ProjectFactory;
use App\Helper\DtoHelper;
use App\Repository\ProjectRepository;
use App\Traits\ProjectStatisticsTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ProjectService extends ApiService
{
    use ProjectStatisticsTrait;

    private $projectRepository;
    private $companyService;
    private $projectFactory;

    public function __construct(
        ProjectRepository $projectRepository,
        ProjectFactory $projectFactory,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->projectRepository = $projectRepository;
        $this->projectFactory = $projectFactory;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->projectRepository->findBy(['company' => $company]);
    }

    public function getAllOpenedByCompany(Company $company): array
    {
        return $this->projectRepository->findBy(['company' => $company, 'isOpen' => true]);
    }

    public function getAllClosedByCompany(Company $company): array
    {
        return $this->projectRepository->findBy(['company' => $company, 'isOpen' => false]);
    }

    public function getStatisticsByProject(Project $project): array
    {
        return [
            'totalFullDays' => $this->countTotalFullDays($project),
            'totalTime' => $this->countTotalTime($project),
            'totalDelayedDays' => $this->countTotalDelayedDays($project),
            'totalProfit' => $this->countTotalProfit($project),
            'totalSubmittedReports' => $this->countTotalSubmittedReports($project),
            'totalEmployees' => $this->countTotalEmployees($project),
        ];
    }

    public function create(Request $request): Project
    {
        $project = $this->projectFactory->create($this->prepareDto($request));
        $client = $project->getClient();

        if ($client) {
            $client->setNumberOfProjects($client->getNumberOfProjects() + 1);
            $this->entityManager->persist($client);
        }

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        return $project;
    }

    public function update(Request $request, Project $project): Project
    {
        $updatedProject = $this->projectFactory->update($this->prepareDto($request), $project);
        $this->entityManager->persist($updatedProject);
        $this->entityManager->flush();

        return $updatedProject;
    }

    public function close(Request $request, Project $project): void
    {
        if (!preg_match(RegexEnum::FLOAT_REGEX, $request->get('finalIncome')) ||
            !preg_match(RegexEnum::FLOAT_REGEX, $request->get('spentBudget')) ||
            !is_string($request->get('endDate'))) {
            $this->throwHttpBadRequestException('invalidProjectRequest');
        }

        $project->setIsOpen(false)
            ->setUpdatedAt(new \DateTime())
            ->setFinalIncome(floatval($request->get('finalIncome')))
            ->setSpentBudget(floatval($request->get('spentBudget')))
            ->setEndDate(new \DateTime($request->get('endDate')));;

        $client = $project->getClient();

        if ($client) {
            $client->setNumberOfClosedProjects($client->getNumberOfClosedProjects() + 1);
            $this->entityManager->persist($client);
        }

        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): ProjectDto
    {
        $projectDto = $this->serializer->deserialize($request->getContent(), ProjectDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($projectDto), ProjectConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidProjectRequest');
        }

        return $projectDto;
    }
}
