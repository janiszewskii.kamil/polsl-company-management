<?php

namespace App\Service;

use App\Constraint\UserConstraint;
use App\Constraint\UserDataConstraint;
use App\Dto\UserDataDto;
use App\Dto\UserDto;
use App\Entity\Company;
use App\Entity\User;
use App\Factory\UserDataFactory;
use App\Factory\UserFactory;
use App\Helper\ContextHelper;
use App\Helper\DtoHelper;
use App\Message\NewUserMail;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Date;

class UserService extends ApiService
{
    private $userRepository;
    private $userFactory;
    private $userDataFactory;
    private $messageBus;

    public function __construct(
        UserRepository $userRepository,
        UserFactory $userFactory,
        UserDataFactory $userDataFactory,
        ContextHelper $contextHelper,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        MessageBusInterface $messageBus
    ) {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
        $this->userDataFactory = $userDataFactory;
        $this->messageBus = $messageBus;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->userRepository->findBy(['company' => $company, 'isAnonymized' => false]);
    }

    public function getById(int $id): User
    {
        return $this->userRepository->find($id);
    }

    public function create(Request $request): User
    {
        $user = $this->userFactory->create($this->prepareUserDto($request));
        $userData = $this->userDataFactory->create($this->prepareUserDataDto($request));
        $user->setData($userData);
        $this->entityManager->persist($user);
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
        $this->messageBus->dispatch(new NewUserMail($user->getId(), $user->getTemporaryPlainPassword()));

        return $user;
    }

    public function update(Request $request, User $user): User
    {
        $updatedUser = $this->userFactory->update($this->prepareUserDto($request), $user);
        $updatedDataUser = $this->userDataFactory->update($this->prepareUserDataDto($request), $user->getData());
        $this->entityManager->persist($updatedUser);
        $this->entityManager->persist($updatedDataUser);
        $this->entityManager->flush();

        return $updatedUser;
    }

    public function getPermissionsByUser(User $user): array
    {
        return $user->getPermissions();
    }

    public function updatePermissions(Request $request, User $user): void
    {
        $content = json_decode($request->getContent(), true);
        $permissions = [];
        foreach ($content as $key => $value) {
            if (in_array($key, User::ALLOWED_PERMISSIONS)) {
                $permissions[$key] = $value;
            }
        }
        $user->setPermissions($permissions);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function anonymize(User $user): void
    {
        $userId = $user->getId();
        $user->setIsAnonymized(true)
            ->setIsActive(false)
            ->setHireDate(null)
            ->setPosition(null)
            ->setEmail('anonymizedUserId' . $userId)
            ->setUpdatedAt(new \DateTime())
            ->setPassword(uniqid());

        $userData = $user->getData();
        $userData->setZipCode('anonymizedUserId' . $userId)
            ->setPhoneNumber('anonymizedUserId' . $userId)
            ->setProvince('anonymizedUserId' . $userId)
            ->setGender(null)
            ->setFlatNo('anonymizedUserId' . $userId)
            ->setHouseNo('anonymizedUserId' . $userId)
            ->setCity('anonymizedUserId' . $userId)
            ->setBirthDate(null)
            ->setLastName('anonymizedUserId' . $userId)
            ->setFirstName('anonymizedUserId' . $userId)
            ->setStreet('anonymizedUserId' . $userId);

        $this->entityManager->persist($user);
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
    }

    private function prepareUserDto(Request $request): UserDto
    {
        $userDto = $this->serializer->deserialize($request->getContent(), UserDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($userDto), UserConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidUserRequest');
        }

        return $userDto;
    }

    private function prepareUserDataDto(Request $request): UserDataDto
    {
        $userDataDto = $this->serializer->deserialize($request->getContent(), UserDataDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($userDataDto), UserDataConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidUserDataRequest');
        }

        return $userDataDto;
    }
}
