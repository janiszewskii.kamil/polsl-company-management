<?php

namespace App\Service;

use App\Constraint\ClientConstraint;
use App\Dto\ClientDto;
use App\Entity\Client;
use App\Entity\Company;
use App\Factory\ClientFactory;
use App\Helper\DtoHelper;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ClientService extends ApiService
{
    private $clientRepository;
    private $clientFactory;
    private $companyService;

    public function __construct(
        ClientRepository $clientRepository,
        ClientFactory $clientFactory,
        CompanyService $companyService,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientFactory = $clientFactory;
        $this->companyService = $companyService;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->clientRepository->findBy(['company' => $company]);
    }

    public function getById(int $id): Client
    {
        return $this->clientRepository->find($id);
    }

    public function create(Request $request): Client
    {
        $client = $this->clientFactory->create($this->prepareDto($request));
        $this->entityManager->persist($client);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $client;
    }

    public function update(Request $request, Client $client): Client
    {
        $updatedClient = $this->clientFactory->update($this->prepareDto($request), $client);
        $this->entityManager->persist($updatedClient);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $updatedClient;
    }

    public function remove(Client $client): void
    {
        $clientProjects = $client->getProjects();
        foreach ($clientProjects as $clientProject) {
            $clientProject->setClient(null);
            $this->entityManager->persist($clientProject);
        }
        $this->entityManager->remove($client);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();
    }


    private function prepareDto(Request $request): ClientDto
    {
        $clientDto = $this->serializer->deserialize($request->getContent(), ClientDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($clientDto), ClientConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidClientRequest');
        }

        return $clientDto;
    }
}
