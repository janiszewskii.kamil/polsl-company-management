<?php

namespace App\Service;

use App\Constraint\NewsConstraint;
use App\Dto\NewsDto;
use App\Entity\Company;
use App\Entity\News;
use App\Factory\NewsFactory;
use App\Helper\DtoHelper;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class NewsService extends ApiService
{
    private $newsRepository;
    private $newsFactory;

    public function __construct(
        NewsRepository $newsRepository,
        NewsFactory $newsFactory,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->newsRepository = $newsRepository;
        $this->newsFactory = $newsFactory;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->newsRepository->findBy(['company' => $company], ['createdAt' => 'DESC']);
    }

    public function create(Request $request): News
    {
        $news = $this->newsFactory->create($this->prepareDto($request));
        $this->entityManager->persist($news);
        $this->entityManager->flush();

        return $news;
    }

    public function remove(News $news): void
    {
        $this->entityManager->remove($news);
        $this->entityManager->flush();
    }

    public function update(Request $request, News $news): News
    {
        $updatedNews = $this->newsFactory->update($this->prepareDto($request), $news);
        $this->entityManager->persist($updatedNews);
        $this->entityManager->flush();

        return $updatedNews;
    }


    private function prepareDto(Request $request): NewsDto
    {
        $newsDto = $this->serializer->deserialize($request->getContent(), NewsDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($newsDto), NewsConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidNewsRequest');
        }

        return $newsDto;
    }
}
