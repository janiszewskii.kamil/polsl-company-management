<?php

namespace App\Service;

use App\Constraint\ItemConstraint;
use App\Dto\ItemDto;
use App\Entity\Company;
use App\Entity\Item;
use App\Factory\ItemFactory;
use App\Helper\DtoHelper;
use App\Repository\ItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ItemService extends ApiService
{
    private $itemFactory;
    private $itemRepository;

    public function __construct(
        ItemRepository $itemRepository,
        ItemFactory $itemFactory,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemFactory = $itemFactory;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getById(int $id): Item
    {
        return $this->itemRepository->find($id);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->itemRepository->findBy(['company' => $company]);
    }

    public function create(Request $request): Item
    {
        $item = $this->itemFactory->create($this->prepareDto($request));
        $this->entityManager->persist($item);
        $this->entityManager->flush();

        return $item;
    }

    public function update(Request $request, Item $item): Item
    {
        $updatedItem = $this->itemFactory->update($this->prepareDto($request), $item);
        $this->entityManager->persist($updatedItem);
        $this->entityManager->flush();

        return $updatedItem;
    }

    public function remove(Item $item): void
    {
        $this->entityManager->remove($item);
        $this->entityManager->flush();
    }


    private function prepareDto(Request $request): ItemDto
    {
        $itemDto = $this->serializer->deserialize($request->getContent(), ItemDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($itemDto), ItemConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidItemRequest');
        }

        return $itemDto;
    }
}
