<?php

namespace App\Service;

use App\Constraint\EventConstraint;
use App\Dto\EventDto;
use App\Entity\Company;
use App\Entity\Event;
use App\Entity\User;
use App\Factory\EventFactory;
use App\Helper\ContextHelper;
use App\Helper\DtoHelper;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class EventService extends ApiService
{
    private $eventRepository;
    private $eventFactory;

    public function __construct(
        EventRepository $eventRepository,
        EventFactory $eventFactory,
        ContextHelper $contextHelper,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->eventRepository = $eventRepository;
        $this->eventFactory = $eventFactory;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company, bool $isCanceled = null): array
    {
        if ($isCanceled) {
            return $this->eventRepository->findBy(['company' => $company, 'isCanceled' => $isCanceled]);
        } else {
            return $this->eventRepository->findBy(['company' => $company]);
        }
    }

    public function getAllByUser(User $user): array
    {
        return $this->eventRepository->findBy(['participant' => $user]);
    }

    public function create(Request $request): Event
    {
        $event = $this->eventFactory->create($this->prepareDto($request));
        $this->entityManager->persist($event);
        $this->entityManager->flush();

        return $event;
    }

    public function update(Request $request, Event $event): Event
    {
        $updatedEvent = $this->eventFactory->update($this->prepareDto($request), $event);
        $this->entityManager->persist($updatedEvent);
        $this->entityManager->flush();

        return $updatedEvent;
    }

    public function cancel(Event $event): void
    {
        $event->setIsCanceled(true)
            ->setUpdatedAt(new \DateTime())
            ->setUpdatedBy($this->contextHelper->getCurrentUser());
        $this->entityManager->persist($event);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): EventDto
    {
        $eventDto = $this->serializer->deserialize($request->getContent(), EventDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($eventDto), EventConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidEventRequest');
        }

        return $eventDto;
    }
}
