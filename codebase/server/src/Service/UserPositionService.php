<?php

namespace App\Service;

use App\Constraint\UserPositionConstraint;
use App\Dto\UserPositionDto;
use App\Entity\Company;
use App\Entity\UserPosition;
use App\Factory\UserPositionFactory;
use App\Helper\ContextHelper;
use App\Helper\DtoHelper;
use App\Repository\UserPositionRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class UserPositionService extends ApiService
{
    private $userPositionRepository;
    private $userRepository;
    private $userPositionFactory;
    private $companyService;

    public function __construct(
        UserPositionRepository $userPositionRepository,
        UserRepository $userRepository,
        UserPositionFactory $userPositionFactory,
        CompanyService $companyService,
        ContextHelper $contextHelper,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->userPositionRepository = $userPositionRepository;
        $this->userRepository = $userRepository;
        $this->userPositionFactory = $userPositionFactory;
        $this->companyService = $companyService;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function getById(int $id): UserPosition
    {
        return $this->userPositionRepository->find($id);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->userPositionRepository->findBy(['company' => $company]);
    }

    public function create(Request $request): UserPosition
    {
        $userPosition = $this->userPositionFactory->create($this->prepareDto($request));
        $this->entityManager->persist($userPosition);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $userPosition;
    }

    public function update(Request $request, UserPosition $userPosition): UserPosition
    {
        $updatedUserPosition = $this->userPositionFactory->update($this->prepareDto($request), $userPosition);
        $this->entityManager->persist($updatedUserPosition);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();

        return $updatedUserPosition;
    }

    public function remove(UserPosition $userPosition): void
    {
        $usersWithPosition = $this->userRepository->findBy(['position' => $userPosition]);
        foreach ($usersWithPosition as $userWithPosition) {
            $userWithPosition->setPosition(null);
            $this->entityManager->persist($userWithPosition);
        }
        $this->entityManager->remove($userPosition);
        $this->entityManager->flush();
        $this->companyService->refreshCurrentCompanyUpdatedInfo();
    }

    private function prepareDto($request): UserPositionDto
    {
        $userPositionDto = $this->serializer->deserialize($request->getContent(), UserPositionDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($userPositionDto), UserPositionConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidPositionRequest');
        }

        return $userPositionDto;
    }
}
