<?php

namespace App\Service;

use App\Entity\Company;

class DashboardService
{
    private $userService;
    private $projectService;
    private $eventService;
    private $applicationService;

    public function __construct(
        UserService $userService,
        ProjectService $projectService,
        EventService $eventService,
        ApplicationService $applicationService
    ) {
        $this->userService = $userService;
        $this->projectService = $projectService;
        $this->eventService = $eventService;
        $this->applicationService = $applicationService;
    }

    public function getDashboardByCompany(Company $company): array
    {
        return [
            'numberOfEmployees' => count($this->userService->getAllByCompany($company)),
            'numberOfProjects' => count($this->projectService->getAllOpenedByCompany($company)),
            'numberOfEvents' => count($this->eventService->getAllByCompany($company, false)),
            'numberOfApplications' => count($this->applicationService->getAllByCompany($company, 'opened'))
        ];
    }
}