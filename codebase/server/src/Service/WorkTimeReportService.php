<?php

namespace App\Service;

use App\Constraint\WorkTimeReportConstraint;
use App\Dto\WorkTimeReportDto;
use App\Entity\Company;
use App\Entity\Project;
use App\Entity\User;
use App\Entity\WorkTimeReport;
use App\Factory\WorkTimeReportFactory;
use App\Helper\DtoHelper;
use App\Repository\WorkTimeReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class WorkTimeReportService extends ApiService
{
    private $workTimeRecordRepository;
    private $workTimeRecordFactory;

    public function __construct(
        WorkTimeReportRepository $workTimeRecordRepository,
        WorkTimeReportFactory $workTimeRecordFactory,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    ) {
        $this->workTimeRecordRepository = $workTimeRecordRepository;
        $this->workTimeRecordFactory = $workTimeRecordFactory;
        parent::__construct(null, $entityManager, $serializer);
    }

    public function getAllByCompany(Company $company): array
    {
        return $this->workTimeRecordRepository->findAllByCompany($company);
    }

    public function getAllByUser(User $user): array
    {
        return $this->workTimeRecordRepository->findBy(['createdBy' => $user]);
    }

    public function getAllByProject(Project $project): array
    {
        return $this->workTimeRecordRepository->findBy(['project' => $project]);
    }

    public function create(Request $request): WorkTimeReport
    {
        $workTimeRecord = $this->workTimeRecordFactory->create($this->prepareDto($request));
        $this->entityManager->persist($workTimeRecord);
        $this->entityManager->flush();

        return $workTimeRecord;
    }

    public function update(Request $request, WorkTimeReport $workTimeReport): WorkTimeReport
    {
        $updatedWorkTimeReport = $this->workTimeRecordFactory->update($this->prepareDto($request), $workTimeReport);
        $this->entityManager->persist($updatedWorkTimeReport);
        $this->entityManager->flush();

        return $updatedWorkTimeReport;
    }

    public function remove(WorkTimeReport $workTimeReport): void
    {
        $this->entityManager->remove($workTimeReport);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): WorkTimeReportDto
    {
        $workTimeReportDto = $this->serializer->deserialize($request->getContent(), WorkTimeReportDto::class, 'json');
        $errors = DtoHelper::validate(
            $this->serializer->normalize($workTimeReportDto),
            WorkTimeReportConstraint::get()
        );

        if ($errors) {
            var_dump($errors);
            $this->throwHttpBadRequestException('invalidWorkTimeRequest');
        }

        return $workTimeReportDto;
    }
}
