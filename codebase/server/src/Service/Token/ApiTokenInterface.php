<?php
declare(strict_types=1);

namespace App\Service\Token;

interface ApiTokenInterface
{
    /**
     * Method deactivates user token depended in correct for implemented entity way.
     * @return void
     */
    public function deactivateToken(): void;

    /**
     * Method validates given token.
     * @param String $token
     * @return bool
     */
    public function isTokenValid(String $token): bool;
}
