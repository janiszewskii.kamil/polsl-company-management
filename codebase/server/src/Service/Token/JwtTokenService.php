<?php
declare(strict_types=1);

namespace App\Service\Token;

use App\Entity\BlackListJwt;
use App\Repository\BlackListJwtRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class JwtTokenService implements ApiTokenInterface
{
    private $em;
    private $requestStack;
    private $blackListJwtRepository;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        BlackListJwtRepository $blackListJwtRepository
    )
    {
        $this->requestStack = $requestStack;
        $this->em = $em;
        $this->blackListJwtRepository = $blackListJwtRepository;
    }

    public function deactivateToken(): void
    {
        $jwtToken = $this->requestStack->getCurrentRequest()->headers->get('Authorization');
        $this->addJwtTokenToBlackList($jwtToken);
    }

    private function addJwtTokenToBlackList(String $jwtToken): void
    {
        $blackListJwt = new BlackListJwt();
        $blackListJwt->setCreatedAt(new \DateTime())
            ->setToken(str_replace(
                'Bearer ',
                '',
                $jwtToken
            ));
        $this->em->persist($blackListJwt);
        $this->em->flush();
    }

    public function isTokenValid(String $token): bool
    {
        return $this->blackListJwtRepository->findOneBy(['token' => $token]) ? false : true;
    }
}
