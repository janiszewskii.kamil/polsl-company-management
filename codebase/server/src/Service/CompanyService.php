<?php

namespace App\Service;

use App\Constraint\CompanyConstraint;
use App\Dto\CompanyDto;
use App\Entity\Company;
use App\Factory\CompanyFactory;
use App\Helper\ContextHelper;
use App\Helper\DtoHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class CompanyService extends ApiService
{
    private $companyFactory;

    public function __construct(
        CompanyFactory $companyFactory,
        ContextHelper $contextHelper,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->companyFactory = $companyFactory;
        parent::__construct($contextHelper, $entityManager, $serializer);
    }

    public function update(Request $request, Company $company): Company
    {
        $updatedCompany = $this->companyFactory->update(
            $this->prepareDto($request),
            $company
        );
        $this->entityManager->persist($updatedCompany);
        $this->entityManager->flush();

        return $updatedCompany;
    }

    public function refreshCurrentCompanyUpdatedInfo(): void
    {
        $company =$this->contextHelper->getCurrentCompanyContext();
        $company->setUpdatedBy($this->contextHelper->getCurrentUser())
            ->setUpdatedAt(new \DateTime());
        $this->entityManager->persist($company);
        $this->entityManager->flush();
    }

    private function prepareDto(Request $request): CompanyDto
    {
        $companyDto = $this->serializer->deserialize($request->getContent(), CompanyDto::class, 'json');
        $errors = DtoHelper::validate($this->serializer->normalize($companyDto), CompanyConstraint::get());

        if ($errors) {
            $this->throwHttpBadRequestException('invalidCompanyRequest');
        }

        return $companyDto;
    }
}
