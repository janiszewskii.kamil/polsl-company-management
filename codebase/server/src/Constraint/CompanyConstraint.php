<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class CompanyConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName'])
                    ],
                    'regon' => [
                        new Assert\Regex(['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidRegon']),
                        new Assert\Length(
                            [
                                'min' => '9',
                                'max' => '14',
                                'minMessage' => 'invalidRegon',
                                'maxMessage' => 'invalidRegon'
                            ]
                        ),
                    ],
                    'nip' => [
                        new Assert\Length(
                            [
                                'min' => '10',
                                'max' => '10',
                                'exactMessage' => 'invalidNip',
                            ]
                        ),
                        new Assert\Regex(
                            ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidNip']
                        ),
                    ],
                    'address1' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidAddres1']
                    ),
                    'address2' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidAddres2']
                    ),
                    'typeOfBusiness' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidTypeOfBusiness']
                    ),
                ]
            ]
        );
    }
}