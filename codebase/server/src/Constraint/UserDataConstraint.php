<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

final class UserDataConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'firstName' => [
                        new Assert\NotNull(['message' => 'invalidFirstName']),
                        new Assert\NotBlank(['message' => 'invalidFirstName']),
                        new Assert\Length(['min' => 3, 'minMessage' => 'invalidFirstName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidFirstName']),
                        new Assert\Regex(['pattern' => RegexEnum::LETTERS_ONLY_REGEX, 'message' => 'invalidFirstName'])
                    ],
                    'lastName' => [
                        new Assert\NotNull(['message' => 'invalidLastName']),
                        new Assert\NotBlank(['message' => 'invalidLastName']),
                        new Assert\Length(['min' => 3, 'minMessage' => 'invalidLastName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidLastName']),
                        new Assert\Regex(['pattern' => RegexEnum::LETTERS_ONLY_REGEX, 'message' => 'invalidLastName'])
                    ],
                    'gender' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidGender']
                    ),
                    'birthDate' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidBirthDate']
                    ),
                    'phoneNumber' => [
                        new Assert\Length(['min' => 9, 'max' => 9, 'exactMessage' => 'invalidPhoneNumber']),
                        new Assert\Regex(
                            ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidPhoneNumber']
                        ),
                    ],
                    'street' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidStreet']
                    ),
                    'flatNo' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidFlatNo']
                    ),
                    'houseNo' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidHouseNo']
                    ),
                    'city' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidCity']
                    ),
                    'zipCode' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidZipCode']
                    ),
                    'province' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidProvince']
                    ),
                ]
            ]
        );
    }
}
