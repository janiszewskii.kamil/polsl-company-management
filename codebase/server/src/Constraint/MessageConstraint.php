<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class MessageConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'topic' => [
                        new Assert\NotNull(['message' => 'invalidTopic']),
                        new Assert\NotBlank(['message' => 'invalidTopic']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidTopic'])
                    ],
                    'text' => [
                        new Assert\NotNull(['message' => 'invalidText']),
                        new Assert\NotBlank(['message' => 'invalidText']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidText'])
                    ],
                    'recipient' => [
                        new Assert\NotNull(['message' => 'invalidRecipientId']),
                        new Assert\NotBlank(['message' => 'invalidRecipientId']),
                        new Assert\Regex(['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidRecipientId'])
                    ],
                ]
            ]
        );
    }
}