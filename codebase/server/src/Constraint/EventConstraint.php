<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class EventConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotNull(['message' => 'invalidName']),
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName'])
                    ],
                    'startDate' => [
                        new Assert\NotNull(['message' => 'invalidStartDate']),
                        new Assert\NotBlank(['message' => 'invalidStartDate']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidStartDate'])
                    ],
                    'endDate' => [
                        new Assert\NotNull(['message' => 'invalidEndDate']),
                        new Assert\NotBlank(['message' => 'invalidEndDate']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidEndDate'])
                    ],
                    'participant' => [
                        new Assert\Regex(
                            ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidParticipant']
                        ),
                    ],
                    'place' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidPlace'])
                    ],
                    'time' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidTime'])
                    ],
                ]
            ]
        );
    }
}
