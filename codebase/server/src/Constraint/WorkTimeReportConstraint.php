<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

final class WorkTimeReportConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'reportForDate' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidReportForDate']),
                    ],
                    'time' => [
                        new Assert\Regex(['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidTime']),
                    ],
                    'message' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidMessage']),
                    ],
                    'project' => [
                        new Assert\Regex(['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidProjectId']),
                    ],
                    'type' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidType']),
                    ],
                ]
            ]
        );
    }
}
