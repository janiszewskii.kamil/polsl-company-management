<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

final class DepartmentConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName']),
                    ],
                    'manager' => [
                        new Assert\NotBlank(['message' => 'invalidManager']),
                        new Assert\Regex(['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidManager']),
                    ],
                    'description' => [
                        new Assert\NotBlank(['message' => 'invalidDescription']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidDescription']),
                    ],
                ]
            ]
        );
    }
}
