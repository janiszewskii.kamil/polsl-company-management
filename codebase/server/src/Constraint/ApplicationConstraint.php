<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class ApplicationConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'title' => [
                        new Assert\NotNull(['message' => 'invalidName']),
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName'])
                    ],
                    'text' => [
                        new Assert\NotNull(['message' => 'invalidText']),
                        new Assert\NotBlank(['message' => 'invalidText']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidText'])
                    ],
                    'amount' => [
                        new Assert\Regex(['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidAmount'])
                    ],
                    'targetDepartment' => [
                        new Assert\NotNull(['message' => 'invalidDepartmentId']),
                        new Assert\NotBlank(['message' => 'invalidDepartmentId']),
                        new Assert\Regex(
                            ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidDepartmentId']
                        )
                    ],
                ]
            ]
        );
    }
}