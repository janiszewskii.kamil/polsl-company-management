<?php

namespace App\Constraint;

use Symfony\Component\Validator\Constraints as Assert;

final class UserPositionConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName']),
                    ],
                    'isManagerialPosition' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidIsManagerialPosition']),
                    ],
                    'description' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidDescription']),
                    ],
                ]
            ]
        );
    }
}
