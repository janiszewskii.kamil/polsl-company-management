<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class NewsConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'title' => [
                        new Assert\NotNull(['message' => 'invalidTitle']),
                        new Assert\NotBlank(['message' => 'invalidTitle']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidTitle'])
                    ],
                    'text' => [
                        new Assert\NotNull(['message' => 'invalidText']),
                        new Assert\NotBlank(['message' => 'invalidText']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidText'])
                    ],
                    'type' => [
                        new Assert\NotNull(['message' => 'invalidType']),
                        new Assert\NotBlank(['message' => 'invalidType']),
                        new Assert\Regex(
                            ['pattern' => RegexEnum::LETTERS_ONLY_REGEX, 'message' => 'invalidType']
                        )
                    ],
                ]
            ]
        );
    }
}