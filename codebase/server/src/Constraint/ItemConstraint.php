<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

final class ItemConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName']),
                    ],
                    'department' => [
                        new Assert\Regex(
                            ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidDepartmentId']
                        ),
                    ],
                    'employee' => [
                        new Assert\Regex(['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidUserId']),
                    ],
                    'typeOfOwnership' => [
                        new Assert\Regex(
                            ['pattern' => RegexEnum::LETTERS_ONLY_REGEX, 'message' => 'invalidTypeOfOwnership']
                        ),
                    ],
                    'warrantyEndDate' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidWarrantyEndDate']),
                    ],
                    'description' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidDescription']),
                    ],
                ]
            ]
        );
    }
}
