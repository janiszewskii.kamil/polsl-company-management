<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

final class UserConstraint
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'email' => [
                        new Assert\Email(['message' => 'invalidEmail']),
                        new Assert\NotNull(['message' => 'invalidEmail']),
                        new Assert\NotBlank(['message' => 'invalidEmail']),
                    ],
                    'hireDate' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidDate']
                    ),
                    'department' => new Assert\Regex(
                        ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidDepartmentId']
                    ),
                    'position' => new Assert\Regex(
                        ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidPositionId']
                    ),
                ]
            ]
        );
    }
}
