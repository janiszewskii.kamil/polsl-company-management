<?php

namespace App\Constraint;

use Symfony\Component\Validator\Constraints as Assert;

interface ConstraintInterface
{
    public static function get(): Assert\Collection;

}
