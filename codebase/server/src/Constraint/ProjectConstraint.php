<?php

namespace App\Constraint;

use App\Enum\RegexEnum;
use Symfony\Component\Validator\Constraints as Assert;

class ProjectConstraint implements ConstraintInterface
{
    public static function get(): Assert\Collection
    {
        return new Assert\Collection(
            [
                'allowMissingFields' => true,
                'allowExtraFields' => true,
                'fields' => [
                    'name' => [
                        new Assert\NotBlank(['message' => 'invalidName']),
                        new Assert\Type(['type' => 'string', 'message' => 'invalidName'])
                    ],
                    'code' => [
                        new Assert\Type(['type' => 'string', 'message' => 'invalidCode'])
                    ],
                    'startDate' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidStartDate']
                    ),
                    'plannedEndDate' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidPlannedEndDate']
                    ),
                    'endDate' => new Assert\Type(
                        ['type' => 'string', 'message' => 'invalidEndDate']
                    ),
                    'plannedBudget' => new Assert\Regex(
                        ['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidPlannedBudget']
                    ),
                    'spentBudget' => new Assert\Regex(
                        ['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidSpentBudget']
                    ),
                    'plannedIncome' => new Assert\Regex(
                        ['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidPlannedIncome']
                    ),
                    'finalIncome' => new Assert\Regex(
                        ['pattern' => RegexEnum::FLOAT_REGEX, 'message' => 'invalidFinalIncome']
                    ),
                    'manager' => new Assert\Regex(
                        ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidManagerId']
                    ),
                    'client' => new Assert\Regex(
                        ['pattern' => RegexEnum::NUMBERS_ONLY_REGEX, 'message' => 'invalidManagerId']
                    ),
                ]
            ]
        );
    }
}